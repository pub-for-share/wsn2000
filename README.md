蓝牙传感器网络第二代WSN2000由于镜像文件过大，所以关闭了OTA升级功能，如要打开，可以在IDE里菜单Project->Properties,然后
Include OTA Update Bootloader改成Yes，这一步是导入安装目录下C:\CSR_uEnergy_SDK-2.4.5.13\tools\lib\otau\bootloader.img
同时对BootLoader进行了参数配置，然后源代码user_config.h打开宏ENABLE_GATT_OTA_SERVICE，就出现了CSR自定义的OTA service

第二代真正通过蓝牙集中器hub（桥设备）收集传感器数据汇总到ESP32 Gateway网关送入AWS云服务去，然后通过提供的JS（.html）前端
面板呈现数据折线图来直观反映其变化。