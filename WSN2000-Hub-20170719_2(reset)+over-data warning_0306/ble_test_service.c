#include <gatt.h>
#include <status.h>
#include <mem.h>
#include <buf_utils.h>
#include <timer.h>
#include <stream_model.h>
#include <pio.h>
#include "battery_hw.h"
#include "iot_hw.h"

#include "ble_test_service.h"
#include "app_data_stream.h"
#include "app_gatt_db.h"
#include <debug.h>

uint16 resendTestTimes;
//uint16 handleATT;
uint16 dataReadFlag;
uint16 accessDeviceCount;

void repeatSendCmdTimerHandler(timer_id tid);

extern void bleTestServiceInit()
{
    //uint16 devid_nvm=0;
    //devid_nvm=getDevid();//local
    //bleTestServData.deviceID=devid_nvm;
    isRemote=FALSE;
    bleTestServData.temp=2413;
    bleTestServData.humi=6589;//profile两位精度
    bleTestServData.pressure=1013255;//profile一位精度
    bleTestServData.currentVal=3300;    
    //bleTestServData.batLevel=ReadBatteryLevel();//to be confirmed??

    //bleTestServData.batLevelCfg=gatt_client_config_none;
    bleTestServData.dataReadyCfg=gatt_client_config_notification;
            /*if(waitSensorDatarRead_tid!=TIMER_INVALID)
            {
                TimerDelete(waitSensorDatarRead_tid);
                waitSensorDatarRead_tid=TIMER_INVALID;
            }*/
}

extern void bleTestHandleAccessRead(GATT_ACCESS_IND_T *p_ind)   //local or remote
{   //must first read dataReady flag
    sys_status rc=sys_status_success;
    uint8 *p_val=p_ind->value;
    uint8 length=0;
    uint8 value[4];
    switch(p_ind->handle)
    {      
        case HANDLE_ES_TEMP:
        {
            length=2;
            value[0]=bleTestServData.temp&0xff;
            value[1]=(bleTestServData.temp>>8)&0xff;
            p_val=value;
            //BufWriteUint16(&p_val,bleTestServData.temp);//littlt-endian
            streamFinishTransfer|=0x1;
        }
        break;
        
        case HANDLE_ES_HUMI:
        {
            length=2;
            value[0]=bleTestServData.humi&0xff;
            value[1]=(bleTestServData.humi>>8)&0xff;
            //value[2]=(bleTestServData.humi>>16)&0xff;
            //value[3]=(bleTestServData.humi>>24)&0xff;
            p_val=value;
            streamFinishTransfer|=0x02;
        }
        break;
        
        case HANDLE_ES_PRESSURE:
        {
            length=4;
            value[0]=bleTestServData.pressure&0xff;
            value[1]=(bleTestServData.pressure>>8)&0xff;
            value[2]=(bleTestServData.pressure>>16)&0xff;
            value[3]=(bleTestServData.pressure>>24)&0xff;            
            p_val=value;
            streamFinishTransfer|=0x04;
        }
        break;
        
        case HANDLE_CURRENT_VAL:
        {
            length=2;
            value[0]=bleTestServData.currentVal&0xff;
            value[1]=(bleTestServData.currentVal>>8)&0xff;//little-endian
            p_val=value;
            streamFinishTransfer|=0x08;
        }
        break;
        /*case HANDLE_BATTERY_LEVEL:
        {
            length=1;
            uint8 bat_level=bleTestServData.batLevel;
            //value[0]=bat_level&0xff;//little-endian
            //value[1]=(((uint16)bat_level)>>8)&0xff;
            p_val=&bat_level;
            streamFinishTransfer|=0x08;
        }
        break;
        
        case HANDLE_BATTERY_LEVEL_CCC:
        {
            length=2;
            p_val = value;
            BufWriteUint16(&p_val,bleTestServData.batLevelCfg);
            p_val = value;
        }
        break;*/
        
        case HANDLE_BRI_DEVID:
        {
            length=2;
            value[0]=bleTestServData.deviceID&0xff;
            value[1]=(bleTestServData.deviceID>>8)&0xff;
            p_val=value;
            //streamFinishTransfer|=0x10;
            if(current_sensor_status==sensor_status_init)
            {
                current_sensor_status=sensor_status_register;
                saveSensorStatusToNvm((uint16)current_sensor_status);
                
                bridgeFlagNvm=0x0001;//device->bridge转变
                saveBridgeFlagToNVM(bridgeFlagNvm);                
            }
        }
        break;  
        
        /*case HANDLE_DATA_READY:
        {
            length=1;
            p_val=&sensorDataComplete;//sensorDataReady
        }
        break;*/
        
        case HANDLE_DATA_READY_CCC: //0x0037
        {
            length=2;
            p_val = value;
            BufWriteUint16(&p_val,bleTestServData.dataReadyCfg);
            p_val = value;
        }
        break;
        
        default:
        break;
    }
    if((streamFinishTransfer&0x0f)!=0x00)
    {        
        //if(waitSensorDatarRead_tid!=TIMER_INVALID)
        {         
            TimerDelete(waitSensorDatarRead_tid);
            waitSensorDatarRead_tid=TIMER_INVALID;//stop dataReady indication         
        }
        PioEnablePWM(2,FALSE);
        //PioSetMode(LED_PIO_IND,pio_mode_user);//LED_PIO_BLUE
        //PioSet(LED_PIO_IND,LED_OFF);
            /*PioSetMode(LED_PIO_IND,pio_mode_pwm2);//LED_PIO_BLUE                                
            PioConfigPWM(2,pio_pwm_mode_push_pull,50,0,63,0,192,63,0);
            PioEnablePWM(2,TRUE);*/
        bleTestServData.dataReadyCfg=gatt_client_config_none;
        streamFinishTransfer=0x00;
        //sensorDataReady=0;
        sensorDataComplete=0x00;                   
    }
    
    //if( (streamFinishTransfer&0x07) ==0x07)
    //    sensorDataReady&=0x02;//stream1:clear after reading
    //if( (streamFinishTransfer&0x18) ==0x18)
    //    sensorDataReady&=0x01;//stream2
    GattAccessRsp(p_ind->cid,p_ind->handle,rc,length,p_val);
}

extern void bleTestHandleAccessWrite(GATT_ACCESS_IND_T *p_ind)
{
    sys_status rc=sys_status_success;
    uint8 *p_val=p_ind->value;
    //uint8 length=0;
    switch(p_ind->handle)
    {
        case HANDLE_BRI_DEVID:
        {
            uint8  devIDUnpacked[2];
            //devID=BufReadUint16(&p_val);//little-endian
            //MemCopyPack(&devID,p_val,p_ind->size_value);//little-endian
            MemCopy(devIDUnpacked,p_val,p_ind->size_value);//little-endian
            targetDevID=((uint16)devIDUnpacked[1])<<8 | devIDUnpacked[0];
            //DebugWriteUint16(targetDevID);DebugWriteString(" ");            
            if(accessDeviceCount>=7) accessDeviceCount=0;
            accessDeviceCount++;/**/
            //requestSensorDataCfm=FALSE;//RESET
            streamCFM=FALSE;
            sensorDataComplete=0x00;//sensorDataReady=0;
            bleTestServData.deviceID=targetDevID;
            bleTestServData.dataReadyCfg=gatt_client_config_notification;
            if(waitSensorDatarRead_tid!=TIMER_INVALID)
            {
                TimerDelete(waitSensorDatarRead_tid);
                waitSensorDatarRead_tid=TIMER_INVALID;
            }
            
            uint16 devid_nvm=0;
            devid_nvm=getDevid();
            if(targetDevID==devid_nvm) //local device
            {
                isRemote=FALSE;//only directly read characteristic(local)
                BME280_Trigger();
                TimerCreate(10*MILLISECOND,TRUE,timerHandleReadBME280Data);//10*200ms
            }
            else
            {
                //sendCmdCount=10;//retry
                //whichProcess=1;                
                //dat[0]=REQUEST_DATA;
                //sendDataStreamWithCFM(1,targetDevID,10);//10*200ms
    bleTestServData.temp=0;
    bleTestServData.humi=0;//profile两位精度
    bleTestServData.pressure=0;//profile一位精度                
                uint8 requestDat[1];
                sendCustomCmd(200,1,requestDat,1,REQUEST_DATA,targetDevID);//4*500ms
                //TimerCreate(0*SECOND,TRUE,repeatSendCmdTimerHandler);
            }/**/
                        
        }        
        break;
        
        case HANDLE_CURRENT_TIME:
        {
            uint8  timeUnpacked[2];
            if(current_sensor_status==sensor_status_normal) //校时
            {                
                //uint16 writeAlarmTimePacked[2];
                MemCopy(timeUnpacked,p_val,p_ind->size_value);//little-endian
                sendCustomCmd(200,4,timeUnpacked,2,CUSTOM_RTCTIME_CORRECTION,0x0000);//to all devices:200ms*4                
            }
            
            //writeAlarmTimePacked[0]=PACK(0X12,timeUnpacked[0]);
            //writeAlarmTimePacked[1]=PACK(timeUnpacked[1],0X08);
            /*uint16 setPCF2123AlarmTimer[9]=
            {
                PACK(0X10,0X00),PACK(0X11,timeUnpacked[0]),PACK(timeUnpacked[1],0x08),PACK(0X09,0X04),PACK(0X11,0X17),
                PACK(0X80,0X80),PACK(0X80,0X80),PACK(0X00,0X0a),0x08,//规则点开始(起点)  
            };            
                    uint16 cmdLength=0;
                    clearSpiCmdAndLen();
                    cmdArrayCat(setPCF2123AlarmTimer,sizeof(setPCF2123AlarmTimer)/sizeof(uint16));//9
                    cmdLength=checkCmdLength();
                    deviceType=0;
                    spiTransfer(deviceType,PIO_CTRLR_CODE_ADDR0,2*cmdLength);*/
            bleTestServData.currentTime=((uint16)timeUnpacked[1])<<8 | timeUnpacked[0];
        }
        break;
        
        case HANDLE_DATA_READY_CCC://0x0037
        {
            uint16 dataReadyCfg;
            dataReadyCfg=BufReadUint16(&p_val);
            if(dataReadyCfg==gatt_client_config_none||
               dataReadyCfg==gatt_client_config_notification)
            {
                bleTestServData.dataReadyCfg=dataReadyCfg;
            }
            else 
                rc = gatt_status_desc_improper_config;
        }
        break;
        
        /*case HANDLE_BATTERY_LEVEL_CCC:
        {
            uint16 batLevelCfg;
            uint8 batLevel;
            batLevel=bleTestServData.batLevel;
            batLevelCfg=BufReadUint16(&p_val);
            if(batLevelCfg==gatt_client_config_none||
               batLevelCfg==gatt_client_config_notification)
            {
                bleTestServData.batLevelCfg=batLevelCfg;
            }
            else 
                rc = gatt_status_desc_improper_config;
            if(bleTestServData.batLevelCfg==gatt_client_config_notification)
            {
                GattCharValueNotification(CONNECTION_CID,HANDLE_BATTERY_LEVEL,
                                        sizeof(batLevel),&batLevel);
            }           
        }
        break;*/
        
        default:
        break;
    }
    GattAccessRsp(p_ind->cid,p_ind->handle,rc,0,NULL);
    /*if((bleTestServData.dataReadyCfg==gatt_client_config_notification)&&
       (sensorDataComplete==0x01))
    {
        GattCharValueNotification(CONNECTION_CID,HANDLE_DATA_READY,
                                sizeof(sensorDataComplete),&sensorDataComplete);//直接发送一长串
    }*/
}/*
void repeatSendCmdTimerHandler(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
    if(sendCmdCount>0)
    {
        if(requestSensorDataCfm==FALSE)
        {
            sendCmdCount--;
            uint8 toSendCmd=REQUEST_DATA;
            StreamStartSender(targetDevID);
            StreamSendData(&toSendCmd,sizeof(toSendCmd));
            StreamFlush();
            TimerCreate(SENDCMD_INTERVAL,TRUE,repeatSendCmdTimerHandler);//10*200ms
        }
        else requestSensorDataCfm=FALSE;
    }
}
        
void recvdCmdCbTimerHandler(timer_id tid)
{
    TimerDelete(notifyTestDat_tid);notifyTestDat_tid=TIMER_INVALID;
    //LsRadioEventNotification(CONNECTION_CID,radio_event_first_tx);
    uint8 alterAttTest=0;

    //if(bleTestServData.devidCfg==gatt_client_config_notification)
    {
        //handleATT=HANDLE_DEVICE_ID;
        alterAttTest=bleTestServData.deviceID;        
        GattCharValueNotification(CONNECTION_CID,HANDLE_DEVICE_ID,
                         sizeof(alterAttTest),&alterAttTest);   
        //notifyTestDat_tid=TimerCreate(1*SECOND,TRUE,recvdCmdCbTimerHandler);
    }
}
*/
extern bool bleTestCheckHandleRange(uint16 handle)
{
    return ( (handle>=HANDLE_ES_SERVICE)&&(handle<=HANDLE_ES_SERVICE_END) )?
            TRUE:FALSE;
}
