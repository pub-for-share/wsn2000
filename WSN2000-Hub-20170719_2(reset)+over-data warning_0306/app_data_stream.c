/******************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2015
 *  CSR Bluetooth Low Energy CSRmesh 1.3 Release
 *  Application version 1.3
 *
 *  FILE
 *    app_data_stream.c
 *
 * DESCRIPTION
 *    This file implements a simple protocol over the data model to exchange
 *    device information.
 *    The protocol:
 *       | CODE | LEN (1 or 2 Octets| Data (LEN Octets)|
 *       CODE Defined by APP_DATA_STREAM_CODE_T
 *       LEN - if MS Bit of First octet is 1, then len is 2 octets
 *       if(data[0] & 0x80) LEN = data[0]
 *
 ******************************************************************************/

/*=============================================================================*
 *  SDK Header Files
 *============================================================================*/
#include <gatt.h>
#include <timer.h>
#include <mem.h>
#include <pio.h>
#include <random.h>
/*============================================================================*
 *  CSR Mesh Header Files
 *============================================================================*/
#include <csr_mesh.h>
#include <stream_model.h>

/*=============================================================================*
 *  Local Header Files
*============================================================================*/
#include "app_data_stream.h"
#include "ble_test_service.h"
#include "csr_mesh_tempsensor.h"
#include "sensor_bme280.h"
#include "battery_hw.h"
#include "app_gatt_db.h"
#include "iot_hw.h"

#ifdef  ENABLE_DATA_MODEL
/*=============================================================================*
 *  Private Definitions
 *============================================================================*/

#define DEVICE_INFO_STRING      "CSRmesh Temp Sensor application\r\n" \
                                "Supported Models:\r\n" \
                                "  Sensor Model\r\n" \
                                "  Bearer Model\r\n" \
                                "  Attention Model\r\n" \
                                "  Data Model"

/* Data stream send retry wait time */
#define STREAM_SEND_RETRY_TIME            (500 * MILLISECOND)

/* Data stream received timeout value */
#define RX_STREAM_TIMEOUT                 (5 * SECOND)

/* Max number of retries */
#define MAX_SEND_RETRIES                  (3)

/*=============================================================================*
 *  Private Data
 *============================================================================*/
/* String to give a brief description of the application */
static uint8 device_info[256];//设备信息以数组形式存放

/* Device info length */
static uint8 device_info_length;

/* Stream bytes sent tracker */
static uint16 tx_stream_offset = 0;

/* Stream send retry timer */
static timer_id stream_send_retry_tid = TIMER_INVALID;

/* Stream send retry counter */
static uint16 stream_send_retry_count = 0;

/* Current Rx Stream offset */
static uint16 rx_stream_offset = 0;

/* Rx Stream status flag */
static bool rx_stream_in_progress = FALSE;

/* Rx stream timeout tid */
static timer_id rx_stream_timeout_tid;

static APP_DATA_STREAM_CODE_T current_stream_code;

bool flagCustomReqDataRec;
uint16 devid_target;
uint16 stream_len=0;
//uint16 iCounter=0;
uint16 timecounter=0;

/* Stream send retry counter */
static uint16 streamSendRetryCount = 0;
static uint16 streamSendRetryCountCheck = 0;
uint16 sendCustomCmdIntervalms;
bool isSameDevice;
uint16 devidReferenceSrc;
uint16 checkSensorData;

/*=============================================================================*
 *  Private Function Prototypes
 *============================================================================*/
static void streamSendRetryTimer(timer_id tid);
static void sendNextPacket(void);
void debounceTimerHandlerCUSTOMREQDATA(timer_id tid);
void recvdDevidCmdTimerHandler(timer_id tid);
void forceIntoSleepTimerHandler(timer_id tid);

void streamSendRetryTimerHandler(timer_id tid);
void streamSendRetryTimer1(timer_id tid);

void receiveCmdIndHandler(timer_id tid);
/*=============================================================================*
 *  Private Function Implementations
 *============================================================================*/

/*----------------------------------------------------------------------------*
 *  NAME
 *      streamSendRetryTimer
 *
 *  DESCRIPTION
 *      Timer handler to retry sending next packet
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void streamSendRetryTimer(timer_id tid)
{
    if( tid == stream_send_retry_tid )
    {
        stream_send_retry_tid = TIMER_INVALID;
        stream_send_retry_count++;
        if( stream_send_retry_count < MAX_SEND_RETRIES )//3
        {
            StreamResendLastData();
            stream_send_retry_tid =  TimerCreate(STREAM_SEND_RETRY_TIME, TRUE,
                                                          streamSendRetryTimer);
        }
        else
        {
            stream_send_retry_count = 0;
            StreamFlush();
            /* Set the mesh scan back to low duty cycle if the device is already 
             * configured.
             */
            EnableHighDutyScanMode(FALSE);
        }
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      rxStreamTimeoutHandler
 *
 *  DESCRIPTION
 *      Timer handler to handle rx stream timeout
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void rxStreamTimeoutHandler(timer_id tid)
{
    if( tid == rx_stream_timeout_tid )
    {
        /* Reset the stream */
        rx_stream_timeout_tid = TIMER_INVALID;
        rx_stream_in_progress = FALSE;
        StreamReset();
        /* Set the mesh scan back to low duty cycle if the device is already 
         * configured.
         */
        EnableHighDutyScanMode(FALSE);
    }
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      sendNextPacket
 *
 *  DESCRIPTION
 *      Forms a stream data packet with the current counter and sends it to
 *      the stream receiver
 *
 *  RETURNS/MODIFIES
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
static void sendNextPacket(void)
{
    uint16 data_pending, len;

    /* Stop retry timer */
    stream_send_retry_count = 0;
    TimerDelete(stream_send_retry_tid);
    stream_send_retry_tid = TIMER_INVALID;

    data_pending = device_info_length+2 - tx_stream_offset;

    if( data_pending )
    {   //取其小者
        len = (data_pending > STREAM_DATA_BLOCK_SIZE_MAX)? //库:8
                                    STREAM_DATA_BLOCK_SIZE_MAX:data_pending;

        /* Send the next packet */
        StreamSendData(&device_info[tx_stream_offset], len);
        tx_stream_offset += len;

        stream_send_retry_tid = TimerCreate(100 * MILLISECOND, TRUE,
                                                       streamSendRetryTimer);
    }
    else
    {
        /* Send flush to indicate end of stream */
        StreamFlush();
        /* Set the mesh scan back to low duty cycle if the device is already 
         * configured.
         */
        EnableHighDutyScanMode(FALSE);
    }
}
void sendDataStreamWithCFM(uint16 dat_len,uint16 devid_dest, uint16 counter)
{   
    devid_target = devid_dest;
    stream_len  = dat_len;
    streamSendRetryCount = counter;
    streamSendRetryCountCheck=0;

    streamCFM=FALSE;
    
    /* Stop retry timer */
    TimerDelete(stream_send_retry_tid); //reset timer
    stream_send_retry_tid = TIMER_INVALID; //reset timer
    
    StreamStartSender(devid_target); //set target device id
    StreamFlush();
    StreamSendData(dat,stream_len); //function to send the data
    //StreamFlush();//20170928
        
    stream_send_retry_tid = TimerCreate(200 * MILLISECOND, TRUE,
                                        streamSendRetryTimerHandler);//retry after 100ms call streamSendRetryTimerHandler    
}

void streamSendRetryTimerHandler(timer_id tid)//20160520 //send retry timer handler
{
    stream_send_retry_tid = TIMER_INVALID; //reset timer
    static uint32 random32;//20160815
    streamSendRetryCountCheck++; //inc retry counter
    
    if(streamSendRetryCountCheck>streamSendRetryCount)//directly send data without cfm
    {
        //if(currentSensorStatus==SENSOR_STATUS_REGISTER)
        {               
            streamSendRetryCount = 0; //reset send retry counter
            streamSendRetryCountCheck = 0;
            TimerDelete(stream_send_retry_tid);
            stream_send_retry_tid=TIMER_INVALID;
            streamCFM=FALSE;//20160816
   
            //TimerCreate(0*MILLISECOND,TRUE,forceIntoSleepTimerHandler);//20170216                 
        }        
    }
    else  
        if(streamCFM==FALSE)
    {
        StreamStartSender(devid_target);
        StreamFlush();//20170928
        StreamSendData(dat,stream_len);
        //StreamFlush();//20170928  
        random32=(Random32()%20)*150;//(0:400:8000]
        stream_send_retry_tid =  
                TimerCreate(200*MILLISECOND, 
                            TRUE,streamSendRetryTimerHandler);
    }
}
void sendCustomCmd(uint16 intervalms,uint16 retryCounter,uint8 customData[], \
                   uint16 dataLength,uint16 customCode, uint16 targDevid)   //uint16 intervalms,
{   
    EnableHighDutyScanMode(TRUE);//20160819
    streamSendRetryCount=retryCounter;//20170306:原先屏蔽的两句打开
    sendCustomCmdIntervalms=intervalms;
    devid_target=targDevid;
    stream_len=dataLength;//20170303
    
    dat[0]=customCode; //send cmd
    uint16 i;
    for(i=0;i<dataLength;i++)        
        dat[i+1]=customData[i];
   
    /* Stop retry timer */
    stream_send_retry_count = 0;
    stream_send_retry_tid = TIMER_INVALID;
    //StreamStartSender(devid_target);StreamSendData(dat,2);
    StreamSendDataBlock(devid_target,dat,stream_len+1);//20160824

    streamCFM=FALSE;
    
    stream_send_retry_tid = 
            TimerCreate(sendCustomCmdIntervalms * MILLISECOND,
                        TRUE, streamSendRetryTimer1);
}

/*20160520*/
void streamSendRetryTimer1(timer_id tid)
{     
    TimerDelete(stream_send_retry_tid);
    stream_send_retry_tid = TIMER_INVALID;
    
    stream_send_retry_count++;

    if( stream_send_retry_count<streamSendRetryCount )    //(streamCFM==TRUE)
    {            
        //StreamResendLastData();
        //StreamStartSender(0x0000);//broadcast
        //StreamSendData(dat,2);
        StreamSendDataBlock(devid_target,dat,stream_len+1);//20170210
        timecounter++;

        stream_send_retry_tid = TimerCreate(sendCustomCmdIntervalms*MILLISECOND, 
                                            TRUE,streamSendRetryTimer1);
    }
    else
    {
        stream_send_retry_count = 0;
        timecounter=0;//20160808
        //StreamFlush();
    }
}
/*=============================================================================*
 *  Public Function Implementations
 *============================================================================*/

/*-----------------------------------------------------------------------------*
 *  NAME
 *      AppDataStreamInit
 *
 *  DESCRIPTION
 *      This function initializes the stream Model.
 *
 *  RETURNS
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
extern void AppDataStreamInit(uint16 *group_id_list, uint16 num_groups)
{
    /* Model initialisation */
    StreamModelInit(group_id_list, num_groups);//注册模式句柄

    /* Reset timers */
    stream_send_retry_tid = TIMER_INVALID;
    rx_stream_timeout_tid = TIMER_INVALID;

    /* Reset the device info */
    device_info_length = sizeof(DEVICE_INFO_STRING);
    device_info[0] = CSR_DEVICE_INFO_RSP;//protocol:code|length|data
    device_info[1] = device_info_length;

    MemCopy(&device_info[2], DEVICE_INFO_STRING, sizeof(DEVICE_INFO_STRING));
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      handleCSRmeshDataStreamFlushInd
 *
 *  DESCRIPTION
 *      This function handles the CSR_MESH_DATA_STREAM_FLUSH message.
 *
 *  RETURNS
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
extern void handleCSRmeshDataStreamFlushInd(CSR_MESH_STREAM_EVENT_T *p_event)
{
    rx_stream_offset = 0;

    if( rx_stream_in_progress == FALSE )
    {
        /* Change the Rx scan duty cycle to active at the start of data stream */
        EnableHighDutyScanMode(TRUE);
        /* Start the stream timeout timer */
        TimerDelete(rx_stream_timeout_tid);
        rx_stream_timeout_tid = TimerCreate(RX_STREAM_TIMEOUT, TRUE,
                                                        rxStreamTimeoutHandler);
    }
    else
    {
        /* End of stream */
        rx_stream_in_progress = FALSE;//暂停
        TimerDelete(rx_stream_timeout_tid);
        rx_stream_timeout_tid = TIMER_INVALID;
        /* Set the mesh scan back to low duty cycle if the device is already 
         * configured.
         */
        EnableHighDutyScanMode(FALSE);
    }
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      handleCSRmeshDataBlockInd
 *
 *  DESCRIPTION
 *      This function handles the CSR_MESH_DATA_BLOCK_IND message
 *
 *  RETURNS
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
extern void handleCSRmeshDataBlockInd(CSR_MESH_STREAM_EVENT_T *p_event)
{
    uint16 sourceDevId=p_event->common_data.source_id;    
    switch(p_event->data[0])
    {
        case CSR_DEVICE_INFO_REQ:
        {
            /* Change the Rx scan duty cycle to active at the start of stream */
            EnableHighDutyScanMode(TRUE);
            /* Set the source device ID as the stream target device */
            StreamStartSender(p_event->common_data.source_id );
            tx_stream_offset = 0;
            /* Set the opcode to CSR_DEVICE_INFO_RSP */
            device_info[0] = CSR_DEVICE_INFO_RSP;

            /* start sending the data */
            sendNextPacket();
        }
        break;

        case CSR_DEVICE_INFO_RESET:
        {
            /* Reset the device info */
            device_info_length = sizeof(DEVICE_INFO_STRING);
            device_info[0] = CSR_DEVICE_INFO_RSP;
            device_info[1] = device_info_length;
            MemCopy(&device_info[2], DEVICE_INFO_STRING,
                                                   sizeof(DEVICE_INFO_STRING));
        }
        break;
            /*
            case REQUEST_DATA://target as sender
            {
                devid_bridge=p_event->common_data.source_id;
                if(flagCustomReqDataRec==FALSE)
                {
                    //sendbackSensorData1Cfm=FALSE;
                    //sendbackSensorData2Cfm=FALSE;//RESET
                    
                    //PioSetMode(LED_PIO_GREEN,pio_mode_pwm1);
                    //PioEnablePWM(LED_PWM_GREEN,TRUE);
                    //PioConfigPWM(LED_PWM_GREEN,pio_pwm_mode_push_pull,50,0,20,0,192,20,0);
                    isRemote=TRUE;
                    BME280_Trigger();
                    TimerCreate(10*MILLISECOND,TRUE,timerHandleReadBME280Data);
                    TimerCreate(3*SECOND,TRUE,debounceTimerHandlerCUSTOMREQDATA);
                    flagCustomReqDataRec=TRUE;//locked
                    sensorDataReady=0;
                }
            }
            break;*/        
        
            case RECEIVE_DATA1://bridge as receiver
            {
                if( sourceDevId==targetDevID )//异类干扰（保证始终只处理最新云目标设备）                     
                {
                    /*devidReferenceSrc=0;//test                    
                    if(checkSensorData>=2) checkSensorData=0;
                    else checkSensorData++;*/
                    
                    if(sourceDevId==devidReferenceSrc)//同类干扰
                    {
                        isSameDevice=TRUE;
                    }
                    else
                    {
                        isSameDevice=FALSE;
                        devidReferenceSrc=sourceDevId;//update
                    }
                    
                    if(isSameDevice==FALSE)
                    {
                        uint8 recvdSensorData1[10];
                        testStreamData1++;
                        MemCopy(recvdSensorData1,p_event->data+1,p_event->data_len);
                        /*if(recvdSensorData1[0]==0xaa)
                        {
                            checkSensorData|=0x01;
                            bleTestServData.temp=recvdSensorData1[1]|(((uint16)recvdSensorData1[2])<<8);
                            //testStreamData=testStreamData+bleTestServData.temp;
                            testStreamData++;
                        }
                        if(recvdSensorData1[0]==0xbb)
                        {
                            checkSensorData|=0x02;
                            bleTestServData.humi=recvdSensorData1[4]|(((uint16)recvdSensorData1[5])<<8);
                        }
                        if(recvdSensorData1[0]==0xcc)
                        {
                            checkSensorData|=0x04;
                        bleTestServData.pressure=recvdSensorData1[1]|(((uint16)recvdSensorData1[2])<<8)|
                                                 (((uint24)recvdSensorData1[3])<<16);
                        
                        }*/                        
                        
                        bleTestServData.temp=recvdSensorData1[0]|(((uint16)recvdSensorData1[1])<<8);
                        bleTestServData.humi=recvdSensorData1[2]|(((uint16)recvdSensorData1[3])<<8);
                        bleTestServData.pressure=recvdSensorData1[4]|(((uint16)recvdSensorData1[5])<<8)|
                                                 (((uint24)recvdSensorData1[6])<<16);
                        bleTestServData.currentVal=recvdSensorData1[7]|(((uint16)recvdSensorData1[8])<<8);
                        bleTestServData.lowPowerWarning=recvdSensorData1[9]&0x01;/**/
                        //if((checkSensorData&0x0f)==0x07)
                        {
                            //PioSet(LED_PIO_IND,LED_OFF);//V
                            //TimerCreate(0*SECOND,TRUE,receiveCmdIndHandler);
                        }
                        
                        TimerCreate(3*SECOND,TRUE,debounceTimerHandlerCUSTOMREQDATA);
                        flagCustomReqDataRec=TRUE;//locked
                        
                        sensorDataComplete=0x01;
                        dataReadyNotifyCount=2;
                        waitSensorDatarRead_tid=
                            TimerCreate(0*SECOND,TRUE,waitSensorDataReadTimerHandler);
                    }
                }
            }
            break;/**/            
        
        default:
        break;
    }
}
void receiveCmdIndHandler(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
    checkSensorData=0;
    PioSet(LED_PIO_IND,LED_ON);
}
/*-----------------------------------------------------------------------------*
 *  NAME
 *      handleCSRmeshDataStreamDataInd
 *
 *  DESCRIPTION
 *      This function handles the CSR_MESH_DATA_STREAM_DATA_IND message
 *
 *  RETURNS
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
extern void handleCSRmeshDataStreamDataInd(CSR_MESH_STREAM_EVENT_T *p_event)
{
    /* Restart the stream timeout timer */
    TimerDelete(rx_stream_timeout_tid);
    //uint16 sourceDevId=p_event->common_data.source_id;
    rx_stream_timeout_tid = TimerCreate(RX_STREAM_TIMEOUT, TRUE,
                                                    rxStreamTimeoutHandler);

    /* Set stream_in_progress flag to TRUE */
    rx_stream_in_progress = TRUE;

    if( rx_stream_offset == 0 )
    {
        /* If the stream offset is 0. The data[0] will be the CODE */
        switch(p_event->data[0])
        {
            case CSR_DEVICE_INFO_REQ:
            {
                /* Change the Rx scan duty cycle to active at start of stream */
                EnableHighDutyScanMode(TRUE);
                /* Set the source device ID as the stream target device */
                StreamStartSender(p_event->common_data.source_id);
                tx_stream_offset = 0;
                /* Set the stream code to CSR_DEVICE_INFO_RSP */
                device_info[0] = CSR_DEVICE_INFO_RSP;
                /* Start sending */
                sendNextPacket();
            }
            break;

            case CSR_DEVICE_INFO_RESET:
            {
                /* Reset the device info */
                device_info_length = sizeof(DEVICE_INFO_STRING);
                device_info[0] = CSR_DEVICE_INFO_RSP;
                device_info[1] = device_info_length;
                MemCopy(&device_info[2], DEVICE_INFO_STRING,
                                                    sizeof(DEVICE_INFO_STRING));
            }
            break;//这两分支与上个数据块指示函数处理相同

            case CSR_DEVICE_INFO_SET:
            {
                /* CSR_DEVICE_INFO_SET is received. Store the code, length and
                 * the data into the device_info array in the format received
                 */
                current_stream_code = CSR_DEVICE_INFO_SET;
                device_info_length = p_event->data[1];
                MemCopy(device_info, p_event->data, p_event->data_len);
                rx_stream_offset = p_event->data_len;
                /* Change the Rx scan duty cycle to active at start of stream */
                EnableHighDutyScanMode(TRUE);
            }
            break;
            
            /*case REQUEST_DATA://target as sender
            {
                devid_bridge=p_event->common_data.source_id;
                if(flagCustomReqDataRec==FALSE)
                {
                    //sendbackSensorData1Cfm=FALSE;
                    //sendbackSensorData2Cfm=FALSE;//RESET
                    
                    //PioSetMode(LED_PIO_GREEN,pio_mode_pwm1);
                    //PioEnablePWM(LED_PWM_GREEN,TRUE);
                    //PioConfigPWM(LED_PWM_GREEN,pio_pwm_mode_push_pull,50,0,20,0,192,20,0);
                    isRemote=TRUE;
                    BME280_Trigger();
                    TimerCreate(10*MILLISECOND,TRUE,timerHandleReadBME280Data);
                    TimerCreate(3*SECOND,TRUE,debounceTimerHandlerCUSTOMREQDATA);
                    flagCustomReqDataRec=TRUE;//locked
                    //sensorDataReady=0;
                }
            }
            break;*/
            
            /*case RECEIVE_DATA1://bridge as receiver
            {
                if(sourceDevId==targetDevID)
                {
                    uint8 recvdSensorData1[7];
                    MemCopy(recvdSensorData1,p_event->data+1,p_event->data_len);
                    bleTestServData.temp=recvdSensorData1[0]|(((uint16)recvdSensorData1[1])<<8);
                    bleTestServData.humi=recvdSensorData1[2]|(((uint16)recvdSensorData1[3])<<8);
                    bleTestServData.pressure=recvdSensorData1[4]|(((uint16)recvdSensorData1[5])<<8)|
                                             (((uint24)recvdSensorData1[6])<<16);
                    sensorDataReady|=0x01;
                }
            }
            break;
            
            case RECEIVE_DATA2:
            {
                if(sourceDevId==targetDevID)
                {
                    uint8 recvdSensorData2[3];
                    MemCopy(recvdSensorData2,p_event->data+1,p_event->data_len);
                    bleTestServData.currentVal=
                            recvdSensorData2[0]|(((uint16)recvdSensorData2[1])<<8);//update to bridge
                    //bleTestServData.batLevel=recvdSensorData2[2];
                    sensorDataReady|=0x02;
                }
            }
            break;*/
        
            default:
            break;
        }
        /*if(sensorDataReady==0x03)
        {
            PioEnablePWM(2,FALSE);//LED_PWM_RED
            PioSetMode(LED_PIO_IND,pio_mode_pwm2);            
            PioConfigPWM(2,pio_pwm_mode_push_pull,1,200,1,200,1,1,5);
            PioEnablePWM(2,TRUE);
            sensorDataComplete=0x01;
            //TimerCreate(3*SECOND,TRUE,recvdDevidCmdTimerHandler);//FromControlDevice             
            waitSensorDatarRead_tid=
                TimerCreate(0*SECOND,TRUE,waitSensorDataReadTimerHandler);
        }*/
    }
    else
    {
        if( current_stream_code == CSR_DEVICE_INFO_SET
            && rx_stream_offset + p_event->data_len < sizeof(device_info) )
        {
            MemCopy(&device_info[rx_stream_offset], p_event->data,
                                                            p_event->data_len);
            rx_stream_offset += p_event->data_len;
        }

        /* No other CODE is handled currently */
    }
}
void waitSensorDataReadTimerHandler(timer_id tid)
{
    TimerDelete(waitSensorDatarRead_tid);
    waitSensorDatarRead_tid=TIMER_INVALID;
    TimerDelete(tid);tid=TIMER_INVALID;
    if( (dataReadyNotifyCount>0) &&
        (bleTestServData.dataReadyCfg==gatt_client_config_notification) )
    {
        GattCharValueNotification(CONNECTION_CID,HANDLE_DATA_READY,
                            sizeof(sensorDataComplete),&sensorDataComplete);//0x01
        dataReadyNotifyCount--;                                
        waitSensorDatarRead_tid=
            TimerCreate(10*SECOND,TRUE,waitSensorDataReadTimerHandler);        
    }
}
void debounceTimerHandlerCUSTOMREQDATA(timer_id tid)
{
    TimerDelete(tid);
    tid=TIMER_INVALID;
    //PioEnablePWM(LED_PWM_GREEN,FALSE);
    //PioSetMode(LED_PIO_GREEN,pio_mode_user);
    //PioSet(LED_PIO_GREEN,1);
    SleepModeChange(sleep_mode_deep);
    flagCustomReqDataRec=FALSE;    
}
void recvdDevidCmdTimerHandler(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
    PioEnablePWM(LED_PWM_RED,FALSE);//receive DevID indication LED:OFF
    PioSetMode(LED_PIO_RED,pio_mode_user);
    PioSet(LED_PIO_RED,1);    
}
/*-----------------------------------------------------------------------------*
 *  NAME
 *      handleCSRmeshDataStreamSendCfm
 *
 *  DESCRIPTION
 *      This function handles the CSR_MESH_DATA_STREAM_SEND_CFM message
 *
 *  RETURNS
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
extern void handleCSRmeshDataStreamSendCfm(CSR_MESH_STREAM_EVENT_T *p_event)
{
    /* Send next block if it is not end of string */
    //sendNextPacket();
    streamCFM=TRUE;
        streamSendRetryCount = 0; //reset send retry counter
        streamSendRetryCountCheck = 0;
        TimerDelete(stream_send_retry_tid);
        stream_send_retry_tid=TIMER_INVALID;
    /*if(whichStage==1)
    {
        whichStage=0;//reset
        TimerCreate(2*SECOND,TRUE,sendSensorDataTimerHandler);
    }*/
        
    /*switch(whichProcess)
    {
        case 1:        
            requestSensorDataCfm=TRUE;
        break;
        
        case 2:
            sendbackSensorData1Cfm=TRUE;           
            TimerCreate(2*SECOND+retDataCount*60*MILLISECOND,
                        TRUE,sendSensorDataTimerHandler);//next
            retDataCount=50;
            whichProcess=3;              
            //SleepModeChange(sleep_mode_deep);           
        break;
        
        case 3:
            sendbackSensorData2Cfm=TRUE;
        break;
    }*/
            
}
#endif /* ENABLE_DATA_MODEL */
#if 0
void sendSensorDataTimerHandler(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
    //uint8 measureSensorData2[4];
    dat[0]=RECEIVE_DATA2;
    dat[1]= ampere_value&0xff;
    dat[2]=(ampere_value>>8)&0xff;
    dat[3]=ReadBatteryLevel();//update
    sendDataStreamWithCFM(4,devid_bridge,15);    
    /*if(retDataCount>0)
    {
        if(sendbackSensorData2Cfm==FALSE)
        {            
            StreamStartSender(devid_bridge);
            StreamSendData(measureSensorData2,4);
            //StreamFlush();
            TimerCreate(60*MILLISECOND,TRUE,sendSensorDataTimerHandler);
            retDataCount--;
        }
        else TimerCreate(0*SECOND,TRUE,forceIntoSleepTimerHandler);
    }
    else TimerCreate(0*SECOND,TRUE,forceIntoSleepTimerHandler);*/
}
#endif

void forceIntoSleepTimerHandler(timer_id tid)   //current=5.1uA~7.7uA
{
    TimerDelete(tid);tid=TIMER_INVALID;  

    /*CsrMeshEnableListening(FALSE);    
    CSR_MESH_ADVSCAN_PARAM_T param;//20160831
    CsrMeshGetAdvScanParam(&param);
    param.scan_duty_cycle = OFF_RX_DUTY_CYCLE;
    CsrMeshSetAdvScanParam(&param);*/
    
    PioSetModes( (1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|
               (1L<<SPI_MASTER_PIO_MISO),pio_mode_user );
    PioSetDirs( (1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|(1L<<SPI_MASTER_PIO_MISO),
               (1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|(1L<<SPI_MASTER_PIO_MISO) );
    PioSetPullModes((1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|(1L<<SPI_MASTER_PIO_MISO),
                    pio_mode_strong_pull_up);      
    SleepModeChange(sleep_mode_deep);    
}