/*****************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2015
 *  CSR Bluetooth Low Energy CSRmesh 1.3 Release
 *  Application version 1.3
 *
 *  FILE
 *      csr_mesh_tempsensor.h
 *
 *  DESCRIPTION
 *      This file defines a simple implementation of CSRmesh
 *      Temperature Sensor device
 *
 *****************************************************************************/

#ifndef __CSR_MESH_TEMPSENSOR_H__
#define __CSR_MESH_TEMPSENSOR_H__

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/

#include <types.h>
#include <bt_event_types.h>
#include <timer.h>

/*============================================================================*
 *  Local Header File
 *============================================================================*/
#ifdef ENABLE_BATTERY_MODEL
#include <battery_model.h>
#endif /* ENABLE_BATTERY_MODEL */

#ifdef ENABLE_FIRMWARE_MODEL
#include <firmware_model.h>
#endif

#include "csr_mesh_tempsensor_gatt.h"
#include <bearer_model.h>
#include <sensor_model.h>
#include <actuator_model.h>

/*============================================================================*
 *  Public Definitions
 *============================================================================*/
#define PRESSED     0
#define RELESED     1
/*============================================================================*
 *  Public Data Types
 *============================================================================*/
typedef enum
{
    sensor_status_init=0x00,
    sensor_status_init_pressed=0x01,
    sensor_status_register=0x02,
    sensor_status_normal=0x03,
    sensor_status_error=0x04,
}  sensor_status;/**/
sensor_status current_sensor_status;

/* CSRmesh device association state */
typedef enum
{

    /* Application Initial State */
    app_state_not_associated = 0,

    /* Application state association started */
    app_state_association_started,

    /* Application state associated */
    app_state_associated,

} app_association_state;


typedef enum
{
    /* Application Initial State */
    app_state_init = 0,

    /* Enters when Undirected advertisements are configured */
    app_state_advertising,

    /* Enters when device is not connected and not advertising */
    app_state_idle,

    /* Enters when connection is established with the host */
    app_state_connected,

    /* Enters when disconnect is initiated by the application */
    app_state_disconnecting,

    app_state_sleep    

} app_state;

/* GATT Service Data Structure */
typedef struct
{
    /* TYPED_BD_ADDR_T of the host to which device is connected */
    TYPED_BD_ADDR_T                con_bd_addr;

    /* Track the UCID as Clients connect and disconnect */
    uint16                         st_ucid;

    /* Boolean flag indicates whether the device is temporary paired or not */
    bool                           paired;

    /* Value for which advertisement timer needs to be started */
    uint32                         advert_timer_value;

    /* Store timer id while doing 'UNDIRECTED ADVERTS' */
    timer_id                       app_tid;

    /* Variable to store the current connection interval being used. */
    uint16                         conn_interval;

    /* Variable to store the current slave latency. */
    uint16                         conn_latency;

    /* Variable to store the current connection time-out value. */
    uint16                         conn_timeout;

    /* Variable to keep track of number of connection
     * parameter update requests made.
     */
    uint8                          num_conn_update_req;

    /* Store timer id for Connection Parameter Update timer in Connected
     * state
     */
    timer_id                       con_param_update_tid;

    /* Connection Parameter Update timer value. Upon a connection, it's started
     * for a period of TGAP_CPP_PERIOD, upon the expiry of which it's restarted
     * for TGAP_CPC_PERIOD. When this timer is running, if a GATT_ACCESS_IND is
     * received, it means, the central device is still doing the service discov-
     * -ery procedure. So, the connection parameter update timer is deleted and
     * recreated. Upon the expiry of this timer, a connection parameter update
     * request is sent to the central device.
     */
    uint32                         cpu_timer_value;

} APP_GATT_SERVICE_DATA_T;

/* CSRmesh Temperature Sensor application data structure */
typedef struct
{
    /* Application GATT data */
    APP_GATT_SERVICE_DATA_T        gatt_data;

    /* Current state of application */
    app_state                       state;

    /* CSRmesh Association State */
    app_association_state           assoc_state;

    /* CSRmesh transmit device id advertise timer id */
    timer_id                       mesh_device_id_advert_tid;

    /* Local Device's Random Bluetooth Device Address. */
#ifdef USE_STATIC_RANDOM_ADDRESS
    BD_ADDR_T                      random_bd_addr;
#endif /* USE_STATIC_RANDOM_ADDRESS */

    /* Firmware Version Information */
#ifdef ENABLE_FIRMWARE_MODEL
    FIRMWARE_MODEL_STATE_DATA_T    fw_version;
#endif

    /* Battery Level Data */
#ifdef ENABLE_BATTERY_MODEL
    BATTERY_MODEL_STATE_DATA_T     battery_data;
#endif

    /* Bearer Model Data */
    BEARER_MODEL_STATE_DATA_T      bearer_data;

    /* Sensor Model Data. */
    SENSOR_MODEL_STATE_DATA_T      sensor_data;//封装有值得多个SENSOR_STATE_DATA_T结构体

    /* Actuator Model Data. */
    ACTUATOR_MODEL_STATE_DATA_T    actuator_data;

} CSRMESH_TEMPSENSOR_DATA_T;


/*============================================================================*
 *  Public Data
 *============================================================================*/

/* CSRmesh Temperature Sensor application specific data */
extern CSRMESH_TEMPSENSOR_DATA_T g_tsapp_data;
extern uint16 bridgeFlagNvm;
/*============================================================================*
 *  Public Function Prototypes
 *============================================================================*/
void timerHandleReadBME280Data(timer_id tid);
uint16 getDevid(void);
void saveSensorStatusToNvm(uint16 sensorStatus);
void saveBridgeFlagToNVM(uint16 bridgeFlag);

/* This function is used to set the state of the application */
extern void AppSetState(app_state new_state);

/* This function enables/disables the active scan mode */
extern void EnableHighDutyScanMode(bool enable);
#endif /* __CSR_MESH_TEMPSENSOR_H__ */

