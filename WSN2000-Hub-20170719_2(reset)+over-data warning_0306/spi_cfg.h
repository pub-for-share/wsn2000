/******************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2013-2015
 *  Part of CSR uEnergy SDK 2.4.5
 *  Application version 2.4.5.0
 *
 *  FILE
 *      spi_cfg.h
 *
 *  DESCRIPTION
 *      Configure the SPI Master implementation.
 *
 ******************************************************************************/

#ifndef __SPI_CFG_H__
#define __SPI_CFG_H__

#define PIO_CTRLR_CODE_ADDR0    ((uint16*)&pio_ctrlr_spi_master_0)
#define PIO_CTRLR_CODE_ADDR1    ((uint16*)&pio_ctrlr_spi_master_1)
#define PIO_CTRLR_CODE_ADDR2    ((uint16*)&pio_ctrlr_spi_master_2)
#define PIO_CTRLR_CODE_ADDR3    ((uint16*)&pio_ctrlr_spi_master_3)

/* Pack two octets into a 16-bit word
 *
 * PARAMETERS
 *      LSB         Least significant octet
 *      MSB         Most significant octet
 */
#define PACK(LSB,MSB)         ((((uint16)((MSB)& 0xFF)) << 8U) | ((LSB) & 0xFF))

#define LED_ON  1
#define LED_OFF 0

#define ODD     TRUE
#define EVEN    FALSE
#define ON      TRUE
#define OFF     FALSE
/** PIO Used for SSEL
 *
 * PIOs 0-7 map to P0.0 to P0.7
 *
 * PIOs 8-15 map to P1.0 to P1.7
 *
 * PIOs 16-23 map to P2.0 to P2.7
 *
 * PIOs 24-31 map to P3.0 to P3.7
 */
#define SPI_MASTER_PIO_SSEL_PCF2123                         0//(12U)
#define SPI_MASTER_PIO_SSEL_BME280                          9

/** PIO Used for SCLK
 *
 * PIOs 0-7 map to P0.0 to P0.7
 *
 * PIOs 8-15 map to P1.0 to P1.7
 *
 * PIOs 16-23 map to P2.0 to P2.7
 *
 * PIOs 24-31 map to P3.0 to P3.7
 *
 * NOTE: If this setting is changed, be sure to change in the PIO controller
 * code as well.
 */
#define SPI_MASTER_PIO_SCLK                         4//(9U)

/** PIO Used for MOSI
 *
 * PIOs 0-7 map to P0.0 to P0.7
 *
 * PIOs 8-15 map to P1.0 to P1.7
 *
 * PIOs 16-23 map to P2.0 to P2.7
 *
 * PIOs 24-31 map to P3.0 to P3.7
 *
 * NOTE: If this setting is changed, be sure to change in the PIO controller
 * code as well.
 */
#define SPI_MASTER_PIO_MOSI                         (10U)

/** PIO Used for MISO
 *
 * PIOs 0-7 map to P0.0 to P0.7
 *
 * PIOs 8-15 map to P1.0 to P1.7
 *
 * PIOs 16-23 map to P2.0 to P2.7
 *
 * PIOs 24-31 map to P3.0 to P3.7
 *
 * NOTE: Appropriate pull-up/pull-down must be configured by the application.
 *
 * NOTE: If this setting is changed, be sure to change in the PIO controller
 * code as well.
 */
#define SPI_MASTER_PIO_MISO                         (11U)

/** Configure SPI SSEL line to be active low (TRUE) or active high (FALSE)
 */
#define SPI_MASTER_SSEL_ACTIVE_LOW                  (TRUE)

/** Symbol set to the memory address containing PIO controller SPI Master
 * MODE-0 implementation. This symbol is included by linking
 * with libspi_master.a. This symbol needs to be passed to the function
 * #SpiMasterInit
 */
extern void pio_ctrlr_spi_master_0(void);

/** Symbol set to the memory address containing PIO controller SPI Master
 * MODE-1 implementation. This symbol is included by linking
 * with libspi_master.a. This symbol needs to be passed to the function
 * #SpiMasterInit
 */
extern void pio_ctrlr_spi_master_1(void);

/** Symbol set to the memory address containing PIO controller SPI Master
 * MODE-2 implementation. This symbol is included by linking
 * with libspi_master.a. This symbol needs to be passed to the function
 * #SpiMasterInit
 */
extern void pio_ctrlr_spi_master_2(void);

/** Symbol set to the memory address containing PIO controller SPI Master
 * MODE-3 implementation. This symbol is included by linking
 * with libspi_master.a. This symbol needs to be passed to the function
 * #SpiMasterInit
 */
extern void pio_ctrlr_spi_master_3(void);

void spiTransfer(uint16 device_type,uint16 *p_pio_controller_code_addr,uint16 length);

void cmdArrayCat(uint16 *arr,uint16 len);//20161208
void clearSpiCmdAndLen(void);
uint16 checkCmdLength(void);//20161214
#endif /* __SPI_CFG_H__ */
