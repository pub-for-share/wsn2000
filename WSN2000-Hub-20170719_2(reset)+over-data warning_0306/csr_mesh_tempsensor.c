/******************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2015
 *  CSR Bluetooth Low Energy CSRmesh 2.0 Release
 *  Application version 2.0
 *
 *  FILE
 *      csr_mesh_tempsensor.c
 *
 *  DESCRIPTION
 *      This file defines a CSRmesh Temperature Sensor application
 *
 *****************************************************************************/

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/

#include <main.h>
#include <pio.h>
#include <aio.h>
#include <mem.h>
#include <Sys_events.h>
#include <Sleep.h>
#include <timer.h>
#include <security.h>
#include <gatt.h>
#include <gatt_prim.h>
#include <panic.h>
#include <nvm.h>
#include <buf_utils.h>
#include <random.h>
#include <config_store.h>
#include <debug.h>
#include <reset.h>
/*============================================================================*
 *  CSRmesh Header Files
 *============================================================================*/
#include <csr_mesh.h>
#include <bearer_model.h>
#include <sensor_model.h>
#include <attention_model.h>
#include <actuator_model.h>

/*============================================================================*
 *  Local Header Files
 *============================================================================*/
#include "user_config.h"
#include "app_debug.h"
#include "app_gatt.h"
#include "mesh_control_service.h"
#include "ble_test_service.h"
#include "gap_service.h"
#include "csr_mesh_tempsensor.h"
#include "csr_mesh_tempsensor_gatt.h"
#include "app_gatt_db.h"
#include "nvm_access.h"
#include "iot_hw.h"
#include "tempsensor_hw.h"
#include "appearance.h"
#include "battery_hw.h"
#include "spi_transfer.h"

#ifdef ENABLE_DATA_MODEL
#include "app_data_stream.h"
#endif /* ENABLE_DATA_MODEL */

#if defined (ENABLE_GATT_OTA_SERVICE) || defined (ENABLE_FIRMWARE_MODEL)
#include <csr_ota.h>
#include "csr_ota_service.h"
#include "gatt_service.h"
#endif /* ENABLE_GATT_OTA_SERVICE || ENABLE_FIRMWARE_MODEL */

#ifdef ENABLE_FIRMWARE_MODEL
#include <firmware_model.h>
#endif

#ifdef ENABLE_BATTERY_MODEL
#include "battery_model.h"
#endif /* ENBLE_BATTERY_MODEL */

/*============================================================================*
 *  Private Data Types
 *============================================================================*/
#ifdef ENABLE_ACK_MODE
typedef struct 
{
    uint16 dev_id;
    bool ack_recvd;
    uint16 no_response_count;
}HEATER_INFO_T;
#endif /* ENABLE_ACK_MODE */
/*============================================================================*
 *  Private Definitions
 *============================================================================*/
/* Maximum number of timers */
#define MAX_APP_TIMERS                 (13 + MAX_CSR_MESH_TIMERS + \
                                        NUM_TEMP_SENSOR_TIMERS)//库:6

/* Absolute Difference of two numbers. */
#define ABS_DIFF(x,y) (((x) > (y))?((x) - (y)):((y) - (x)))

/* CSRmesh device UUID size */
#define DEVICE_UUID_SIZE_WORDS         (8)

/* CSRmesh Authorization Code Size in Words */
#define DEVICE_AUTHCODE_SIZE_IN_WORDS  (4)//64位

/* Default device UUID */
/*#define DEFAULT_UUID                   {0x3210, 0x7654, 0xBA98, 0xFEDC,\
                                         0xCDEF, 0x89AB, 0x4567, 0x0123}*/
#define DEFAULT_UUID                   {0x8213, 0x7777, 0x6666, 0x5555,\
                                        0x4444, 0x3333, 0x2222, 0x1111}

/* Default Authorisation code */
#define DEFAULT_AUTH_CODE              {0xCDEF, 0x89AB, 0x4567, 0x0123}

/* CS Key for mesh advertising interval */
#define CSKEY_INDEX_CSRMESH_ADV_INTERVAL \
                                       (0)

/* CS Key for mesh advertising time */
#define CSKEY_INDEX_CSRMESH_ADV_TIME   (1)

/* CS Key for user flags */
#define CSKEY_INDEX_USER_FLAGS         (2)

/* Used for generating Random UUID */
#define RANDOM_UUID_ENABLE_MASK        (0x0001)

/* Used for permanently Enabling/Disabling Relay */
#define RELAY_ENABLE_MASK              (0x0002)

/* Used for permanently Enabling/Disabling Bridge */
#define BRIDGE_ENABLE_MASK             (0x0004)

/* Advertisement Timer for sending device identification */
#define DEVICE_ID_ADVERT_TIMER_ID      (5 * SECOND)

/* Slave device is not allowed to transmit another Connection Parameter
 * Update request till time TGAP(conn_param_timeout). Refer to section 9.3.9.2,
 * Vol 3, Part C of the Core 4.0 BT spec. The application should retry the
 * 'connection parameter update' procedure after time TGAP(conn_param_timeout)
 * which is 30 seconds.
 */
#define GAP_CONN_PARAM_TIMEOUT         (30 * SECOND)


/* TGAP(conn_pause_peripheral) defined in Core Specification Addendum 3 Revision
 * 2. A Peripheral device should not perform a Connection Parameter Update proc-
 * -edure within TGAP(conn_pause_peripheral) after establishing a connection.
 */
#define TGAP_CPP_PERIOD                (5 * SECOND)

/* TGAP(conn_pause_central) defined in Core Specification Addendum 3 Revision 2.
 * After the Peripheral device has no further pending actions to perform and the
 * Central device has not initiated any other actions within TGAP(conn_pause_ce-
 * -ntral), then the Peripheral device may perform a Connection Parameter Update
 * procedure.
 */
#define TGAP_CPC_PERIOD                (1 * SECOND)

#ifdef ENABLE_FIRMWARE_MODEL
/* OTA Reset Defer Duration */
#define OTA_RESET_DEFER_DURATION       (500 * MILLISECOND)
#endif

/* Magic value to check the sanity of NVM region used by the application. This 
 * value should be unique for each application as the NVM layout changes for
 * every application.
 */
#define NVM_SANITY_MAGIC               (0xAB86)

/*Number of IRKs that application can store */
#define MAX_NUMBER_IRK_STORED          (1)

/* NVM offset for the application NVM version */
#define NVM_OFFSET_APP_NVM_VERSION     (0)                              //f800

/* NVM offset for NVM sanity word */
#define NVM_OFFSET_SANITY_WORD         (NVM_OFFSET_APP_NVM_VERSION + 1) //f802

/* NVM offset for NVM device uuid */
#define NVM_OFFSET_DEVICE_UUID         (NVM_OFFSET_SANITY_WORD + 1)     //f804

/* NVM Offset for Authorization Code */
#define NVM_OFFSET_DEVICE_AUTHCODE     (NVM_OFFSET_DEVICE_UUID + \
                                        DEVICE_UUID_SIZE_WORDS)         //f814

#define NVM_OFFSET_NETWORK_KEY         (NVM_OFFSET_DEVICE_AUTHCODE + \
                                        sizeof(CSR_MESH_AUTH_CODE_T))   //f81c

#define NVM_OFFSET_DEVICE_ID           (NVM_OFFSET_NETWORK_KEY + \
                                        sizeof(CSR_MESH_NETWORK_KEY_T)) //f82c

#define NVM_OFFSET_BRIDGE_FLAG         (NVM_OFFSET_DEVICE_ID + 1)       //f82e
#define NVM_OFFSET_SENSOR_STATUS       (NVM_OFFSET_BRIDGE_FLAG+1)       //F830

#define NVM_OFFSET_SEQUENCE_NUMBER     (NVM_OFFSET_SENSOR_STATUS + 1)

#define NVM_OFFSET_DEVICE_ETAG         (NVM_OFFSET_SEQUENCE_NUMBER + 2)

#define NVM_OFFSET_ASSOCIATION_STATE   (NVM_OFFSET_DEVICE_ETAG + \
                                        sizeof(CSR_MESH_ETAG_T))

/* NVM Offset for Sensor Model Groups. */
#define NVM_OFFSET_SENSOR_ACTUATOR_MODEL_GROUPS \
                                       (NVM_OFFSET_ASSOCIATION_STATE + 1)

/* NVM Offset for Attention Model Groups. */
#define NVM_OFFSET_ATTENTION_MODEL_GROUPS \
                                    (NVM_OFFSET_SENSOR_ACTUATOR_MODEL_GROUPS + \
                                    sizeof(sensor_actuator_model_groups))

#ifdef ENABLE_DATA_MODEL                                        
#define NVM_OFFSET_DATA_MODEL_GROUPS \
                                       (NVM_OFFSET_ATTENTION_MODEL_GROUPS + \
                                        sizeof(attention_model_groups))

/* NVM Offset for the Bearer State Data. */
#define NVM_BEARER_DATA_OFFSET         (NVM_OFFSET_DATA_MODEL_GROUPS + \
                                        sizeof(data_model_groups))

#else

/* NVM Offset for the Bearer State Data. */
#define NVM_BEARER_DATA_OFFSET         (NVM_OFFSET_ATTENTION_MODEL_GROUPS + \
                                        sizeof(attention_model_groups))
#endif /* ENABLE_DATA_MODEL */

/* NVM Offset for Sensor State Data */
#define NVM_SENSOR_STATE_OFFSET        (NVM_BEARER_DATA_OFFSET + \
                                        sizeof(BEARER_MODEL_STATE_DATA_T))

/* Size of Sensor State data to be stored in NVM */
#define SENSOR_SAVED_STATE_SIZE        (2 * sizeof(uint16))

/* Get NVM Offset of a sensor from it's index. */
#define GET_SENSOR_NVM_OFFSET(idx)     (NVM_SENSOR_STATE_OFFSET + \
                                        ((idx) * (SENSOR_SAVED_STATE_SIZE)))

/* NVM Offset for Application data */
#define NVM_MAX_APP_MEMORY_WORDS       (NVM_SENSOR_STATE_OFFSET + \
                                        (NUM_SENSORS_SUPPORTED * \
                                         SENSOR_SAVED_STATE_SIZE))

/* Time for which the button bounce is expected */
#define BUTTON_DEBOUNCE_TIME           (20 * MILLISECOND)

/* Timer to check if button was pressed for 1 second */
#define BUTTON_ONE_SEC_PRESS_TIME      (1 * SECOND)

#define KEY_PRESSED                    (TRUE)

#define KEY_RELEASED                   (FALSE)

/* Number of Supported Sensors. */
#define NUM_SENSORS_SUPPORTED          (2)

/* Desired Air temperature sensor Index */
#define DESIRED_AIR_TEMP_IDX           (1)

/* Current Air temperature sensor Index */
#define CURRENT_AIR_TEMP_IDX           (0)
#define CURRENT_AIR_HUMI_IDX           (1)
//#define CURRENT_AIR_PRESSURE_LOW       (2)
//#define CURRENT_AIR_PRESSURE_HIGH      (3)
//#define CURRENT_CURRENT_VALUE_IDX      (4)

/* Increment/Decrement Step for Button Press. */
#define STEP_SIZE_PER_BUTTON_PRESS     (32)

/* Macro to convert Celsius to 1/32 kelvin units. */
#define CELSIUS_TO_BY32_KELVIN(x)      (((x)*32) + CELSIUS_TO_KELVIN_FACTOR)

/* Max. Temperature. */
#define MAX_DESIRED_TEMPERATURE        (CELSIUS_TO_BY32_KELVIN(40))

/* Min. Temperature. */
#define MIN_DESIRED_TEMPERATURE        (CELSIUS_TO_BY32_KELVIN(-5))

/* Max sensor type supp in this app sensor_type_desired_air_temperature = 3*/
#define SENSOR_TYPE_SUPPORTED_MAX      (sensor_type_desired_air_temperature)

/* Max transmit msg density */
#define MAX_TRANSMIT_MSG_DENSITY       (5)

#ifdef ENABLE_ACK_MODE
/* The broadcast id for MESH is defined as 0 */
#define CSR_MESH_BROADCAST_ID          (0)

/* The maximum number of heater devices that in grp */
#define MAX_HEATERS_IN_GROUP           (5)

/* The heater will be removed from the added list if it does not respond to 
 * maximim no response count times.
 */
#define MAX_NO_RESPONSE_COUNT          (5)
#endif /* ENABLE_ACK_MODE */

/*============================================================================*
 *  Public Data
 *============================================================================*/

/* CSRmesh Temperature Sensor application data instance */
CSRMESH_TEMPSENSOR_DATA_T g_tsapp_data;

/* CSRmesh network related node data */
CSR_MESH_NODE_DATA_T g_node_data ={ 
    .device_uuid.uuid = DEFAULT_UUID,
    .auth_code.auth_code = DEFAULT_AUTH_CODE,
};

/* Application VID,PID and Version. */
CSR_MESH_VID_PID_VERSION_T vid_pid_info =
{
    .vendor_id  = APP_VENDOR_ID,
    .product_id = APP_PRODUCT_ID,
    .version    = APP_VERSION,
};

/* Attention timer id */
static timer_id attn_tid = TIMER_INVALID;

#ifdef ENABLE_FIRMWARE_MODEL
/* Firmware Reset Delay Timer Id */
static timer_id ota_rst_tid = TIMER_INVALID;
#endif /* ENABLE_FIRMWARE_MODEL */

/* Device Apprearance. */
CSR_MESH_APPEARANCE_T device_appearance = {APPEARANCE_ORG_BLUETOOTH_SIG,
                                         APPEARANCE_CSR_MESH_TEMP_SENSOR_VALUE};

/* Device Short name */
uint8 short_name[9] = "Sensor";


uint16 dataReadyNotifyCount=0;
uint16 bridgeFlagNvm=0xffff;
uint16 ledFlashtid;
bool isMesh;
/*============================================================================*
 *  Private Data
 *============================================================================*/
/* Declare space for application timers. */
static uint16 app_timers[SIZEOF_APP_TIMER * MAX_APP_TIMERS];

/* Sensor Model Data */
static SENSOR_STATE_DATA_T sensor_state[NUM_SENSORS_SUPPORTED];

/* Actuator Model Data */
static ACTUATOR_STATE_DATA_T actuator_state[NUM_SENSORS_SUPPORTED];

#ifdef ENABLE_DATA_MODEL
static uint16 data_model_groups[NUM_DATA_MODEL_GROUPS];
#endif /* ENABLE_DATA_MODEL */

/* Sensor Model Grouping Data. */
static uint16 sensor_actuator_model_groups[NUM_SENSOR_ACTUATOR_MODEL_GROUPS];

/* Attention Model Grouping Data. */
static uint16 attention_model_groups[NUM_ATTENTION_MODEL_GROUPS];

/* Temperature Sensor Sample Timer ID. */
static timer_id tempsensor_sample_tid = TIMER_INVALID;

/* Retransmit Timer ID. */
static timer_id retransmit_tid = TIMER_INVALID;

/* Repeat Interval Timer ID. */
static timer_id repeat_interval_tid = TIMER_INVALID;

/* Write Value Msg Retransmit counter */
//static uint16 write_val_retransmit_count = 0;

/* Last Broadcast temperature in 1/32 kelvin units. */
static SENSOR_FORMAT_TEMPERATURE_T last_bcast_air_temp;

/* Temperature Controller's Current Desired Air Temperature. */
static SENSOR_FORMAT_TEMPERATURE_T current_desired_air_temp;

/* Temperature Controller's Last Broadcast Desired Air Temperature. */
static SENSOR_FORMAT_TEMPERATURE_T last_bcast_desired_air_temp;

/* Sensor Type length Array */
static uint8 sensor_type_length[SENSOR_TYPE_SUPPORTED_MAX + 1] = {0,2,4,2};

/* Retransmit interval based on the msg transmit density 
 * These values are calculated based on the number of tx msgs added in queue.
 * TRANSMIT_MSG_DENSITY = 1 -> 90ms + (random 0-12.8ms) * 5 -> 500ms.(Approx)
 * TRANSMIT_MSG_DENSITY = 2 -> 45ms + (random 0-12.8ms) * 11 -> 700ms.(Approx)
 * TRANSMIT_MSG_DENSITY = 3 -> 30ms + (random 0-12.8ms) * 17 -> 800ms.(Approx)
 * TRANSMIT_MSG_DENSITY = 4 -> 22.5ms + (random 0-12.8ms) * 23 -> 900ms.(Approx)
 * TRANSMIT_MSG_DENSITY = 5 -> 20ms + (random 0-12.8ms) * 35 -> 1100ms.(Approx)
 */
/*static uint32 retransmit_interval[MAX_TRANSMIT_MSG_DENSITY]={500 * MILLISECOND,
                                                             700 * MILLISECOND,
                                                             800 * MILLISECOND,
                                                             900 * MILLISECOND,
                                                            1100 * MILLISECOND};*/

/* To send the MASP associate to NW msg and Dev appearance msg alternatively */
static bool send_dev_appearance = FALSE;

/* store the bearer relay active value during connection and restore it back 
 * after the disconnection
 */
static uint16 bearer_relay_active;

/* store the promiscuous value during connection and restore it back after the 
 * disconnection
 */
static uint16 bearer_promiscuous;

#ifdef ENABLE_ACK_MODE
/* Stores the device info of the heaters participating in the group.*/
static HEATER_INFO_T heater_list[MAX_HEATERS_IN_GROUP];
#endif /* ENABLE_ACK_MODE */

//#ifdef ENABLE_TEMPERATURE_CONTROLLER
/* Temperature Controller Button Debounce Timer ID. */
//static timer_id oneSecTimerId = TIMER_INVALID;

#ifndef DEBUG_ENABLE
/* Toggle switch de-bounce timer id */
//static timer_id debounce_tid = TIMER_INVALID;

static uint16 debounceINT_tid=TIMER_INVALID;

/* Switch Button States. */
//static bool     incButtonState = KEY_RELEASED;
//static bool     decButtonState = KEY_RELEASED;
#endif /* DEBUG_ENABLE */
//#endif /* ENABLE_TEMPERATURE_CONTROLLER */

//uint16 sensorDatIntegrityFlag=0;
uint16 devTest[5]={0x8305,0x830a,0x830c,0x8311,0x8312};
uint16 devTestIndex=0;
uint16 time10sCount=0;
/*============================================================================*
 *  Private Function Prototypes
 *============================================================================*/
static void appDataInit(void);
static void readPersistentStore(void);
static void handleSignalLmEvConnectionComplete(
                                     LM_EV_CONNECTION_COMPLETE_T *p_event_data);
static void handleSignalLmConnectionUpdate(
                                     LM_EV_CONNECTION_UPDATE_T* p_event_data);
static void handleGapCppTimerExpiry(timer_id tid);
static void deviceIdAdvertTimeoutHandler(timer_id tid);
//static void retransmitIntervalTimerHandler(timer_id tid);
//static void startRetransmitTimer(void);
//static void tempSensorSampleIntervalTimeoutHandler(timer_id tid);
static void writeSensorDataToNVM(uint16 idx);
//static void writeTempValue(void);
//static void startTempTransmission(void);
static bool IsSensorConfigured(void);
//static void repeatIntervalTimerHandler(timer_id tid);
//static void startRepeatIntervalTimer (void);

#ifndef ENABLE_TEMPERATURE_CONTROLLER
#ifdef DEBUG_ENABLE
/* handle the desired temp change on uart callback */
static void handleDesiredTempChange(timer_id tid);
/* UART Receive callback */
static uint16 UartDataRxCallback (void* p_data, uint16 data_count,
                                  uint16* p_num_additional_words);
#else
/* Button De-bounce Timer Handler */
//static void handleButtonDebounce(timer_id tid);
/* PIO Event Handler */
static void handlePIOChangedEvent(uint32 pio_changed);
#endif /* DEBUG_ENABLE */
#endif /* ENABLE_TEMPERATURE_CONTROLLER */

#ifdef DEBUG_ENABLE
/* Print a number in decimal. */
//static void printInDecimal(uint32 val);
#endif /* DEBUG_ENABLE */

#ifdef ENABLE_ACK_MODE
static void resetHeaterList(void);
static void resetAckInHeaterList(void);
#endif /* ENABLE_ACK_MODE */
void sendbackSensorDataTimerHandler(timer_id tid);

void turnOffMeshAndSleep(timer_id tid);
void turnOnMesh(timer_id tid);
void handleLEDFlash1S(timer_id tid);
void handleLongButtonPress(timer_id tid);
void countTimeIntervalHandler(timer_id tid);
void sendMeshCmdHandler(timer_id tid);
/*============================================================================*
 *  Private Function Implementations
 *============================================================================*/
void saveSensorStatusToNvm(uint16 sensorStatus)
{
    Nvm_Write((uint16 *)&sensorStatus,1,NVM_OFFSET_SENSOR_STATUS);
}
void saveBridgeFlagToNVM(uint16 bridgeFlag)
{
    Nvm_Write(&bridgeFlag,1,NVM_OFFSET_BRIDGE_FLAG);
}
        
void handleLEDFlash1S(timer_id tid)
{
    TimerDelete(ledFlashtid);
    ledFlashtid=TIMER_INVALID;
    PioEnablePWM(2,FALSE);
    PioSetMode(LED_PIO_IND,pio_mode_user);    
    if(PioGet(LED_PIO_IND)==1) PioSet(LED_PIO_IND,0);//IOToggle
    else PioSet(LED_PIO_IND,1);
    ledFlashtid=TimerCreate(1*SECOND,TRUE,handleLEDFlash1S);
}
uint16 getDevid(void)
{
    uint16 devid=0;
    Nvm_Read(&devid,sizeof(uint16),NVM_OFFSET_DEVICE_ID);
    return devid;
}
uint16 getBridgeFlag()
{
    uint16 bridgeFlag=FALSE;
    Nvm_Read(&bridgeFlag,1,NVM_OFFSET_BRIDGE_FLAG);
    return bridgeFlag;
}
void handleLongButtonPress(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
    if(PioGet(RTC_INT)==FALSE) //if the button still press>5s
    {   
        PioEnablePWM(2,FALSE);
        PioSetMode(LED_PIO_IND,pio_mode_user);        
        PioSet(LED_PIO_IND,0);
 
        uint16 nvm_erase=0xffff;
        Nvm_Write(&nvm_erase, 1, NVM_OFFSET_APP_NVM_VERSION);//in order enter state same as the first time
        WarmReset();
    }    
}        
#ifdef USE_STATIC_RANDOM_ADDRESS
/*-----------------------------------------------------------------------------*
 *  NAME
 *      generateStaticRandomAddress
 *
 *  DESCRIPTION
 *      This function generates a static random address.
 *
 *  RETURNS/MODIFIES
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
static void generateStaticRandomAddress(BD_ADDR_T *addr)
{
    uint16 temp[3];
    uint16 idx = 0;

    if (!addr) return;

    for (idx = 0; idx < 3;)
    {
        temp[idx] = Random16();
        if ((temp[idx] != 0) && (temp[idx] != 0xFFFF))
        {
            idx++;
        }
    }

    addr->lap = ((uint32)(temp[1]) << 16) | (temp[0]);
    addr->lap &= 0x00FFFFFFUL;
    addr->uap = (temp[1] >> 8) & 0xFF;
    addr->nap = temp[2];

    addr->nap &= ~BD_ADDR_NAP_RANDOM_TYPE_MASK;
    addr->nap |=  BD_ADDR_NAP_RANDOM_TYPE_STATIC;
}
#endif /* USE_STATIC_RANDOM_ADDRESS */

#ifdef ENABLE_FIRMWARE_MODEL
/*-----------------------------------------------------------------------------*
 *  NAME
 *      issueOTAReset
 *
 *  DESCRIPTION
 *      This function issues an OTA Reset.
 *
 *  RETURNS/MODIFIES
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
static void issueOTAReset(timer_id tid)
{
    if (ota_rst_tid == tid)
    {
        ota_rst_tid = TIMER_INVALID;

        /* Issue OTA Reset. */
        OtaReset();
    }
}
#endif /* ENABLE_FIRMWARE_MODEL */


#ifdef DEBUG_ENABLE
/*----------------------------------------------------------------------------*
 *  NAME
 *      printInDecimal
 *
 *  DESCRIPTION
 *      This function prints an UNSIGNED integer in decimal.
 *
 *  RETURNS
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
/*static void printInDecimal(uint32 val)
{
    if(val >= 10)
    {
        printInDecimal(val/10);
    }
        DebugWriteChar(('0' + (val%10)));
}*/
#else
#define printInDecimal(n)
#endif /* DEBUG_ENABLE */

#ifdef ENABLE_TEMPERATURE_CONTROLLER
#ifdef DEBUG_ENABLE
/*----------------------------------------------------------------------------*
 *  NAME
 *      handleDesiredTempChange
 *
 *  DESCRIPTION
 *      This function handles the case when desired temperature is changed 
 *      through uart.
 *
 *  RETURNS
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
static void handleDesiredTempChange(timer_id tid)
{
    if (tid == oneSecTimerId)
    {
        oneSecTimerId = TIMER_INVALID;
        /* Check if the current desired value of temperature is,
         * different from last broadcast value.
         */
        if (current_desired_air_temp != last_bcast_desired_air_temp)
        {
            /* Set last Broadcast Temperature to current temperature. */
            last_bcast_desired_air_temp = current_desired_air_temp;

            startTempTransmission();
        }

        /* Display Temperature. */

        //DEBUG_STR(" USER CHANGED DESIRED TEMPERATURE : ");
        //printInDecimal(current_desired_air_temp/32);
        //DEBUG_STR(" kelvin\r\n");
    }
}

/*----------------------------------------------------------------------------*
 * NAME
 *    UartDataRxCallback
 *
 * DESCRIPTION
 *     This callback is issued when data is received over UART. Application
 *     may ignore the data, if not required. For more information refer to
 *     the API documentation for the type "uart_data_out_fn"
 *
 * RETURNS
 *     The number of words processed, return data_count if all of the received
 *     data had been processed (or if application don't care about the data)
 *
 *----------------------------------------------------------------------------*/
static uint16 UartDataRxCallback ( void* p_data, uint16 data_count,
        uint16* p_num_additional_words )
{
    bool change = FALSE;
    uint8 *byte = (uint8 *)p_data;

    /* Application needs 1 additional data to be received */
    *p_num_additional_words = 1;

    /* If device is not associated, return. */
    if (g_tsapp_data.assoc_state != app_state_associated)
    {
        return data_count;
    }

    switch(byte[0])
    {
        case '+':
        {
            if (current_desired_air_temp < 
                (MAX_DESIRED_TEMPERATURE - STEP_SIZE_PER_BUTTON_PRESS) &&
                IsSensorConfigured())
            {
                change = TRUE;
                current_desired_air_temp += STEP_SIZE_PER_BUTTON_PRESS;
                //DEBUG_STR("+ 1 kelvin\r\n");
            }
        }
        break;

        case '-':
        {
            if (current_desired_air_temp > 
                (MIN_DESIRED_TEMPERATURE + STEP_SIZE_PER_BUTTON_PRESS) &&
                IsSensorConfigured())
            {
                change = TRUE;
                current_desired_air_temp -= STEP_SIZE_PER_BUTTON_PRESS;
                //DEBUG_STR("- 1 kelvin\r\n");
            }
        }
        break;

        default:
        break;
    }

    if (change)
    {
        TimerDelete(oneSecTimerId);
        oneSecTimerId = TimerCreate(BUTTON_ONE_SEC_PRESS_TIME, TRUE,
                                          handleDesiredTempChange);
    }

    return data_count;
}

#else /* DEBUG_ENABLE */
#if 0
    /*----------------------------------------------------------------------------*
     *  NAME
     *      handleButtonDebounce
     *
     *  DESCRIPTION
     *      This function handles De-bounce Timer Events.
     *
     *  RETURNS
     *      Nothing.
     *
     *---------------------------------------------------------------------------*/
    static void handleButtonDebounce(timer_id tid)
    {
        bool startOneSecTimer = FALSE;
        bool update_nvm = FALSE;
    
        if( tid == debounce_tid)
        {
            debounce_tid = TIMER_INVALID;
    
            /* Enable PIO Events again */
            PioSetEventMask(BUTTONS_BIT_MASK, pio_event_mode_both);
    
            /* If PIO State is same as before starting de-bounce timer,
             * we have a valid event.
             */
            if ((PioGet(SW3_PIO) == FALSE) && (incButtonState == KEY_RELEASED))
            {
                /* Set State and increment level */
                incButtonState = KEY_PRESSED;
                /* Start 1 second timer */
                startOneSecTimer = TRUE;
            }
            else if ((PioGet(SW3_PIO) == TRUE) && (incButtonState == KEY_PRESSED))
            {
                /* Set state to KEY RELEASE */
                incButtonState = KEY_RELEASED;
                update_nvm = TRUE;
            }
        }
    
    }
#endif    
    
#endif /* DEBUG_ENABLE */
#endif /* ENABLE_TEMPERATURE_CONTROLLER */

/*----------------------------------------------------------------------------*
 *  NAME
 *      readSensorDataFromNVM
 *
 *  DESCRIPTION
 *      This function reads sensor state data from NVM into state variable.
 *
 *  RETURNS
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
static void readSensorDataFromNVM(uint16 idx)
{
    Nvm_Read((uint16*)(sensor_state[idx].value), 
             sizeof(uint16),
             (GET_SENSOR_NVM_OFFSET(idx)));

    Nvm_Read((uint16*)&(sensor_state[idx].repeat_interval), 
             sizeof(uint8),
             (GET_SENSOR_NVM_OFFSET(idx) + sizeof(uint8)));

}

/*----------------------------------------------------------------------------*
 *  NAME
 *      writeSensorDataToNVM
 *
 *  DESCRIPTION
 *      This function writes sensor state data from state variable into NVM.
 *
 *  RETURNS
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
static void writeSensorDataToNVM(uint16 idx)
{
    Nvm_Write((uint16*)(sensor_state[idx].value), 
              sizeof(uint16),
              (GET_SENSOR_NVM_OFFSET(idx)));

    Nvm_Write((uint16*)&(sensor_state[idx].repeat_interval), 
              sizeof(uint8),
              (GET_SENSOR_NVM_OFFSET(idx) + sizeof(uint8)));
}

#ifdef ENABLE_ACK_MODE
/*----------------------------------------------------------------------------*
 *  NAME
 *      resetHeaterList
 *
 *  DESCRIPTION
 *      The function resets the device id and the ack flag of complete db
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void resetHeaterList(void)
{
    uint16 idx;

    for(idx=0; idx < MAX_HEATERS_IN_GROUP; idx++)
    {
        heater_list[idx].dev_id = CSR_MESH_BROADCAST_ID;
        heater_list[idx].ack_recvd = FALSE;
        heater_list[idx].no_response_count = 0;
    }
}
/*----------------------------------------------------------------------------*
 *  NAME
 *      resetAckInHeaterList
 *
 *  DESCRIPTION
 *      The function resets the ack flag of complete db with valid dev id
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void resetAckInHeaterList(void)
{
    uint16 idx;

    for(idx=0; idx < MAX_HEATERS_IN_GROUP; idx++)
    {
        if(heater_list[idx].dev_id != CSR_MESH_BROADCAST_ID)
        {
            heater_list[idx].ack_recvd = FALSE;
        }
    }
}
#endif /* ENABLE_ACK_MODE */
#if 0
    /*----------------------------------------------------------------------------*
     *  NAME
     *      startTempTransmission
     *
     *  DESCRIPTION
     *      This function starts the transmission of the temp sensor value.
     *
     *  RETURNS
     *      Nothing.
     *
     *----------------------------------------------------------------------------*/
    static void startTempTransmission()
    {
        write_val_retransmit_count = (NUM_OF_RETRANSMISSIONS/TRANSMIT_MSG_DENSITY);
        //DEBUG_STR(" WRITE DESIRED TEMP : ");
        //printInDecimal(current_desired_air_temp/32);
        //DEBUG_STR(" kelvin\r\n");
    
        //DEBUG_STR(" WRITE AIR TEMP : ");
        //printInDecimal(current_air_temp/32);
        //DEBUG_STR(" kelvin\r\n");
        writeTempValue();
        write_val_retransmit_count --;
    
        TimerDelete(retransmit_tid);
        retransmit_tid = TIMER_INVALID;
        startRetransmitTimer();
    
        writeSensorDataToNVM(DESIRED_AIR_TEMP_IDX);
    #ifdef ENABLE_ACK_MODE 
        resetAckInHeaterList();
    #endif /* ENABLE_ACK_MODE */
    }
#endif
void timerHandleReadBME280Data(timer_id tid)
{
     TimerDelete(tid);
     tid=TIMER_INVALID;
     uint32 air_temp=0;
     uint32 air_pressure=0;
     uint32 air_humi=0;
     uint16 voltValue=0;
    uint8 rawDat[8];
    BME280_Getdata(rawDat,8,TRUE);
    BME280_compensation_int32(&air_temp,&air_pressure,&air_humi);
    voltValue=AioRead(AIO0);/**/
 
    current_air_temp     =(uint16)(air_temp&0xffff);//2613+Random16();//raw sensor data
    //current_pressure_low =(air_pressure*10)&0xfff;//放大10倍
    //current_pressure_high =((air_pressure*10)>>12)&0xfff;
    current_air_humi     =air_humi/10;//7089+Random16();//丢弃最低一位精度
    current_air_pressure =air_pressure*10;//1013112+Random32();
    ampere_value        =2013+(Random16()&0x0f);
    
    PioSetModes(0x0e10,pio_mode_user);//0x1110 0001 0000
    PioSetDirs(0x0e10,0x0e10);
    PioSetPullModes(0x0e10,pio_mode_strong_pull_up); 
    
    /*if(isRemote==TRUE)
    {
        //retDataCount=50;
        //whichProcess=2;
        TimerCreate(0*SECOND,TRUE,sendbackSensorDataTimerHandler);
    }*/
    if(isRemote==FALSE)    //local
    {
        bleTestServData.temp=current_air_temp;
        bleTestServData.humi=current_air_humi;
        bleTestServData.pressure=current_air_pressure;
        bleTestServData.currentVal=ampere_value;        
        
            /*PioEnablePWM(2,FALSE);//LED_PWM_RED
            PioSetMode(LED_PIO_IND,pio_mode_pwm2);            
            PioConfigPWM(2,pio_pwm_mode_push_pull,1,200,1,200,1,1,5);
            PioEnablePWM(2,TRUE);*/
            
        sensorDataComplete=0x01;//sensorDataReady=0X03;    
        dataReadyNotifyCount=2;
        waitSensorDatarRead_tid=
            TimerCreate(0*SECOND,TRUE,waitSensorDataReadTimerHandler);
    }
    
}
void sendbackSensorDataTimerHandler(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
    uint8 measureSensorData1[10];
    measureSensorData1[0]= current_air_temp&0xff;//little-endian
    measureSensorData1[1]=(current_air_temp>>8)&0xff;
    measureSensorData1[2]= current_air_humi&0xff;
    measureSensorData1[3]=(current_air_humi>>8)&0xff;
    measureSensorData1[4]= current_air_pressure&0xff;
    measureSensorData1[5]=(current_air_pressure>>8)&0xff;
    measureSensorData1[6]=(current_air_pressure>>16)&0xff;//uint32->uint24
    measureSensorData1[7]= ampere_value&0xff;
    measureSensorData1[8]=(ampere_value>>8)&0xff;
    sendCustomCmd(200,15,measureSensorData1,9,RECEIVE_DATA1,devid_bridge);//15*200ms
    /**/    
}
#if 0
    /*----------------------------------------------------------------------------*
     *  NAME
     *      writeTempValue
     *
     *  DESCRIPTION
     *      This function writes the current and the desired temp values onto the 
     *      groups.
     *
     *  RETURNS
     *      Nothing.
     *
     *----------------------------------------------------------------------------*/
    static void writeTempValue(void)
    {
        uint16 index, index1;
        sensor_type_t type[2] = {sensor_type_internal_air_temperature,
                                 sensor_type_internal_humidity};
    
        bool ack_reqd = FALSE;
     
    #ifdef ENABLE_ACK_MODE 
        ack_reqd = TRUE;
    #endif /* ENABLE_ACK_MODE */
    
        for(index1 = 0; index1 < TRANSMIT_MSG_DENSITY; index1 ++)//2
        {
            for(index = 0; index < NUM_SENSOR_ACTUATOR_MODEL_GROUPS; index++)
            {
                if(sensor_actuator_model_groups[index] != 0 )
                   //&& current_air_temp != 0 &&
                   //current_desired_air_temp != 0)
                {
                    /* Retransmitting same messages back to back multiple times 
                     * increases the probability of the message being received by 
                     * devices running on low duty cycle scan. 
                     * This can be tuned by setting the TRANSMIT_MESSAGE_DENSITY 
                     */
                    /* transmit the pending message to all the groups */
                    SensorWriteValue(sensor_actuator_model_groups[index], 
                                     ack_reqd, 
                                     type,
                                     2,
                                     &g_tsapp_data.sensor_data);
                }
            }
        }
    }
    
    /*----------------------------------------------------------------------------*
     *  NAME
     *      retransmitIntervalTimerHandler
     *
     *  DESCRIPTION
     *      This function expires when the next message needs to be transmitted 
     *
     *  RETURNS
     *      Nothing.
     *
     *----------------------------------------------------------------------------*/
    static void retransmitIntervalTimerHandler(timer_id tid)
    {
        if (tid == retransmit_tid)
        {
            bool start_timer = TRUE;
    
            retransmit_tid = TIMER_INVALID;
    
            /* transmit the pending message to all the groups*/
            writeTempValue();
    
            write_val_retransmit_count --;
    
    #ifdef ENABLE_ACK_MODE 
            /* After half of the max retransmissions are over then check whether
             * ack has been received from all the heaters stored and if so then
             * stop sending the packet as we have received acks for all heaters.
             */
            if(write_val_retransmit_count < (NUM_OF_RETRANSMISSIONS/2))
            {
                uint8 idx;
                for(idx=0; idx < MAX_HEATERS_IN_GROUP; idx++)
                {
                    if(heater_list[idx].dev_id != CSR_MESH_BROADCAST_ID &&
                       heater_list[idx].ack_recvd == FALSE)
                    {
                        break;
                    }
                    if(idx == (MAX_HEATERS_IN_GROUP-1))
                    {
                        start_timer = FALSE;
                        //DEBUG_STR(" RECVD ALL ACK'S STOP TIMER : ");
                    }
                }
                /* One or more devices have not acked back increase the no response
                 * count. If the no response count reaches the maximum, remove the
                 * device from the heater list.
                 */
                if(write_val_retransmit_count == 0)
                {
                    for(idx=0; idx < MAX_HEATERS_IN_GROUP; idx++)
                    {
                        if(heater_list[idx].dev_id != CSR_MESH_BROADCAST_ID &&
                           heater_list[idx].ack_recvd == FALSE)
                        {
                            heater_list[idx].no_response_count++;
                            if(heater_list[idx].no_response_count >= 
                                                            MAX_NO_RESPONSE_COUNT)
                            {
                                heater_list[idx].dev_id = CSR_MESH_BROADCAST_ID;
                                heater_list[idx].no_response_count = 0;
                            }
                        }
                    }
                }
            }
    #endif /* ENABLE_ACK_MODE */ 
     
            if(start_timer == TRUE)
            {
                /* start a timer to send the broadcast sensor data */
                startRetransmitTimer();
            }
        }
    }
    
    /*----------------------------------------------------------------------------*
     *  NAME
     *      startRetransmitTimer
     *
     *  DESCRIPTION
     *      This function starts the broadcast timer for the current tempertature.
     *
     *  RETURNS
     *      None
     *
     *----------------------------------------------------------------------------*/
    static void startRetransmitTimer(void)
    {
        if(write_val_retransmit_count > 0)
        {
            retransmit_tid=TimerCreate(retransmit_interval[TRANSMIT_MSG_DENSITY-1],
                                       TRUE,
                                       retransmitIntervalTimerHandler);
        }
    }
    
    /*----------------------------------------------------------------------------*
     *  NAME
     *      tempSensorSampleIntervalTimeoutHandler
     *
     *  DESCRIPTION
     *      This function handles the sensor sample interval time-out.
     *
     *  RETURNS
     *      Nothing.
     *
     *----------------------------------------------------------------------------*/
    static void tempSensorSampleIntervalTimeoutHandler(timer_id tid)
    {
        if (tid == tempsensor_sample_tid)
        {
            tempsensor_sample_tid = TIMER_INVALID;
    
            /* Issue a Temperature Sensor Read. */
            TempSensorRead();
    
            /* Start the timer for next sample. */
            tempsensor_sample_tid = TimerCreate(
                                            (uint32)TEMPERATURE_SAMPLING_INTERVAL, 
                                            TRUE,
                                            tempSensorSampleIntervalTimeoutHandler);
    
        }
    }
    
    /*----------------------------------------------------------------------------*
     *  NAME
     *      tempSensorEvent
     *
     *  DESCRIPTION
     *      This function Handles the sensor read complete event. Checks if the new 
     *      temperature is within tolerence from the last broadcast value, otherwise
     *      broadcasts new value.
     *
     *  RETURNS
     *      Nothing.
     *
     *----------------------------------------------------------------------------*/
    static void tempSensorEvent(int16 temp)
    {
        /* Check if the new value is valid. */
        if (temp > 0)
        {
            uint16 abs_diff;
            SENSOR_FORMAT_TEMPERATURE_T cur_temp_returned = temp;
    
            /* If Desired air temperature is not Initialised, Initialise it based 
             * on current air temperature.
             */
            if (current_desired_air_temp == 0x0000)
            {
                current_desired_air_temp = cur_temp_returned;
            }
    
            //DEBUG_STR(" CURR AIR TEMP read : ");
            //printInDecimal(cur_temp_returned/32);
            //DEBUG_STR(" kelvin\r\n");
    
            /* Find the absolute difference between current and last broadcast
             * temperatures.
             */
            abs_diff = ABS_DIFF((uint16)last_bcast_air_temp, cur_temp_returned);
    
            /* If it changed beyond the tolerance value, then write the current 
             * temp onto the group.
             */
            if (abs_diff >= TEMPERATURE_CHANGE_TOLERANCE)
            {
                /* Set the present temperature as it is more than the temperature
                 * tolerance change. Write the new value to the group as well as
                 * reset the retransmit count to max and start the retransmit timer
                 * if its not started.
                 */
                current_air_temp = cur_temp_returned;
    
                /* Set last Broadcast Temperature to current temperature. */
                last_bcast_air_temp = current_air_temp;
    
                //startTempTransmission();
            }
        }
    }
#endif
/*-----------------------------------------------------------------------------*
 *  NAME
 *      IsSensorConfigured
 *
 *  DESCRIPTION
 *      This below function returns whether the sensor is configured or not
 *
 *  RETURNS/MODIFIES
 *      TRUE if the sensor has been grouped otherwise returns FALSE
 *
 *----------------------------------------------------------------------------*/
static bool IsSensorConfigured(void)
{
    uint16 index;

    for(index = 0; index < NUM_SENSOR_ACTUATOR_MODEL_GROUPS; index++)
    {
        if(sensor_actuator_model_groups[index] != 0)
        {
            return TRUE;
        }
    }
    return FALSE;
}
#if 0
/*-----------------------------------------------------------------------------*
 *  NAME
 *      startRepeatIntervalTimer
 *
 *  DESCRIPTION
 *      Start the repeat interval timer  The function should be called only if
 *      the repeat interval on the desired or the current index is non zero.
 *
 *  RETURNS/MODIFIES
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
static void startRepeatIntervalTimer (void)
{
    /* The app takes the minimum value of the repeat interval from current and
     * desired repeat intervals.
     */
    uint8 interval = sensor_state[CURRENT_AIR_TEMP_IDX].repeat_interval;

    if(sensor_state[CURRENT_AIR_TEMP_IDX].repeat_interval == 0 || 
      (sensor_state[DESIRED_AIR_TEMP_IDX].repeat_interval !=0 && 
        sensor_state[DESIRED_AIR_TEMP_IDX].repeat_interval < 
        sensor_state[CURRENT_AIR_TEMP_IDX].repeat_interval))
    {
        interval = sensor_state[DESIRED_AIR_TEMP_IDX].repeat_interval;
    }

    /* As the application transmits the same message for a longer period of time
     * we would consider the repeat interval values below 30 seconds to be 
     * atleast 30 seconds.
     */
    if(interval < 30)
    {
        interval = 30;
    }

    /* Delete the repeat interval timer */
    TimerDelete(repeat_interval_tid);
    repeat_interval_tid = TIMER_INVALID;

    repeat_interval_tid = TimerCreate(((uint32)interval* SECOND),
            TRUE, repeatIntervalTimerHandler);

    /* Start the transmission once here */
    //startTempTransmission();
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      repeatIntervalTimerHandler
 *
 *  DESCRIPTION
 *      This function handles repeat interval time-out.
 *
 *  RETURNS/MODIFIES
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
static void repeatIntervalTimerHandler(timer_id tid)
{
    if (repeat_interval_tid == tid)
    {
        repeat_interval_tid = TIMER_INVALID;
        startRepeatIntervalTimer();
    }
}
#endif
/*-----------------------------------------------------------------------------*
 *  NAME
 *      deviceIdAdvertTimeoutHandler
 *
 *  DESCRIPTION
 *      This function handles the Device ID advertise timer event.
 *
 *  RETURNS/MODIFIES
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
static void deviceIdAdvertTimeoutHandler(timer_id tid)
{
    if(tid == g_tsapp_data.mesh_device_id_advert_tid)
    {
        g_tsapp_data.mesh_device_id_advert_tid = TIMER_INVALID;
        /* Start the timer only if the device is not associated */
        if(g_tsapp_data.assoc_state == app_state_not_associated)
        {
            /* Generate a random delay between 0 to 4095 ms */
            uint32 random_delay = ((uint32)(Random16() & 0x0FFF))*(MILLISECOND);

            if(send_dev_appearance == FALSE)
            {
                /* Send the device ID advertisements */
                CsrMeshAssociateToANetwork();
                send_dev_appearance = TRUE;
            }
            else
            {
                /* Send the device appearance */
                CsrMeshAdvertiseDeviceAppearance(&device_appearance, 
                                                 short_name, 
                                                 sizeof(short_name));
                send_dev_appearance = FALSE;
            }
            g_tsapp_data.mesh_device_id_advert_tid = TimerCreate(
                                       DEVICE_ID_ADVERT_TIMER_ID + random_delay, 
                                       TRUE,
                                       deviceIdAdvertTimeoutHandler);
        }
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      appDataInit
 *
 *  DESCRIPTION
 *      This function is called to initialise CSRmesh Temperature Sensor
 *      application data structure.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void appDataInit(void)
{
    /* Reset/Delete all the timers */
    TimerDelete(g_tsapp_data.gatt_data.app_tid);
    g_tsapp_data.gatt_data.app_tid = TIMER_INVALID;

    TimerDelete(g_tsapp_data.gatt_data.con_param_update_tid);
    g_tsapp_data.gatt_data.con_param_update_tid = TIMER_INVALID;
    g_tsapp_data.gatt_data.cpu_timer_value = 0;

    g_tsapp_data.gatt_data.st_ucid = GATT_INVALID_UCID;

    g_tsapp_data.gatt_data.advert_timer_value = 0;

    /* Reset the connection parameter variables. */
    g_tsapp_data.gatt_data.conn_interval = 0;
    g_tsapp_data.gatt_data.conn_latency = 0;
    g_tsapp_data.gatt_data.conn_timeout = 0;

    /* Initialise GAP Data structure */
    GapDataInit();//以下同等并列函数均是简单配置对应结构体参数

    /* Initialise the Mesh Control Service Data Structure */
    MeshControlServiceDataInit();
    
    bleTestServiceInit();

#ifdef ENABLE_GATT_OTA_SERVICE
    /* Initialise GATT Data structure */
    GattDataInit();

    /* Initialise the CSR OTA Service Data */
    OtaDataInit();
#endif /* ENABLE_GATT_OTA_SERVICE */
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      initiateAssociation
 *
 *  DESCRIPTION
 *      This function starts timer to send CSRmesh Association Messages
 *      and also gives visual indication that Temperature Sensor is not 
 *      associated.
 *
 *  RETURNS/MODIFIES
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
static void initiateAssociation(void)
{
    /* Blink light to indicate that it is not associated */
    IOTLightControlDeviceBlink(0, 0, 127, 32, 32);

    /* Send the device ID advertisements */
    CsrMeshAssociateToANetwork();
    send_dev_appearance = TRUE;

    /* Start a timer to send Device ID messages periodically to get
     * associated to a network
     */
    g_tsapp_data.mesh_device_id_advert_tid = TimerCreate(
                                               DEVICE_ID_ADVERT_TIMER_ID,
                                               TRUE,
                                               deviceIdAdvertTimeoutHandler);
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      readPersistentStore
 *
 *  DESCRIPTION
 *      This function is used to Initialise and read NVM data
 *
 *  RETURNS/MODIFIES
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
static void readPersistentStore(void)
{
    /* NVM offset for supported services */
    uint16 nvm_offset = 0;
    uint16 nvm_sanity = 0xffff;
    bool nvm_start_fresh = FALSE;
    uint16 app_nvm_version;
    uint16 i;

    nvm_offset = NVM_MAX_APP_MEMORY_WORDS;

    /* Read the Application NVM version */
    Nvm_Read(&app_nvm_version, 1, NVM_OFFSET_APP_NVM_VERSION);

    if( app_nvm_version != APP_NVM_VERSION )        //first time
    {
        /* The NVM structure could have changed
         * with a new version of the application, due to NVM values getting
         * removed or added. Currently this application clears all application
         * and CSRmesh NVM values and writes to NVM
         */
#ifdef NVM_TYPE_EEPROM
        uint16 eeprom_erase = 0xFFFF;
        /* Erase a block in EEPROM to remove all sanity words */
        for (i = 0; i < 128; i++)
        {
            Nvm_Write(&eeprom_erase, 0x1, i);
        }
#elif NVM_TYPE_FLASH
        NvmErase(FALSE);
#endif /* NVM_TYPE_EEPROM */

        /* Save new version of the NVM */
        app_nvm_version = APP_NVM_VERSION;
        Nvm_Write(&app_nvm_version, 1, NVM_OFFSET_APP_NVM_VERSION);
    }

    /* Read the NVM sanity word to check if the NVM validity */
    Nvm_Read(&nvm_sanity, sizeof(nvm_sanity),
             NVM_OFFSET_SANITY_WORD);

    /* Initialise the paired flag to false */
    g_tsapp_data.gatt_data.paired = FALSE;

    if(nvm_sanity == NVM_SANITY_MAGIC)          //not first time
    {
        /* Read association state from NVM */
        Nvm_Read((uint16 *)&g_tsapp_data.assoc_state,
                sizeof(g_tsapp_data.assoc_state),
                NVM_OFFSET_ASSOCIATION_STATE);

        /* Read Bearer Model Data from NVM */
        Nvm_Read((uint16 *)&g_tsapp_data.bearer_data,
                 sizeof(BEARER_MODEL_STATE_DATA_T), NVM_BEARER_DATA_OFFSET);

        /* Read Sensor State Data. */
        for (i = 0; i < NUM_SENSORS_SUPPORTED; i++)
        {
            readSensorDataFromNVM(i);
        }
        
        /* Update Bearer Model Data to NVM */
        Nvm_Write((uint16 *)&g_tsapp_data.bearer_data,
                  sizeof(BEARER_MODEL_STATE_DATA_T), NVM_BEARER_DATA_OFFSET);        

        /* If NVM in use, read device name and length from NVM */
        GapReadDataFromNVM(&nvm_offset);
        //uint16 bridgeFlagErase=0xffff;
        Nvm_Read(&bridgeFlagNvm,1,NVM_OFFSET_BRIDGE_FLAG);//erase bridge flag        
        Nvm_Read((uint16 *)&current_sensor_status,1,NVM_OFFSET_SENSOR_STATUS);
    }
    else /* NVM Sanity check failed means either the device is being brought up
          * for the first time or memory has got corrupted in which case
          * discard the data and start fresh.
          */        //first time
    {
        /* Read Configuration flags from User CS Key */
        uint16 cskey_flags = CSReadUserKey(CSKEY_INDEX_USER_FLAGS);
        nvm_start_fresh = TRUE;

        nvm_sanity = NVM_SANITY_MAGIC;

        /* Write NVM Sanity word to the NVM */
        Nvm_Write(&nvm_sanity, sizeof(nvm_sanity),
                  NVM_OFFSET_SANITY_WORD);

        if (cskey_flags & RANDOM_UUID_ENABLE_MASK)
        {
            /* The flag is set so generate a random UUID NVM */
            for( i = 0 ; i < 8 ; i++)
            {
                g_node_data.device_uuid.uuid[i] = Random16();
            }
        }
        /* Write to NVM */
        Nvm_Write(g_node_data.device_uuid.uuid, DEVICE_UUID_SIZE_WORDS,
                  NVM_OFFSET_DEVICE_UUID);
        
        isMesh=FALSE;//GATT Server
        current_sensor_status=sensor_status_init;//sensor_status_init
        Nvm_Write((uint16 *)&current_sensor_status,1,NVM_OFFSET_SENSOR_STATUS);
        Nvm_Write(&bridgeFlagNvm,1,NVM_OFFSET_BRIDGE_FLAG);//bridgeFlagNvm=0xffff        

        /* Update Bearer Model Data from CSKey flags for the first time. */
        g_tsapp_data.bearer_data.bearerPromiscuous = 0x0000;
        g_tsapp_data.bearer_data.bearerEnabled     = BLE_BEARER_MASK;
        g_tsapp_data.bearer_data.bearerRelayActive = 0x0000;
       
        if (cskey_flags & RELAY_ENABLE_MASK)
        {
            g_tsapp_data.bearer_data.bearerRelayActive |= BLE_BEARER_MASK;
            g_tsapp_data.bearer_data.bearerPromiscuous |= BLE_BEARER_MASK;
        }

        if (cskey_flags & BRIDGE_ENABLE_MASK)
        {
            g_tsapp_data.bearer_data.bearerEnabled     |= 
                                                    BLE_GATT_SERVER_BEARER_MASK;
            g_tsapp_data.bearer_data.bearerRelayActive |= 
                                                    BLE_GATT_SERVER_BEARER_MASK;
            g_tsapp_data.bearer_data.bearerPromiscuous |= 
                                                    BLE_GATT_SERVER_BEARER_MASK;
        }/*
        g_tsapp_data.bearer_data.bearerRelayActive|= BLE_BEARER_MASK;//0x0001
        g_tsapp_data.bearer_data.bearerEnabled|=BLE_BEARER_MASK;//0x0001
        g_tsapp_data.bearer_data.bearerPromiscuous=BLE_BEARER_MASK;//0x0001
    
        bearer_relay_active = g_tsapp_data.bearer_data.bearerRelayActive;
        bearer_promiscuous = g_tsapp_data.bearer_data.bearerPromiscuous;*/
        
        CsrMeshRelayEnable(g_tsapp_data.bearer_data.bearerEnabled);
        CsrMeshEnablePromiscuousMode(g_tsapp_data.bearer_data.bearerPromiscuous);
        
        /* Update Bearer Model Data to NVM */
        Nvm_Write((uint16 *)&g_tsapp_data.bearer_data,
                  sizeof(BEARER_MODEL_STATE_DATA_T), NVM_BEARER_DATA_OFFSET);

#ifdef USE_AUTHORIZATION_CODE
        /* Write Authorization Code to NVM */
        Nvm_Write(g_node_data.auth_code.auth_code,DEVICE_AUTHCODE_SIZE_IN_WORDS,
                  NVM_OFFSET_DEVICE_AUTHCODE);
#endif /* USE_AUTHORIZATION_CODE */

        /* The device will not be associated as it is coming up for the
         * first time
         */
        g_tsapp_data.assoc_state = app_state_not_associated;

        /* Write association state to NVM */
        Nvm_Write((uint16 *)&g_tsapp_data.assoc_state,
                 sizeof(g_tsapp_data.assoc_state),
                 NVM_OFFSET_ASSOCIATION_STATE);

        /* Write Sensor State data to NVM. */
        for (i = 0; i < NUM_SENSORS_SUPPORTED; i++)
        {
            writeSensorDataToNVM(i);
        }

        /* If fresh NVM, write device name and length to NVM for the
         * first time.
         */
        GapInitWriteDataToNVM(&nvm_offset);
    }

    /* Read the UUID from NVM */
    Nvm_Read(g_node_data.device_uuid.uuid, DEVICE_UUID_SIZE_WORDS, 
                       NVM_OFFSET_DEVICE_UUID);

#ifdef USE_AUTHORIZATION_CODE
    /* Read Authorization Code from NVM */
    Nvm_Read(g_node_data.auth_code.auth_code, DEVICE_AUTHCODE_SIZE_IN_WORDS,
             NVM_OFFSET_DEVICE_AUTHCODE);
    g_node_data.use_authorisation = TRUE;
#endif /* USE_AUTHORIZATION_CODE */

    Nvm_Read((uint16 *)&g_tsapp_data.assoc_state,1, 
                                                  NVM_OFFSET_ASSOCIATION_STATE);
}
static void presetDevid(void)
{
    sensor_actuator_model_groups[0] = 0x0002;
    attention_model_groups[0] = 0x0002;
    data_model_groups[0] = 0x0002;
    uint16 local_device_id=g_node_data.device_uuid.uuid[0];
    uint16 local_network_key[8]={0X1234,0X1234,0X1234,0X1234,
                                 0X1234,0X1234,0X1234,0X1234};
    CSR_MESH_ADVSCAN_PARAM_T param;
    
    /* Save group id to NVM */
    Nvm_Write(&sensor_actuator_model_groups[0], 
       sizeof(uint16),
       NVM_OFFSET_SENSOR_ACTUATOR_MODEL_GROUPS);					 
    
    Nvm_Write(&attention_model_groups[0],
       sizeof(uint16),
       NVM_OFFSET_ATTENTION_MODEL_GROUPS);
    
    Nvm_Write(&data_model_groups[0],
       sizeof(uint16),
       NVM_OFFSET_DATA_MODEL_GROUPS);
    
    /*save device id to NVM*/			        
    Nvm_Write(&local_device_id, 1, NVM_OFFSET_DEVICE_ID);
    
    /*save network key to NVM*/
    Nvm_Write(&local_network_key[0], sizeof(CSR_MESH_NETWORK_KEY_T), 
              NVM_OFFSET_NETWORK_KEY);
    
      /* Network key */
    g_node_data.associated = TRUE;
    Nvm_Read(g_node_data.nw_key.key, sizeof(CSR_MESH_NETWORK_KEY_T),
                                                    NVM_OFFSET_NETWORK_KEY);
    /* Device ID */
    Nvm_Read(&g_node_data.device_id, 1, NVM_OFFSET_DEVICE_ID);
    /* Sequence Number */
    Nvm_Read((uint16 *)&g_node_data.seq_number, 2,
                                                NVM_OFFSET_SEQUENCE_NUMBER);
    /***eof g_node data*/
              
    g_tsapp_data.bearer_data.bearerPromiscuous &= ~BLE_BEARER_MASK;
    g_tsapp_data.assoc_state = app_state_associated;
    
    CsrMeshEnablePromiscuousMode(
         g_tsapp_data.bearer_data.bearerPromiscuous);										
    
    Nvm_Write((uint16 *)&g_tsapp_data.assoc_state, 1,
      NVM_OFFSET_ASSOCIATION_STATE);
      
    /* Update Bearer Model Data to NVM */
    Nvm_Write((uint16 *)&g_tsapp_data.bearer_data,
       sizeof(BEARER_MODEL_STATE_DATA_T),NVM_BEARER_DATA_OFFSET);

    /* When MESH_BRIDGE_SERVICE is not supported, Temperature Sensor 
    * needs to be associated with CSRmesh network, before it can send 
    * commands. Stop the blue led blinking visual indication, as 
    * Temperature Sensor is now associated to network.
    */
    IOTLightControlDevicePower(FALSE);
    
    /* If sensor was previously not grouped and has been grouped now, 
    * then the sensor should move into low duty cycle 
    */
    CsrMeshGetAdvScanParam(&param);
    param.scan_duty_cycle = DEFAULT_RX_DUTY_CYCLE;
    CsrMeshSetAdvScanParam(&param);
}
/*-----------------------------------------------------------------------------*
 *  NAME
 *      attnTimerHandler
 *
 *  DESCRIPTION
 *      This function handles Attention time-out.
 *
 *  RETURNS/MODIFIES
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
static void attnTimerHandler(timer_id tid)
{
    if (attn_tid == tid)
    {
        attn_tid = TIMER_INVALID;
        if(g_tsapp_data.assoc_state == app_state_associated)
        {
            /* Stop blink */
            IOTLightControlDevicePower(FALSE);

            /* Set back the scan to low duty cycle only if the device has
             * already been grouped.
             */
            EnableHighDutyScanMode(FALSE);
        }
        else
        {
            /* Restart association blink */
            IOTLightControlDeviceBlink(0, 0, 127, 32, 32);
        }
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      requestConnParamUpdate
 *
 *  DESCRIPTION
 *      This function is used to send L2CAP_CONNECTION_PARAMETER_UPDATE_REQUEST
 *      to the remote device when an earlier sent request had failed.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void requestConnParamUpdate(timer_id tid)
{
    /* Application specific preferred parameters */
    ble_con_params app_pref_conn_param;

    if(g_tsapp_data.gatt_data.con_param_update_tid == tid)
    {
        g_tsapp_data.gatt_data.con_param_update_tid = TIMER_INVALID;
        g_tsapp_data.gatt_data.cpu_timer_value = 0;

        /*Handling signal as per current state */
        switch(g_tsapp_data.state)
        {
            case app_state_connected:
            {
                /* Increment the count for Connection Parameter Update
                 * requests
                 */
                ++ g_tsapp_data.gatt_data.num_conn_update_req;

                /* If it is first or second request, preferred connection
                 * parameters should be request
                 */
                if(g_tsapp_data.gatt_data.num_conn_update_req == 1 ||
                   g_tsapp_data.gatt_data.num_conn_update_req == 2)
                {
                    app_pref_conn_param.con_max_interval =
                                                PREFERRED_MAX_CON_INTERVAL;
                    app_pref_conn_param.con_min_interval =
                                                PREFERRED_MIN_CON_INTERVAL;
                    app_pref_conn_param.con_slave_latency =
                                                PREFERRED_SLAVE_LATENCY;
                    app_pref_conn_param.con_super_timeout =
                                                PREFERRED_SUPERVISION_TIMEOUT;
                }
                /* If it is 3rd or 4th request, APPLE compliant parameters
                 * should be requested.
                 */
                else if(g_tsapp_data.gatt_data.num_conn_update_req == 3 ||
                        g_tsapp_data.gatt_data.num_conn_update_req == 4)
                {
                    app_pref_conn_param.con_max_interval =
                                                APPLE_MAX_CON_INTERVAL;
                    app_pref_conn_param.con_min_interval =
                                                APPLE_MIN_CON_INTERVAL;
                    app_pref_conn_param.con_slave_latency =
                                                APPLE_SLAVE_LATENCY;
                    app_pref_conn_param.con_super_timeout =
                                                APPLE_SUPERVISION_TIMEOUT;
                }

                /* Send Connection Parameter Update request using application
                 * specific preferred connection parameters
                 */

                if(LsConnectionParamUpdateReq(
                                            &g_tsapp_data.gatt_data.con_bd_addr,
                    &app_pref_conn_param) != ls_err_none)
                {
                    ReportPanic(app_panic_con_param_update);
                }
            }
            break;

            default:
                /* Ignore in other states */
            break;
        }

    } /* Else ignore the timer */

}


/*-----------------------------------------------------------------------------*
 *  NAME
 *      handleGapCppTimerExpiry
 *
 *  DESCRIPTION
 *      This function handles the expiry of TGAP(conn_pause_peripheral) timer.
 *      It starts the TGAP(conn_pause_central) timer, during which, if no activ-
 *      -ity is detected from the central device, a connection parameter update
 *      request is sent.
 *
 *  RETURNS
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
static void handleGapCppTimerExpiry(timer_id tid)
{
    if(g_tsapp_data.gatt_data.con_param_update_tid == tid)
    {
        g_tsapp_data.gatt_data.con_param_update_tid =
                           TimerCreate(TGAP_CPC_PERIOD, TRUE,
                                       requestConnParamUpdate);
        g_tsapp_data.gatt_data.cpu_timer_value = TGAP_CPC_PERIOD;
    }
}


/*----------------------------------------------------------------------------*
 *  NAME
 *      appAdvertisingExit
 *
 *  DESCRIPTION
 *      This function is called while exiting app_state_advertising
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void appAdvertisingExit(void)
{
    /* Stop on-going advertisements */
    GattStopAdverts();//GattCancelConnectReq库函数

    /* Cancel advertisement timer */
    TimerDelete(g_tsapp_data.gatt_data.app_tid);
    g_tsapp_data.gatt_data.app_tid = TIMER_INVALID;
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      handleSignalGattAddDBCfm
 *
 *  DESCRIPTION
 *      This function handles the signal GATT_ADD_DB_CFM
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void handleSignalGattAddDBCfm(GATT_ADD_DB_CFM_T *p_event_data)
{
    switch(g_tsapp_data.state)
    {
        case app_state_init:
        {
            if(p_event_data->result == sys_status_success)
            {
                /* If GATT bearer is enabled move to advertisement state 
                 * otherwise move to idle state. The advertisement would be 
                 * triggerred once the GATT bearer is enabled again.
                 */
                if(g_tsapp_data.bearer_data.bearerEnabled & 
                                                BLE_GATT_SERVER_BEARER_MASK)
                {
                    AppSetState(app_state_advertising);
                }
                else
                {
                    AppSetState(app_state_idle);
                }
            }
            else
            {
                /* Don't expect this to happen */
                ReportPanic(app_panic_db_registration);
            }
        }
        break;

        default:
            /* Control should never come here */
            ReportPanic(app_panic_invalid_state);
        break;
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      handleSignalGattCancelConnectCfm
 *
 *  DESCRIPTION
 *      This function handles the signal GATT_CANCEL_CONNECT_CFM
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void handleSignalGattCancelConnectCfm(void)
{
    /*Handling signal as per current state */
    switch(g_tsapp_data.state)
    {
        case app_state_advertising:
        {
            /* Do nothing here */
        }
        break;

        case app_state_connected:
            /* The CSRmesh could have been sending data on
             * advertisements so do not panic
             */
        break;

        default:
            /* Control should never come here */
            ReportPanic(app_panic_invalid_state);
        break;
    }
}

/*---------------------------------------------------------------------------
 *
 *  NAME
 *      handleSignalLmEvConnectionComplete
 *
 *  DESCRIPTION
 *      This function handles the signal LM_EV_CONNECTION_COMPLETE.
 *
 *  RETURNS
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
static void handleSignalLmEvConnectionComplete(
                                     LM_EV_CONNECTION_COMPLETE_T *p_event_data)
{
    /* Store the connection parameters. */
    g_tsapp_data.gatt_data.conn_interval = p_event_data->data.conn_interval;
    g_tsapp_data.gatt_data.conn_latency  = p_event_data->data.conn_latency;
    g_tsapp_data.gatt_data.conn_timeout  = 
                                        p_event_data->data.supervision_timeout;
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      handleSignalGattConnectCfm
 *
 *  DESCRIPTION
 *      This function handles the signal GATT_CONNECT_CFM
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void handleSignalGattConnectCfm(GATT_CONNECT_CFM_T* p_event_data)
{
    /*Handling signal as per current state */
    switch(g_tsapp_data.state)
    {
        case app_state_advertising:
        {
            if(p_event_data->result == sys_status_success)
            {
                /* Store received UCID */
                g_tsapp_data.gatt_data.st_ucid = p_event_data->cid;

                /* Store connected BD Address */
                g_tsapp_data.gatt_data.con_bd_addr = p_event_data->bd_addr;

                /* Store the bearer relay active and promiscuous onto global 
                 * as they need to be reverted after disconnection.
                 */
                bearer_relay_active = 
                    g_tsapp_data.bearer_data.bearerRelayActive;

                bearer_promiscuous = 
                    g_tsapp_data.bearer_data.bearerPromiscuous;

                g_tsapp_data.bearer_data.bearerRelayActive = 
                    BLE_BEARER_MASK | BLE_GATT_SERVER_BEARER_MASK;

                g_tsapp_data.bearer_data.bearerPromiscuous = 
                    BLE_BEARER_MASK | BLE_GATT_SERVER_BEARER_MASK;

                /* When device is connected as bridge enable the BLE and GATT 
                 * bearer relays otherwise mesh messages sent by control device 
                 * over GATT will not be forwarded on mesh.
                 */
                CsrMeshRelayEnable(
                                g_tsapp_data.bearer_data.bearerRelayActive);

                /* Enable the promiscuous mode on both the bearers which makes
                 * sure the connected control device can control any mesh n/w.
                 */
                CsrMeshEnablePromiscuousMode(
                                g_tsapp_data.bearer_data.bearerPromiscuous);

                /* Enter connected state */
                AppSetState(app_state_connected);
                
                /* Inform CSRmesh that we are connected now */
                CsrMeshHandleDataInConnection(
                                g_tsapp_data.gatt_data.st_ucid,
                                g_tsapp_data.gatt_data.conn_interval);


                /* Since CSRmesh Temperature Sensor app does not mandate
                 * encryption requirement on its characteristics, so the
                 * remote master may or may not encrypt the link. Start a
                 * timer  here to give remote master some time to encrypt
                 * the link and on expiry of that timer, send a connection
                 * parameter update request to remote side.
                 */

                /* Don't request security as this causes connection issues
                 * with Android 4.4
                 *
                 *  SMRequestSecurityLevel(
                                    &g_tsapp_data.gatt_data.con_bd_addr);
                 */

                /* If the current connection parameters being used don't
                 * comply with the application's preferred connection
                 * parameters and the timer is not running and, start timer
                 * to trigger Connection Parameter Update procedure
                 */
                if((g_tsapp_data.gatt_data.con_param_update_tid ==
                                                        TIMER_INVALID) &&
                   (g_tsapp_data.gatt_data.conn_interval <
                                             PREFERRED_MIN_CON_INTERVAL ||
                    g_tsapp_data.gatt_data.conn_interval >
                                             PREFERRED_MAX_CON_INTERVAL
#if PREFERRED_SLAVE_LATENCY
                    || g_tsapp_data.gatt_data.conn_latency <
                                             PREFERRED_SLAVE_LATENCY
#endif
                   )
                  )
                {
                    /* Set the number of conn update attempts to zero */
                    g_tsapp_data.gatt_data.num_conn_update_req = 0;

                    /* The application first starts a timer of
                     * TGAP_CPP_PERIOD. During this time, the application
                     * waits for the peer device to do the database
                     * discovery procedure. After expiry of this timer, the
                     * application starts one more timer of period
                     * TGAP_CPC_PERIOD. If the application receives any
                     * GATT_ACCESS_IND during this time, it assumes that
                     * the peer device is still doing device database
                     * discovery procedure or some other configuration and
                     * it should not update the parameters, so it restarts
                     * the TGAP_CPC_PERIOD timer. If this timer expires, the
                     * application assumes that database discovery procedure
                     * is complete and it initiates the connection parameter
                     * update procedure.
                     */
                    g_tsapp_data.gatt_data.con_param_update_tid =
                                      TimerCreate(TGAP_CPP_PERIOD, TRUE,
                                                  handleGapCppTimerExpiry);
                    g_tsapp_data.gatt_data.cpu_timer_value =
                                                        TGAP_CPP_PERIOD;
                }
                  /* Else at the expiry of timer Connection parameter
                   * update procedure will get triggered
                   */
            }
            else
            {
                /* If GATT bearer is enabled move to advertisement state 
                 * otherwise move to idle state. The advertisement would be 
                 * triggerred once the GATT bearer is enabled again.
                 */
                if(g_tsapp_data.bearer_data.bearerEnabled & 
                                                BLE_GATT_SERVER_BEARER_MASK)
                {
                    AppSetState(app_state_advertising);
                }
                else
                {
                    AppSetState(app_state_idle);
                }
            }
        }
        break;

        default:
            /* Control should never come here */
            ReportPanic(app_panic_invalid_state);
        break;
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      handleSignalSmSimplePairingCompleteInd
 *
 *  DESCRIPTION
 *      This function handles the signal SM_SIMPLE_PAIRING_COMPLETE_IND
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void handleSignalSmSimplePairingCompleteInd(
                                 SM_SIMPLE_PAIRING_COMPLETE_IND_T *p_event_data)
{
    /*Handling signal as per current state */
    switch(g_tsapp_data.state)
    {
        case app_state_connected:
        {
            if(p_event_data->status == sys_status_success)
            {
                /* Store temporary pairing info. */
                g_tsapp_data.gatt_data.paired = TRUE;
            }
            else
            {
                /* Pairing has failed.disconnect the link.*/
                AppSetState(app_state_disconnecting);
            }
        }
        break;

        default:
            /* Firmware may send this signal after disconnection. So don't
             * panic but ignore this signal.
             */
        break;
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      handleSignalLsConnParamUpdateCfm
 *
 *  DESCRIPTION
 *      This function handles the signal LS_CONNECTION_PARAM_UPDATE_CFM.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void handleSignalLsConnParamUpdateCfm(
                            LS_CONNECTION_PARAM_UPDATE_CFM_T *p_event_data)
{
    /*Handling signal as per current state */
    switch(g_tsapp_data.state)
    {
        case app_state_connected:
        {
            /* Received in response to the L2CAP_CONNECTION_PARAMETER_UPDATE
              * request sent from the slave after encryption is enabled. If
              * the request has failed, the device should again send the same
              * request only after Tgap(conn_param_timeout). Refer
              * Bluetooth 4.0 spec Vol 3 Part C, Section 9.3.9 and profile spec.
              */
            if ((p_event_data->status != ls_err_none) &&
                (g_tsapp_data.gatt_data.num_conn_update_req <
                                        MAX_NUM_CONN_PARAM_UPDATE_REQS))
            {
                /* Delete timer if running */
                TimerDelete(g_tsapp_data.gatt_data.con_param_update_tid);

                g_tsapp_data.gatt_data.con_param_update_tid =
                                 TimerCreate(GAP_CONN_PARAM_TIMEOUT,
                                             TRUE, requestConnParamUpdate);
                g_tsapp_data.gatt_data.cpu_timer_value =
                                             GAP_CONN_PARAM_TIMEOUT;
            }
        }
        break;

        default:
            /* Control should never come here but in one of the odd cases when 
             * the master is disconnecting during the connection param update 
             * the above msg is received after the disconnection complete from  
             * the firmware. Hence ignoring the signal is other states too.
             */
            /* ReportPanic(app_panic_invalid_state); */
        break;
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      handleSignalLmConnectionUpdate
 *
 *  DESCRIPTION
 *      This function handles the signal LM_EV_CONNECTION_UPDATE.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void handleSignalLmConnectionUpdate(
                                   LM_EV_CONNECTION_UPDATE_T* p_event_data)
{
    switch(g_tsapp_data.state)
    {
        case app_state_connected:
        case app_state_disconnecting:
        {
            /* Store the new connection parameters. */
            g_tsapp_data.gatt_data.conn_interval =
                                            p_event_data->data.conn_interval;
            g_tsapp_data.gatt_data.conn_latency =
                                            p_event_data->data.conn_latency;
            g_tsapp_data.gatt_data.conn_timeout =
                                        p_event_data->data.supervision_timeout;

            CsrMeshHandleDataInConnection(g_tsapp_data.gatt_data.st_ucid,
                                       g_tsapp_data.gatt_data.conn_interval);

            //DEBUG_STR("Parameter Update Complete: ");
            //DEBUG_U16(g_tsapp_data.gatt_data.conn_interval);
            //DEBUG_STR("\r\n");
        }
        break;

        default:
            /* Control should never come here but in one of the odd cases when 
             * the master is disconnecting during the connection param update 
             * the above msg is received after the disconnection complete from  
             * the firmware. Hence ignoring the signal is other states too.
             */
            /* ReportPanic(app_panic_invalid_state); */
        break;
    }
}


/*----------------------------------------------------------------------------*
 *  NAME
 *      handleSignalLsConnParamUpdateInd
 *
 *  DESCRIPTION
 *      This function handles the signal LS_CONNECTION_PARAM_UPDATE_IND.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void handleSignalLsConnParamUpdateInd(
                                 LS_CONNECTION_PARAM_UPDATE_IND_T *p_event_data)
{
    /*Handling signal as per current state */
    switch(g_tsapp_data.state)
    {
        case app_state_connected:
        {
            /* Delete timer if running */
            TimerDelete(g_tsapp_data.gatt_data.con_param_update_tid);
            g_tsapp_data.gatt_data.con_param_update_tid = TIMER_INVALID;
            g_tsapp_data.gatt_data.cpu_timer_value = 0;

            /* The application had already received the new connection
             * parameters while handling event LM_EV_CONNECTION_UPDATE.
             * Check if new parameters comply with application preferred
             * parameters. If not, application shall trigger Connection
             * parameter update procedure
             */

            if(g_tsapp_data.gatt_data.conn_interval <
                                                PREFERRED_MIN_CON_INTERVAL ||
               g_tsapp_data.gatt_data.conn_interval >
                                                PREFERRED_MAX_CON_INTERVAL
#if PREFERRED_SLAVE_LATENCY
               || g_tsapp_data.gatt_data.conn_latency <
                                                PREFERRED_SLAVE_LATENCY
#endif
              )
            {
                /* Set the num of conn update attempts to zero */
                g_tsapp_data.gatt_data.num_conn_update_req = 0;

                /* Start timer to trigger Connection Parameter Update
                 * procedure
                 */
                g_tsapp_data.gatt_data.con_param_update_tid =
                                TimerCreate(GAP_CONN_PARAM_TIMEOUT,
                                            TRUE, requestConnParamUpdate);
                g_tsapp_data.gatt_data.cpu_timer_value =
                                                        GAP_CONN_PARAM_TIMEOUT;
            }
        }
        break;

        default:
            /* Control should never come here but in one of the odd cases when 
             * the master is disconnecting during the connection param update 
             * the above msg is received after the disconnection complete from  
             * the firmware. Hence ignoring the signal is other states too.
             */
            /* ReportPanic(app_panic_invalid_state); */
        break;
    }

}

/*----------------------------------------------------------------------------*
 *  NAME
 *      handleSignalGattAccessInd
 *
 *  DESCRIPTION
 *      This function handles GATT_ACCESS_IND message for attributes
 *      maintained by the application.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void handleSignalGattAccessInd(GATT_ACCESS_IND_T *p_event_data)
{

    /*Handling signal as per current state */
    switch(g_tsapp_data.state)
    {
        case app_state_connected:
        {
            /* GATT_ACCESS_IND indicates that the central device is still disco-
             * -vering services. So, restart the connection parameter update
             * timer
             */
             if(g_tsapp_data.gatt_data.cpu_timer_value == TGAP_CPC_PERIOD &&
                g_tsapp_data.gatt_data.con_param_update_tid != TIMER_INVALID)
             {
                TimerDelete(g_tsapp_data.gatt_data.con_param_update_tid);
                g_tsapp_data.gatt_data.con_param_update_tid =
                                    TimerCreate(TGAP_CPC_PERIOD,
                                                TRUE, requestConnParamUpdate);
             }

            /* Received GATT ACCESS IND with write access */
            if(p_event_data->flags & ATT_ACCESS_WRITE)
            {
                /* If only ATT_ACCESS_PERMISSION flag is enabled, then the
                 * firmware is asking the app for permission to go along with
                 * prepare write request from the peer. Allow it.
                 */
                if(((p_event_data->flags) &
                   (ATT_ACCESS_PERMISSION | ATT_ACCESS_WRITE_COMPLETE))
                                                    == ATT_ACCESS_PERMISSION)
                {
                    GattAccessRsp(p_event_data->cid, p_event_data->handle,
                                  sys_status_success, 0, NULL);
                }
                else
                {
                    HandleAccessWrite(p_event_data);
                }
            }
            else if(p_event_data->flags & ATT_ACCESS_WRITE_COMPLETE)
            {
                GattAccessRsp(p_event_data->cid, p_event_data->handle,
                                          sys_status_success, 0, NULL);
            }
            /* Received GATT ACCESS IND with read access */
            else if(p_event_data->flags ==
                                    (ATT_ACCESS_READ | ATT_ACCESS_PERMISSION))
            {
                HandleAccessRead(p_event_data);
            }
            else
            {
                GattAccessRsp(p_event_data->cid, p_event_data->handle,
                              gatt_status_request_not_supported,
                              0, NULL);
            }
        }
        break;

        default:
            /* Control should never come here */
            ReportPanic(app_panic_invalid_state);
        break;
    }
}


/*----------------------------------------------------------------------------*
 *  NAME
 *      handleSignalLmDisconnectComplete
 *
 *  DESCRIPTION
 *      This function handles LM Disconnect Complete event which is received
 *      at the completion of disconnect procedure triggered either by the
 *      device or remote host or because of link loss
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void handleSignalLmDisconnectComplete(
                HCI_EV_DATA_DISCONNECT_COMPLETE_T *p_event_data)
{

    /* Reset the connection parameter variables. */
    g_tsapp_data.gatt_data.conn_interval = 0;
    g_tsapp_data.gatt_data.conn_latency = 0;
    g_tsapp_data.gatt_data.conn_timeout = 0;

    CsrMeshHandleDataInConnection(GATT_INVALID_UCID, 0);

    /* Restore the relay and the promiscuous settings to the last set values */
    g_tsapp_data.bearer_data.bearerRelayActive = bearer_relay_active;
    g_tsapp_data.bearer_data.bearerPromiscuous = bearer_promiscuous;

    CsrMeshRelayEnable(g_tsapp_data.bearer_data.bearerRelayActive);
    CsrMeshEnablePromiscuousMode(g_tsapp_data.bearer_data.bearerPromiscuous);
    
    PioEnablePWM(2,FALSE);
    PioSetMode(LED_PIO_IND,pio_mode_user);
    PioSet(LED_PIO_IND,LED_OFF);//connection indication LED:OFF
    /*if(waitSensorDatarRead_tid!=TIMER_INVALID)
    {
        TimerDelete(waitSensorDatarRead_tid);
        waitSensorDatarRead_tid=TIMER_INVALID;//stop dataReady indication        
    }*/
#ifdef ENABLE_GATT_OTA_SERVICE
    if(OtaResetRequired())
    {
        OtaReset();
    }
#endif /* ENABLE_GATT_OTA_SERVICE */


    /*Handling signal as per current state */
    switch(g_tsapp_data.state)
    {
        case app_state_connected:
        case app_state_disconnecting:
        {
            /* Connection is terminated either due to Link Loss or
             * the local host terminated connection. In either case
             * Initialise the app data and go to fast advertising.
             */
            appDataInit();

            /* If GATT bearer is enabled move to advertisement state 
             * otherwise move to idle state. The advertisement would be 
             * triggerred once the GATT bearer is enabled again.
             */
            if(g_tsapp_data.bearer_data.bearerEnabled & 
                                            BLE_GATT_SERVER_BEARER_MASK)
            {   //switch to GATT
                /*uint16 cskey_flags = CSReadUserKey(CSKEY_INDEX_USER_FLAGS);
                if (cskey_flags & RELAY_ENABLE_MASK)
                {
                    g_tsapp_data.bearer_data.bearerRelayActive |= BLE_BEARER_MASK;
                    g_tsapp_data.bearer_data.bearerPromiscuous |= BLE_BEARER_MASK;
                }
        
                if (cskey_flags & BRIDGE_ENABLE_MASK)
                {
                    g_tsapp_data.bearer_data.bearerEnabled     |= 
                                                            BLE_GATT_SERVER_BEARER_MASK;
                    g_tsapp_data.bearer_data.bearerRelayActive |= 
                                                            BLE_GATT_SERVER_BEARER_MASK;
                    g_tsapp_data.bearer_data.bearerPromiscuous |= 
                                                            BLE_GATT_SERVER_BEARER_MASK;
                }
                
                CsrMeshRelayEnable(g_tsapp_data.bearer_data.bearerEnabled);
                CsrMeshEnablePromiscuousMode(g_tsapp_data.bearer_data.bearerPromiscuous);
            
                Nvm_Write((uint16 *)&g_tsapp_data.bearer_data,
                          sizeof(BEARER_MODEL_STATE_DATA_T),NVM_BEARER_DATA_OFFSET);*/
                
        /*g_tsapp_data.bearer_data.bearerRelayActive|= BLE_BEARER_MASK;//0x0001
        g_tsapp_data.bearer_data.bearerEnabled|=BLE_BEARER_MASK;//0x0001
        g_tsapp_data.bearer_data.bearerPromiscuous=BLE_BEARER_MASK;//0x0001
    
        bearer_relay_active = g_tsapp_data.bearer_data.bearerRelayActive;
        bearer_promiscuous = g_tsapp_data.bearer_data.bearerPromiscuous;
        
        CsrMeshRelayEnable(g_tsapp_data.bearer_data.bearerEnabled);
        CsrMeshEnablePromiscuousMode(g_tsapp_data.bearer_data.bearerPromiscuous);
    
        Nvm_Write((uint16 *)&g_tsapp_data.bearer_data,
                  sizeof(BEARER_MODEL_STATE_DATA_T),NVM_BEARER_DATA_OFFSET);*/        
                
                /*if(current_sensor_status==sensor_status_normal)
                {
                    isMesh=TRUE;
                    WarmReset();
                }*/
                AppSetState(app_state_advertising);
                if(current_sensor_status==sensor_status_register)
                {
                    current_sensor_status=sensor_status_normal;
                    Nvm_Write((uint16 *)&current_sensor_status,1,NVM_OFFSET_SENSOR_STATUS);
                    isMesh=FALSE;                    
                }
            }
            else
            {
                AppSetState(app_state_idle);
            }
        }
        break;

        default:
            /* Control should never come here */
            ReportPanic(app_panic_invalid_state);
        break;
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      handleCsrMeshGroupSetMsg
 *
 *  DESCRIPTION
 *      This function handles the CSRmesh Group Assignment message. Stores
 *      the group_id at the given index for the model
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static bool handleCsrMeshGroupSetMsg(uint8 *msg, uint16 len)
{
    CSR_MESH_MODEL_TYPE_T model = msg[0];
    uint8 index = msg[1];
    uint16 group_id = msg[3] + (msg[4] << 8);
    bool update_lastetag = TRUE;

    if(model == CSR_MESH_SENSOR_MODEL || model == CSR_MESH_ACTUATOR_MODEL || 
       model == CSR_MESH_ALL_MODELS)
    {
        if(index < NUM_SENSOR_ACTUATOR_MODEL_GROUPS)
        {
            CSR_MESH_ADVSCAN_PARAM_T param;
            bool old_config_status = IsSensorConfigured();

            /* Store Group ID */
            sensor_actuator_model_groups[index] = group_id;

            /* Save to NVM */
            Nvm_Write(&sensor_actuator_model_groups[index], 
                      sizeof(uint16),
                     (NVM_OFFSET_SENSOR_ACTUATOR_MODEL_GROUPS + index));

            /* If sensor was previously not grouped and has been grouped now, 
             * then the sensor should move into low duty cycle 
             */
            if(!old_config_status && IsSensorConfigured())
            {
#ifdef ENABLE_TEMPERATURE_CONTROLLER
                /* Print a message for temperature control. */
                //DEBUG_STR("\r\nPress '+'/'-' Increase/Decrease Temp.\r\n");
#endif /* ENABLE_TEMPERATURE_CONTROLLER */
                CsrMeshGetAdvScanParam(&param);
                param.scan_duty_cycle = DEFAULT_RX_DUTY_CYCLE;//2%
                CsrMeshSetAdvScanParam(&param);
                //DEBUG_STR("Moving to Low Power Sniff Mode \r\n\r\n");

                if(sensor_state[DESIRED_AIR_TEMP_IDX].repeat_interval !=0 ||
                   sensor_state[CURRENT_AIR_TEMP_IDX].repeat_interval !=0)
                {
                    //startRepeatIntervalTimer();
                }
            }
            else if(old_config_status && !IsSensorConfigured())
            {
                //DEBUG_STR("Sensor Moving to active scan Mode \r\n\r\n");
                CsrMeshGetAdvScanParam(&param);
                param.scan_duty_cycle = HIGH_RX_DUTY_CYCLE;
                CsrMeshSetAdvScanParam(&param);

                /* Delete the repeat interval timer */
                TimerDelete(repeat_interval_tid);
                repeat_interval_tid = TIMER_INVALID;

                /* Stop the periodic reading of the temp */
                TimerDelete(tempsensor_sample_tid);
                tempsensor_sample_tid = TIMER_INVALID;
            }

            /* A new group has been set. Hence start temp read and update */
            if(group_id != 0)
            {
                /* Reset last bcast temp to trigger temp update on sensor read*/
                last_bcast_air_temp = 0;

                /* Issue a Read to start sampling timer. */
                TempSensorRead();

                /* Start the timer for next sample. */
                //tempsensor_sample_tid = TimerCreate(
                //                        (uint32)TEMPERATURE_SAMPLING_INTERVAL, 
                //                        TRUE,
                //                        tempSensorSampleIntervalTimeoutHandler);
            }
        }
        else
        {
            update_lastetag = FALSE;
        }
    }

    if(model == CSR_MESH_ATTENTION_MODEL || model == CSR_MESH_ALL_MODELS)
    {
        if(index < NUM_ATTENTION_MODEL_GROUPS)
        {
            attention_model_groups[index] = group_id;

            /* Save to NVM */
            Nvm_Write(&attention_model_groups[index],
                     sizeof(uint16),
                     NVM_OFFSET_ATTENTION_MODEL_GROUPS + index);
        }
        else
        {
            update_lastetag = FALSE;
        }
    }

#ifdef ENABLE_DATA_MODEL
    if(model == CSR_MESH_DATA_MODEL || model == CSR_MESH_ALL_MODELS)
    {
        if(index < NUM_DATA_MODEL_GROUPS)
        {
            data_model_groups[index] = group_id;

            /* Save to NVM */
            Nvm_Write(&data_model_groups[index],
                     sizeof(uint16),
                     NVM_OFFSET_DATA_MODEL_GROUPS + index);
        }
        else
        {
            update_lastetag = FALSE;
        }
    }
#endif /* ENABLE_DATA_MODEL */
    return update_lastetag;
}

/*============================================================================*
 *  Public Function Implementations
 *============================================================================*/

#ifdef NVM_TYPE_FLASH
/*----------------------------------------------------------------------------*
 *  NAME
 *      WriteApplicationAndServiceDataToNVM
 *
 *  DESCRIPTION
 *      This function writes the application data to NVM. This function should
 *      be called on getting nvm_status_needs_erase
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
extern void WriteApplicationAndServiceDataToNVM(void)
{
    uint16 nvm_sanity = 0xffff;
    nvm_sanity = NVM_SANITY_MAGIC;

    /* Write NVM sanity word to the NVM */
    Nvm_Write(&nvm_sanity, sizeof(nvm_sanity), NVM_OFFSET_SANITY_WORD);

    /* Store the Association State */
    Nvm_Write((uint16 *)&g_tsapp_data.assoc_state,
              sizeof(g_tsapp_data.assoc_state),
              NVM_OFFSET_ASSOCIATION_STATE);

    /* Write GAP service data into NVM */
    WriteGapServiceDataInNVM();

}
#endif /* NVM_TYPE_FLASH */

/*-----------------------------------------------------------------------------*
 *  NAME
 *      EnableHighDutyScanMode
 *
 *  DESCRIPTION
 *      The function enables/disables the active scan mode 
 *
 *  RETURNS/MODIFIES
 *      None
 *
 *----------------------------------------------------------------------------*/
extern void EnableHighDutyScanMode(bool enable)
{
    CSR_MESH_ADVSCAN_PARAM_T param;

    CsrMeshGetAdvScanParam(&param);

    if(enable)
    {
        param.scan_duty_cycle = HIGH_RX_DUTY_CYCLE;//100%
    }
    else
    {
        if(IsSensorConfigured())
        {
            /* Change the Rx scan duty cycle to default val on disabling 
             * attention 
             */
            param.scan_duty_cycle = DEFAULT_RX_DUTY_CYCLE;//2%
        }
        else
        {
            /* Change the Rx scan duty cycle to active as the device 
             * is not grouped yet.
             */
            param.scan_duty_cycle = HIGH_RX_DUTY_CYCLE;
        }
    }
    CsrMeshSetAdvScanParam(&param);
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      AppSetState
 *
 *  DESCRIPTION
 *      This function is used to set the state of the application.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
extern void AppSetState(app_state new_state)
{
    /* Check if the new state to be set is not the same as the present state
     * of the application.
     */
    app_state old_state = g_tsapp_data.state;

    if (old_state != new_state)
    {
        /* Handle exiting old state */
        switch(old_state)
        {
            case app_state_init:
                /* Do nothing here */
            break;

            case app_state_disconnecting:
                /* Common things to do whenever application exits
                 * app_state_disconnecting state.
                 */

                /* Initialise CSRmesh Temperature Sensor data and services
                 * data structure while exiting Disconnecting state.
                 */
                appDataInit();
            break;

            case app_state_advertising:
                /* Common things to do whenever application exits
                 * APP_*_ADVERTISING state.
                 */
                appAdvertisingExit();
            break;

            case app_state_connected:
            {
                /* Do nothing here */
            }
            break;

            default:
                /* Nothing to do */
            break;
        }

        /* Set new state */
        g_tsapp_data.state = new_state;

        /* Handle entering new state */
        switch(new_state)
        {
            case app_state_advertising:
            {
                GattTriggerFastAdverts();
                ledFlashtid=TimerCreate(0*SECOND,TRUE,handleLEDFlash1S);
            }
            break;

            case app_state_connected:
            {
                TimerDelete(ledFlashtid);ledFlashtid=TIMER_INVALID;
                uint16 bridgeFlag=0;
                Nvm_Read(&bridgeFlag,1,NVM_OFFSET_BRIDGE_FLAG);
                    /*//Switch to mesh
                    g_tsapp_data.bearer_data.bearerRelayActive|= BLE_BEARER_MASK;//0x0001
                    g_tsapp_data.bearer_data.bearerEnabled|=BLE_BEARER_MASK;//0x0001
                    g_tsapp_data.bearer_data.bearerPromiscuous|=BLE_BEARER_MASK;//0x0001
                
                    bearer_relay_active = g_tsapp_data.bearer_data.bearerRelayActive;
                    bearer_promiscuous = g_tsapp_data.bearer_data.bearerPromiscuous;
                    
                    CsrMeshRelayEnable(g_tsapp_data.bearer_data.bearerEnabled);
                    CsrMeshEnablePromiscuousMode(g_tsapp_data.bearer_data.bearerPromiscuous);
                
                    Nvm_Write((uint16 *)&g_tsapp_data.bearer_data,
                              sizeof(BEARER_MODEL_STATE_DATA_T),NVM_BEARER_DATA_OFFSET);*/
                    
                if(bridgeFlag==0xffff)  //first connect--register
                {
                    PioSetMode(LED_PIO_IND,pio_mode_pwm2);//fast bilnk ind 
                    PioEnablePWM(2,TRUE);             
                    PioConfigPWM(2,pio_pwm_mode_push_pull, 1, 200, 1,200, 1, 1, 5);
                }
                else if(bridgeFlag==0x0001) //not first time--bridge mode
                {
                    PioEnablePWM(2,FALSE);
                    PioSetMode(LED_PIO_IND,pio_mode_user);
                    PioSet(LED_PIO_IND,LED_ON);
                    if(current_sensor_status==sensor_status_normal)
                    {
                        CSR_MESH_ADVSCAN_PARAM_T param;
                        SleepModeChange(sleep_mode_never);
                        CsrMeshEnableListening( FALSE );
                        CsrMeshGetAdvScanParam(&param);
                        param.scan_duty_cycle = CUSTOM_RX_DUTY_CYCLE;
                        CsrMeshSetAdvScanParam(&param);
                        CsrMeshEnableListening(TRUE);        
                    }
                    //TimerCreate(30*SECOND,TRUE,turnOffMeshAndSleep);
                }
            }
            break;

            case app_state_disconnecting:
                GattDisconnectReq(g_tsapp_data.gatt_data.st_ucid);
            break;

            case app_state_sleep:
            {
                TimerCreate(0*SECOND,TRUE,turnOffMeshAndSleep);
            }
            break;
            
            default:
            break;
        }
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      ReportPanic
 *
 *  DESCRIPTION
 *      This function calls firmware panic routine and gives a single point
 *      of debugging any application level panics
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
extern void ReportPanic(app_panic_code panic_code)
{
    /* Raise panic */
    Panic(panic_code);
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      AppPowerOnReset
 *
 *  DESCRIPTION
 *      This user application function is called just after a power-on reset
 *      (including after a firmware panic), or after a wakeup from Hibernate or
 *      Dormant sleep states.
 *
 *      At the time this function is called, the last sleep state is not yet
 *      known.
 *
 *      NOTE: this function should only contain code to be executed after a
 *      power-on reset or panic. Code that should also be executed after an
 *      HCI_RESET should instead be placed in the AppInit() function.
 *
 *  RETURNS
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
void AppPowerOnReset(void)
{
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      AppInit
 *
 *  DESCRIPTION
 *      This user application function is called after a power-on reset
 *      (including after a firmware panic), after a wakeup from Hibernate or
 *      Dormant sleep states, or after an HCI Reset has been requested.
 *
 *      The last sleep state is provided to the application in the parameter.
 *
 *      NOTE: In the case of a power-on reset, this function is called
 *      after app_power_on_reset().
 *
 *  RETURNS
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
void AppInit(sleep_state last_sleep_state)
{
    uint16 gatt_db_length = 0;
    uint16 *p_gatt_db_pointer = NULL;
    CSR_MESH_ADVSCAN_PARAM_T param;
    
#ifdef USE_STATIC_RANDOM_ADDRESS
    /* Generate static random address for the CSRmesh Device. */
    generateStaticRandomAddress(&g_tsapp_data.random_bd_addr);

    /* Set the Static Random Address of the device. */
    GapSetRandomAddress(&g_tsapp_data.random_bd_addr);
#endif /* USE_STATIC_RANDOM_ADDRESS */

    /* Initialise the application timers */
    TimerInit(MAX_APP_TIMERS, (void*)app_timers);
    
#ifdef DEBUG_ENABLE
#ifdef ENABLE_TEMPERATURE_CONTROLLER
    /* Initialise UART and configure with
     * default baud rate and port configuration.
     */
    DebugInit(1, UartDataRxCallback, NULL);

    /* UART Rx threshold is set to 1,
     * so that every byte received will trigger the Rx callback.
     */
    UartRead(1, 0);
#else /* ENABLE_TEMPERATURE_CONTROLLER */
    DebugInit(0, NULL, NULL);
#endif /* ENABLE_TEMPERATURE_CONTROLLER */
#endif /* DEBUG_ENABLE */

#if defined(ENABLE_TEMPERATURE_CONTROLLER) && !defined(DEBUG_ENABLE)
    /* Initialise the Switch Hardware */
    IOTSwitchInit();
#endif /* defined(ENABLE_TEMPERATURE_CONTROLLER) && !defined(DEBUG_ENABLE) */
    //UartEnable(FALSE);
    IOTSwitchInit();
    /* Initialise Light and turn it off. */
    IOTLightControlDeviceInit();
    IOTLightControlDevicePower(FALSE);

    //DEBUG_STR("\r\nTemperature Sensor Application\r\n");

    /* Initialise GATT entity */
    GattInit();

    /* Install GATT Server support for the optional Write procedure
     * This is mandatory only if control point characteristic is supported.
     */
    GattInstallServerWriteLongReliable();
    
    /* Don't wakeup on UART RX line */
    SleepWakeOnUartRX(FALSE);

#ifdef NVM_TYPE_EEPROM
    /* Configure the NVM manager to use I2C EEPROM for NVM store */
    NvmConfigureI2cEeprom();
#elif NVM_TYPE_FLASH
    /* Configure the NVM Manager to use SPI flash for NVM store. */
    NvmConfigureSpiFlash();
#endif /* NVM_TYPE_EEPROM */

    NvmDisable();

    /* Initialise Temperature Sensor Hardware. */
    //if (!TempSensorHardwareInit(tempSensorEvent))
    {
        //DEBUG_STR("\r\nFailed to Initialise temperature sensor\r\n");
    }
    BME280_Init();
    /* Initialise the GATT and GAP data.
     * Needs to be done before readPersistentStore
     */
    appDataInit();
    bleTestServiceInit();

    /* Initialise Application specific Sensor Model Data.
     * This needs to be done before readPersistentStore.
     */
    sensor_state[CURRENT_AIR_TEMP_IDX].repeat_interval = 
                                        DEFAULT_REPEAT_INTERVAL & 0xFF;
    sensor_state[CURRENT_AIR_TEMP_IDX].type        = 
                                        sensor_type_internal_air_temperature;
    sensor_state[CURRENT_AIR_TEMP_IDX].value_size  = 
                                        (2*sizeof(SENSOR_FORMAT_TEMPERATURE_T));
    sensor_state[CURRENT_AIR_TEMP_IDX].value       = 
                                        (uint16 *)&current_air_temp;

    sensor_state[CURRENT_AIR_HUMI_IDX].repeat_interval = 
                                                DEFAULT_REPEAT_INTERVAL & 0xFF;
    sensor_state[CURRENT_AIR_HUMI_IDX].type        = 
                                        sensor_type_internal_humidity;
    sensor_state[CURRENT_AIR_HUMI_IDX].value_size  = 
                                        (2*sizeof(SENSOR_FORMAT_TEMPERATURE_T));
    sensor_state[CURRENT_AIR_HUMI_IDX].value       = 
                                        (uint16 *)&current_air_humi;/**/

    g_tsapp_data.sensor_data.num_types   = NUM_SENSORS_SUPPORTED;
    g_tsapp_data.sensor_data.sensor_list = sensor_state;

    /* Initialise the actuator model specific data */
    actuator_state[CURRENT_AIR_TEMP_IDX].type        = 
                                        sensor_type_internal_air_temperature;
    actuator_state[CURRENT_AIR_TEMP_IDX].value_size  = 
                                        (2*sizeof(SENSOR_FORMAT_TEMPERATURE_T));
    actuator_state[CURRENT_AIR_TEMP_IDX].value       = 
                                        (uint16 *)&current_air_temp;

    actuator_state[CURRENT_AIR_HUMI_IDX].type        = 
                                        sensor_type_internal_humidity;
    actuator_state[CURRENT_AIR_HUMI_IDX].value_size  = 
                                        (2*sizeof(SENSOR_FORMAT_TEMPERATURE_T));
    actuator_state[CURRENT_AIR_HUMI_IDX].value       = 
                                        (uint16 *)&current_air_humi;/**/

    g_tsapp_data.actuator_data.num_types   = NUM_SENSORS_SUPPORTED;
    g_tsapp_data.actuator_data.sensor_list = actuator_state;

    tempsensor_sample_tid = TIMER_INVALID;
    retransmit_tid  = TIMER_INVALID;
    repeat_interval_tid = TIMER_INVALID;

    /* Read persistent storage.
     * Call this before CsrMeshInit.
     */
    readPersistentStore();
    presetDevid();
    uint16 devid_nvm=0;
    devid_nvm=getDevid();
    bleTestServData.deviceID=devid_nvm;
    
    /* Initialise the CSRmesh */
    CsrMeshInit(&g_node_data);

    /* Update relay status */
    CsrMeshRelayEnable(g_tsapp_data.bearer_data.bearerRelayActive);

    /* Update promiscuous status */
    CsrMeshEnablePromiscuousMode(g_tsapp_data.bearer_data.bearerPromiscuous);

    /* Enable notifications */
    CsrMeshEnableRawMsgEvent(TRUE);

    /* Initialise Sensor Model. */
    SensorModelInit(sensor_actuator_model_groups, 
                                            NUM_SENSOR_ACTUATOR_MODEL_GROUPS);

    /* Initialise Actuator Model */
    ActuatorModelInit(sensor_actuator_model_groups, 
                                            NUM_SENSOR_ACTUATOR_MODEL_GROUPS);

    /* Initialise Attention Model */
    AttentionModelInit(attention_model_groups, NUM_ATTENTION_MODEL_GROUPS);

#ifdef ENABLE_FIRMWARE_MODEL
    /* Initialise Firmware Model */
    FirmwareModelInit();

    /* Set Firmware Version */
    g_tsapp_data.fw_version.major_version = APP_MAJOR_VERSION;
    g_tsapp_data.fw_version.minor_version = APP_MINOR_VERSION;
#endif /* ENABLE_FIRMWARE_MODEL */

#ifdef ENABLE_DATA_MODEL
    AppDataStreamInit(data_model_groups, NUM_DATA_MODEL_GROUPS);
#endif /* ENABLE_DATA_MODEL */

    /* Initialise Bearer Model */
    BearerModelInit();

#ifdef ENABLE_BATTERY_MODEL
    BatteryModelInit();
#endif

    /* Start CSRmesh */
    CsrMeshStart();

    /* Tell Security Manager module about the value it needs to Initialise it's
     * diversifier to.
     */
    SMInit(0);

    /* Initialise CSRmesh Temperature Sensor application State */
    g_tsapp_data.state = app_state_init;

    /* Get the stored adv scan parameters */
    CsrMeshGetAdvScanParam(&param);

    /* Read the mesh advertising parameter setting from the CS User Keys */
    param.advertising_interval =
                                CSReadUserKey(CSKEY_INDEX_CSRMESH_ADV_INTERVAL);
    param.advertising_time = CSReadUserKey(CSKEY_INDEX_CSRMESH_ADV_TIME);

    if (g_tsapp_data.assoc_state != app_state_associated)
    {
        initiateAssociation();
    }
    else
    {
        //DEBUG_STR("Temperature Sensor is associated\r\n");
       if(IsSensorConfigured())
       {
            param.scan_duty_cycle = DEFAULT_RX_DUTY_CYCLE;
            //DEBUG_STR("Temp Sensor Configured, Moving to Low Power Mode\r\n");

            /* Issue a Read to start sampling timer. */
            TempSensorRead();

            /* Start the timer for next sample. */
            //tempsensor_sample_tid = TimerCreate(
            //                            (uint32)TEMPERATURE_SAMPLING_INTERVAL, 
            //                           TRUE,
            //                           tempSensorSampleIntervalTimeoutHandler);

            //if(sensor_state[DESIRED_AIR_TEMP_IDX].repeat_interval !=0 ||
            //   sensor_state[CURRENT_AIR_TEMP_IDX].repeat_interval !=0)
            {
                //startRepeatIntervalTimer();
            }
        }
    }
    /* Set the Advertising and Scan parameters */
    CsrMeshSetAdvScanParam(&param);

#ifdef ENABLE_ACK_MODE 
    resetHeaterList();
#endif /* ENABLE_ACK_MODE */


    if(current_sensor_status==sensor_status_init)
    {
        /* Tell GATT about our database. We will get a GATT_ADD_DB_CFM event when
         * this has completed.
         */
        p_gatt_db_pointer = GattGetDatabase(&gatt_db_length);
        GattAddDatabaseReq(gatt_db_length, p_gatt_db_pointer);        
    }
    
    if( (PioGet(RTC_INT)==PRESSED)&&(current_sensor_status!=sensor_status_init) ) //if user press button during power on, unreg the device
    {
        current_sensor_status=sensor_status_init_pressed; //change status
        TimerCreate(100*MILLISECOND,TRUE,handleLongButtonPress);           
    }
    if(current_sensor_status==sensor_status_normal)
    {
        SleepModeChange(sleep_mode_never);
        CsrMeshEnableListening( FALSE );
        CsrMeshGetAdvScanParam(&param);
        param.scan_duty_cycle = CUSTOM_RX_DUTY_CYCLE;
        CsrMeshSetAdvScanParam(&param);
        CsrMeshEnableListening(TRUE);        
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      AppProcessSystemEvent
 *
 *  DESCRIPTION
 *      This user application function is called whenever a system event, such
 *      as a battery low notification, is received by the system.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
extern void AppProcessSystemEvent(sys_event_id id, void *data)
{
    switch(id)
    {
        case sys_event_pio_changed:
//#if defined(ENABLE_TEMPERATURE_CONTROLLER) && !defined(DEBUG_ENABLE)
            if(0)handlePIOChangedEvent(((pio_changed_data*)data)->pio_cause);
//#endif /* defined(ENABLE_TEMPERATURE_CONTROLLER) && !defined(DEBUG_ENABLE) */
        break;

        case sys_event_pio_ctrlr:
            /* Process the interrupt from the PIO controller */
            //SpiMasterProcessPioControllerEvent((uint16 *)data);
        break;
        
        default:
            /* Ignore anything else */
        break;
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      handlePIOChangedEvent
 *
 *  DESCRIPTION
 *      This function handles PIO Changed event
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void handlePIOChangedEvent(uint32 pio_changed)
{
    if (pio_changed & PIO_BIT_MASK(RTC_INT))
    {
        /* Check if PIO Changed from previous state */
        if ( PioGet(RTC_INT) == FALSE )
        {
            /* Disable further Events */
            PioSetEventMask(1L<<RTC_INT, pio_event_mode_disable);
            if(ledFlashtid!=TIMER_INVALID)
            {
                TimerDelete(ledFlashtid);
                ledFlashtid=TIMER_INVALID;
            }
            PioEnablePWM(2,FALSE);
            PioSetMode(LED_PIO_IND,pio_mode_user);
            PioSet(LED_PIO_IND,LED_ON);//ON
            //TimerDelete(debounce_tid);
            
            /*uint16 clearRTCInterruptFlag[1]={PACK(0x11,0x11)};
            uint16 cmdLength=0;            
            clearSpiCmdAndLen();        
            cmdArrayCat(clearRTCInterruptFlag,sizeof(clearRTCInterruptFlag)/sizeof(uint16));//1
            cmdLength=checkCmdLength();        
            deviceType=0;//selected device type:RTC(0)/BME280(1)
            spiTransfer(deviceType,PIO_CTRLR_CODE_ADDR0,2*cmdLength);*/        

                TimerCreate(0*MILLISECOND,TRUE,turnOnMesh);
            /*if(devTestIndex>=2) devTestIndex=0;
            targetDevID=devTest[devTestIndex];               
            devTestIndex++;*/
            //targetDevID=0x8311;
            bleTestServData.deviceID=targetDevID;
            bleTestServData.temp=0;
            bleTestServData.humi=0;//profile两位精度
            bleTestServData.pressure=0;//profile一位精度
            bleTestServData.currentVal=0;
            testStreamData=0;
            
            //time10sCount=0;
            //timeInterval_tid=TimerCreate(10*MILLISECOND,TRUE,countTimeIntervalHandler);
            //uint8 requestDat[1];
            //sendCustomCmd(500,1,requestDat,1,REQUEST_DATA,targetDevID);//4*500ms
                            
            if(1)debounceINT_tid=TimerCreate(4*MINUTE,TRUE,turnOffMeshAndSleep);//onDuration          
        }
    }
}

void countTimeIntervalHandler(timer_id tid)
{
    TimerDelete(timeInterval_tid);timeInterval_tid=TIMER_INVALID;
    time10sCount++;
    uint8 requestDat[1];
    if(time10sCount>=50) sendCustomCmd(500,1,requestDat,1,REQUEST_DATA,targetDevID);
    timeInterval_tid=TimerCreate(10*MILLISECOND,TRUE,countTimeIntervalHandler);
}
void turnOnMesh(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
    SleepModeChange(sleep_mode_never);
    CSR_MESH_ADVSCAN_PARAM_T param;//20160831
    CsrMeshEnableListening( FALSE );
    CsrMeshGetAdvScanParam(&param);
    param.scan_duty_cycle = CUSTOM_RX_DUTY_CYCLE;
    CsrMeshSetAdvScanParam(&param);
    CsrMeshEnableListening(TRUE);

    //TimerCreate(500*MILLISECOND,TRUE,sendMeshCmdHandler);    
    //meshStatus=TRUE;//ON:TRUE/OFF:FALSE 
    //Nvm_Write(&meshStatus,1,NVM_MESH_STATUS);//20170216
#if 0
    uint16 clearRTCInterruptFlag[1]={PACK(0x11,0x12)};
    uint16 cmdLength=0;
    
    //if(writeRTCTimes==0)
    {
        clearSpiCmdAndLen();
        cmdArrayCat(clearRTCInterruptFlag,sizeof(clearRTCInterruptFlag)/sizeof(uint16));//1
        cmdLength=checkCmdLength();        
        deviceType=0;//selected device type:RTC(0)/BME280(1)
        spiTransfer(deviceType,PIO_CTRLR_CODE_ADDR0,2*cmdLength);
    }
#endif    
    //TimerCreate(100*MILLISECOND,TRUE,handleCheckRTCContent);//20170510:remove write confirming
}
void sendMeshCmdHandler(timer_id tid)
{
    TimerDelete(tid);
    tid=TIMER_INVALID;
    PioSetEventMask(1L<<RTC_INT,pio_event_mode_both);
    //PioSet(LED_PIO_IND,LED_OFF);//OFF
}
void turnOffMeshAndSleep(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;  
    PioSet(LED_PIO_IND,LED_OFF);

    CsrMeshEnableListening(FALSE);    
    CSR_MESH_ADVSCAN_PARAM_T param;//20160831
    CsrMeshGetAdvScanParam(&param);
    param.scan_duty_cycle = OFF_RX_DUTY_CYCLE;
    CsrMeshSetAdvScanParam(&param);
    
    PioSetModes( (1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|
               (1L<<SPI_MASTER_PIO_MISO),pio_mode_user );//1100 0001 0000:0xc10
    PioSetDirs( (1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|(1L<<SPI_MASTER_PIO_MISO),
               (1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|(1L<<SPI_MASTER_PIO_MISO) );
    PioSetPullModes((1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|(1L<<SPI_MASTER_PIO_MISO),
                    pio_mode_strong_pull_up);//close the SPI bus      
    SleepModeChange(sleep_mode_deep);
    
    //meshStatus=FALSE;//ON:TRUE/OFF:FALSE 
    //Nvm_Write(&meshStatus,1,NVM_MESH_STATUS);//20170216
    //uint16 mainVolt=BatteryReadVoltage();
    //if(mainVolt>=3000)
        PioSetEventMask(1L<<RTC_INT,pio_event_mode_both);//re-receive interrupt for next recycle
    //otherwise to weaken leakage,turn off mesh activity forever when VDD_BAT lower than 3V
}
/*----------------------------------------------------------------------------*
 *  NAME
 *      AppProcessLmEvent
 *
 *  DESCRIPTION
 *      This user application function is called whenever a LM-specific event is
 *      received by the system.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
extern bool AppProcessLmEvent(lm_event_code event_code,
                              LM_EVENT_T *p_event_data)
{

    switch(event_code)
    {
        /* Handle events received from Firmware */

        case GATT_ADD_DB_CFM:
            /* Attribute database registration confirmation */
            handleSignalGattAddDBCfm((GATT_ADD_DB_CFM_T*)p_event_data);
        break;

        case GATT_CANCEL_CONNECT_CFM:
            /* Confirmation for the completion of GattCancelConnectReq()
             * procedure
             */
            handleSignalGattCancelConnectCfm();
        break;

        case LM_EV_CONNECTION_COMPLETE:
            /* Handle the LM connection complete event. */
            handleSignalLmEvConnectionComplete((LM_EV_CONNECTION_COMPLETE_T*)
                                                                p_event_data);
        break;

        case GATT_CONNECT_CFM:
            /* Confirmation for the completion of GattConnectReq()
             * procedure
             */
            handleSignalGattConnectCfm((GATT_CONNECT_CFM_T*)p_event_data);
        break;

        case SM_SIMPLE_PAIRING_COMPLETE_IND:
            /* Indication for completion of Pairing procedure */
            handleSignalSmSimplePairingCompleteInd(
                (SM_SIMPLE_PAIRING_COMPLETE_IND_T*)p_event_data);
        break;

        case LM_EV_ENCRYPTION_CHANGE:
            /* Indication for encryption change event */
            /* Nothing to do */
        break;

        /* Received in response to the LsConnectionParamUpdateReq()
         * request sent from the slave after encryption is enabled. If
         * the request has failed, the device should again send the same
         * request only after Tgap(conn_param_timeout). Refer Bluetooth 4.0
         * spec Vol 3 Part C, Section 9.3.9 and HID over GATT profile spec
         * section 5.1.2.
         */
        case LS_CONNECTION_PARAM_UPDATE_CFM:
            handleSignalLsConnParamUpdateCfm(
                (LS_CONNECTION_PARAM_UPDATE_CFM_T*) p_event_data);
        break;

        case LM_EV_CONNECTION_UPDATE:
            /* This event is sent by the controller on connection parameter
             * update.
             */
            handleSignalLmConnectionUpdate(
                            (LM_EV_CONNECTION_UPDATE_T*)p_event_data);
        break;

        case LS_CONNECTION_PARAM_UPDATE_IND:
            /* Indicates completion of remotely triggered Connection
             * parameter update procedure
             */
            handleSignalLsConnParamUpdateInd(
                            (LS_CONNECTION_PARAM_UPDATE_IND_T *)p_event_data);
        break;

        case GATT_ACCESS_IND:
            /* Indicates that an attribute controlled directly by the
             * application (ATT_ATTR_IRQ attribute flag is set) is being
             * read from or written to.
             */
            handleSignalGattAccessInd((GATT_ACCESS_IND_T*)p_event_data);
        break;

        case GATT_DISCONNECT_IND:
            /* Disconnect procedure triggered by remote host or due to
             * link loss is considered complete on reception of
             * LM_EV_DISCONNECT_COMPLETE event. So, it gets handled on
             * reception of LM_EV_DISCONNECT_COMPLETE event.
             */
         break;

        case GATT_DISCONNECT_CFM:
            /* Confirmation for the completion of GattDisconnectReq()
             * procedure is ignored as the procedure is considered complete
             * on reception of LM_EV_DISCONNECT_COMPLETE event. So, it gets
             * handled on reception of LM_EV_DISCONNECT_COMPLETE event.
             */
        break;

        case LM_EV_DISCONNECT_COMPLETE:
        {
            /* Disconnect procedures either triggered by application or remote
             * host or link loss case are considered completed on reception
             * of LM_EV_DISCONNECT_COMPLETE event
             */
             handleSignalLmDisconnectComplete(
                    &((LM_EV_DISCONNECT_COMPLETE_T *)p_event_data)->data);
        }
        break;

        case LM_EV_ADVERTISING_REPORT:
            CsrMeshProcessMessage((LM_EV_ADVERTISING_REPORT_T*)p_event_data);
        break;

        case LS_RADIO_EVENT_IND:
        {
            CsrMeshHandleRadioEvent();
        }
        break;

        default:
            /* Ignore any other event */
        break;

    }

    return TRUE;
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      AppProcessCsrMeshEvent
 *
 *  DESCRIPTION
 *      This user application function is called whenever a CSRmesh event
 *      is received by the system.
 *
 * PARAMETERS
 *      event_code csr_mesh_event_t
 *      data       Data associated with the event
 *      length     Length of the data
 *      state_data Pointer to the variable pointing to state data.
 *
 * RETURNS
 *      TRUE if the app has finished with the event data; the control layer
 *      will free the buffer.
 *----------------------------------------------------------------------------*/
extern void AppProcessCsrMeshEvent(csr_mesh_event_t event_code, uint8* data,
                                   uint16 length, void **state_data)
{
    bool update_lastetag = FALSE;

    switch(event_code)
    {
        case CSR_MESH_ASSOCIATION_REQUEST:
        {
            //DEBUG_STR("Association Started\r\n");
            if (g_tsapp_data.assoc_state != app_state_association_started)
            {
                g_tsapp_data.assoc_state = app_state_association_started;
            }
            TimerDelete(g_tsapp_data.mesh_device_id_advert_tid);
            g_tsapp_data.mesh_device_id_advert_tid = TIMER_INVALID;

            /* Blink Light in Yellow to indicate association started */
            IOTLightControlDeviceBlink(127, 127, 0, 32, 32);
        }
        break;

        case CSR_MESH_KEY_DISTRIBUTION:
        {
            //DEBUG_STR("Association complete\r\n");
            g_tsapp_data.assoc_state = app_state_associated;

            /* Write association state to NVM */
            Nvm_Write((uint16 *)&g_tsapp_data.assoc_state, 1,
                     NVM_OFFSET_ASSOCIATION_STATE);

            /* Save the network key on NVM */
            Nvm_Write((uint16 *)data, sizeof(CSR_MESH_NETWORK_KEY_T), 
                                                        NVM_OFFSET_NETWORK_KEY);

            /* The association is complete set LE bearer to non-promiscuous.*/
            g_tsapp_data.bearer_data.bearerPromiscuous &= ~BLE_BEARER_MASK;
            CsrMeshEnablePromiscuousMode(
                                    g_tsapp_data.bearer_data.bearerPromiscuous);

            /* Update Bearer Model Data to NVM */
            Nvm_Write((uint16 *)&g_tsapp_data.bearer_data,
                      sizeof(BEARER_MODEL_STATE_DATA_T),NVM_BEARER_DATA_OFFSET);

            /* When MESH_BRIDGE_SERVICE is not supported, Temperature Sensor 
             * needs to be associated with CSRmesh network, before it can send 
             * commands. Stop the blue led blinking visual indication, as 
             * Temperature Sensor is now associated to network.
             */
            IOTLightControlDevicePower(FALSE);
        }
        break;

        case CSR_MESH_ASSOCIATION_ATTENTION:
        {
            CSR_MESH_ASSOCIATION_ATTENTION_DATA_T *attn_data;

            attn_data = (CSR_MESH_ASSOCIATION_ATTENTION_DATA_T *)data;

            /* Delete attention timer if it exists */
            if (TIMER_INVALID != attn_tid)
            {
                TimerDelete(attn_tid);
                attn_tid = TIMER_INVALID;
            }
            /* If attention Enabled */
            if (attn_data->attract_attention)
            {
                /* Create attention duration timer if required */
                if(attn_data->duration != 0xFFFF)
                {
                    attn_tid = TimerCreate(attn_data->duration * MILLISECOND, 
                                                        TRUE, attnTimerHandler);
                }
                /* Enable Green light blinking to attract attention */
                IOTLightControlDeviceBlink(0, 127, 0, 16, 16);
            }
            else
            {
                if(g_tsapp_data.assoc_state == app_state_not_associated)
                {
                    /* Blink blue to indicate not associated status */
                    IOTLightControlDeviceBlink(0, 0, 127, 32, 32);
                }
                else
                {
                    /* Restore the light Power State */
                    IOTLightControlDevicePower(FALSE);
                }
            }
        }
        break;

        case CSR_MESH_UPDATE_MSG_SEQ_NUMBER:
        {
            /* Sequence number has updated, store it in NVM */
            Nvm_Write((uint16 *)data, 2, NVM_OFFSET_SEQUENCE_NUMBER);
        }
        break;

        case CSR_MESH_CONFIG_RESET_DEVICE:
        {
            uint16 i;
            //DEBUG_STR("Reset Device\r\n");

            /* Move device to dissociated state */
            g_tsapp_data.assoc_state = app_state_not_associated;

            /* Write association state to NVM */
            Nvm_Write((uint16 *)&g_tsapp_data.assoc_state,
                     sizeof(g_tsapp_data.assoc_state),
                     NVM_OFFSET_ASSOCIATION_STATE);

            /* Delete Temperature Sensor Timers. */
            TimerDelete(retransmit_tid);
            retransmit_tid = TIMER_INVALID;

            TimerDelete(tempsensor_sample_tid);
            tempsensor_sample_tid = TIMER_INVALID;


            /* Reset the supported model groups and save it to NVM */
            /* Sensor model */
            MemSet(sensor_actuator_model_groups, 0x0000,
                                        sizeof(sensor_actuator_model_groups));
            Nvm_Write(sensor_actuator_model_groups, 
                sizeof(sensor_actuator_model_groups),
                    NVM_OFFSET_SENSOR_ACTUATOR_MODEL_GROUPS);

            /* Attention model */
            MemSet(attention_model_groups, 0x0000, 
                                            sizeof(attention_model_groups));
            Nvm_Write(attention_model_groups, sizeof(attention_model_groups),
                                            NVM_OFFSET_ATTENTION_MODEL_GROUPS);

#ifdef ENABLE_DATA_MODEL
            /* Data stream model */
            MemSet(data_model_groups, 0x0000, sizeof(data_model_groups));
            Nvm_Write((uint16 *)data_model_groups, sizeof(data_model_groups),
                                            NVM_OFFSET_DATA_MODEL_GROUPS);
#endif /* ENABLE_DATA_MODEL */

            /* Reset Sensor State. */
            for (i = 0; i < NUM_SENSORS_SUPPORTED; i++)
            {
                writeSensorDataToNVM(i);
            }

            current_air_temp = 0;
            last_bcast_air_temp  = 0;
            current_desired_air_temp = 0;
            last_bcast_desired_air_temp = 0;

            /* Enable promiscuous mode on un-associated devices so that they 
             * relay all the messages. This helps propagate messages(MCP) based 
             * on the newly assigned network key as they will be relayed also by
             * the devices not yet associated.
             */
            g_tsapp_data.bearer_data.bearerPromiscuous = 
                               (BLE_BEARER_MASK | BLE_GATT_SERVER_BEARER_MASK);
            CsrMeshEnablePromiscuousMode(
                                g_tsapp_data.bearer_data.bearerPromiscuous);

            /* Update Bearer Model Data to NVM */
            Nvm_Write((uint16 *)&g_tsapp_data.bearer_data,
                      sizeof(BEARER_MODEL_STATE_DATA_T),NVM_BEARER_DATA_OFFSET);

            /* Start Mesh association again */
            initiateAssociation();
        }
        break;

        case CSR_MESH_CONFIG_GET_VID_PID_VERSION:
        {
            if (state_data != NULL)
            {
                *state_data = (void *)&vid_pid_info;
            }
        }
        break;

        case CSR_MESH_CONFIG_GET_APPEARANCE:
        {
            if (state_data != NULL)
            {
                *state_data = (void *)&device_appearance;
            }
        }
        break;

        case CSR_MESH_GROUP_SET_MODEL_GROUPID:
        {
            /* Save Group Information here */
            update_lastetag = handleCsrMeshGroupSetMsg(data, length);
        }
        break;

        case CSR_MESH_CONFIG_DEVICE_IDENTIFIER:
        {
            Nvm_Write((uint16 *)data, 1, NVM_OFFSET_DEVICE_ID);
            //DEBUG_STR("Device ID received:");
            //DEBUG_U16(((uint16)data[0]));
            //DEBUG_STR("\r\n");
        }
        break;

#ifdef ENABLE_BATTERY_MODEL
        case CSR_MESH_BATTERY_GET_STATE:
        {
            /* Initialise battery state. IOT  boards (H13323) are battery powered */
            g_tsapp_data.battery_data.battery_state = 
                                            BATTERY_MODEL_STATE_POWERING_DEVICE;
            /* Read Battery Level */
            g_tsapp_data.battery_data.battery_level = ReadBatteryLevel();

            if(g_tsapp_data.battery_data.battery_level == 0)
            {
                /* Voltage is below flat battery voltage. Set the needs 
                 * replacement flag in the battery state
                 */
                g_tsapp_data.battery_data.battery_state |=
                                          BATTERY_MODEL_STATE_NEEDS_REPLACEMENT;
            }

            /* Pass Battery state data to model */
            if (state_data != NULL)
            {
                *state_data = (void *)&g_tsapp_data.battery_data;
            }
        }
        break;
#endif /* ENABLE_BATTERY_MODEL */

        case CSR_MESH_BEARER_SET_STATE:
        {
            uint8 *pData = data;
            g_tsapp_data.bearer_data.bearerRelayActive = BufReadUint16(&pData);
            g_tsapp_data.bearer_data.bearerEnabled     = BufReadUint16(&pData);
            g_tsapp_data.bearer_data.bearerPromiscuous = BufReadUint16(&pData);

            /* BLE Advert Bearer is always enabled on this device. */
            g_tsapp_data.bearer_data.bearerEnabled    |= BLE_BEARER_MASK;

            /* Filter the supported bearers from the bitmap received */
            g_tsapp_data.bearer_data.bearerRelayActive = 
                g_tsapp_data.bearer_data.bearerRelayActive & 
                    (BLE_BEARER_MASK | BLE_GATT_SERVER_BEARER_MASK);

            /* Filter the supported bearers from the bitmap received */
            g_tsapp_data.bearer_data.bearerEnabled = 
                g_tsapp_data.bearer_data.bearerEnabled & 
                    (BLE_BEARER_MASK | BLE_GATT_SERVER_BEARER_MASK);

            /* Filter the supported bearers from the bitmap received */
            g_tsapp_data.bearer_data.bearerPromiscuous = 
                g_tsapp_data.bearer_data.bearerPromiscuous & 
                    (BLE_BEARER_MASK | BLE_GATT_SERVER_BEARER_MASK);

            /* Update the saved values */
            bearer_relay_active = g_tsapp_data.bearer_data.bearerRelayActive;
            bearer_promiscuous = g_tsapp_data.bearer_data.bearerPromiscuous;

            /* Update new bearer state */
            CsrMeshRelayEnable(g_tsapp_data.bearer_data.bearerRelayActive);
            CsrMeshEnablePromiscuousMode(
                                    g_tsapp_data.bearer_data.bearerPromiscuous);

            /* Update Bearer Model Data to NVM */
            Nvm_Write((uint16 *)&g_tsapp_data.bearer_data,
                      sizeof(BEARER_MODEL_STATE_DATA_T),NVM_BEARER_DATA_OFFSET);

            if(g_tsapp_data.state != app_state_connected) 
            {
                if(g_tsapp_data.bearer_data.bearerEnabled 
                                                & BLE_GATT_SERVER_BEARER_MASK)
                {
                    AppSetState(app_state_advertising);
                }
                else
                {
                    AppSetState(app_state_idle);
                }
            }

            update_lastetag = TRUE;
        }
        /* Fall through */
        case CSR_MESH_BEARER_GET_STATE:
        {
            if (state_data != NULL)
            {
                *state_data = (void *)&g_tsapp_data.bearer_data;
            }
        }
        break;

#ifdef ENABLE_FIRMWARE_MODEL
        case CSR_MESH_FIRMWARE_GET_VERSION_INFO:
        {
            /* Send Firmware Version data to the model */
            if (state_data != NULL)
            {
                *state_data = (void *)&g_tsapp_data.fw_version;
            }
        }
        break;

        case CSR_MESH_FIRMWARE_UPDATE_REQUIRED:
        {
            BD_ADDR_T *pBDAddr = NULL;
#ifdef USE_STATIC_RANDOM_ADDRESS
            pBDAddr = &g_tsapp_data.random_bd_addr;
#endif /* USE_STATIC_RANDOM_ADDRESS */

            //DEBUG_STR("\r\n FIRMWARE UPDATE IN PROGRESS \r\n");

            /* Write the value CSR_OTA_BOOT_LOADER to NVM so that
             * it starts in OTA mode upon reset
             */
            OtaWriteCurrentApp(csr_ota_boot_loader,
                               FALSE,   /* is bonded */
                               NULL,    /* Typed host BD Address */
                               0,       /* Diversifier */
                               pBDAddr, /* local_random_address */
                               NULL,    /* irk */
                               FALSE    /* service_changed_config */
                              );

           /* Defer OTA Reset for half a second to ensure that,
            * acknowledgements are sent before reset.
            */
           ota_rst_tid = TimerCreate(OTA_RESET_DEFER_DURATION, TRUE,
                                     issueOTAReset);

           /* Update LastETag. */
           update_lastetag = TRUE;
        }
        break;
#endif /* ENABLE_FIRMWARE_MODEL */

        case CSR_MESH_ATTENTION_SET_STATE:
        {
            ATTENTION_MODEL_STATE_DATA_T   attn_data;

            /* Read the data */
            attn_data.attract_attn  = BufReadUint8(&data);
            attn_data.attn_duration = BufReadUint16(&data);

            /* Delete attention timer if it exists */
            if (TIMER_INVALID != attn_tid)
            {
                TimerDelete(attn_tid);
                attn_tid = TIMER_INVALID;
            }

            /* If attention Enabled */
            if (attn_data.attract_attn)
            {
                /* Create attention duration timer if required */
                if (attn_data.attn_duration != 0xFFFF)
                {
                    attn_tid = TimerCreate(
                        (uint32)attn_data.attn_duration * MILLISECOND,
                            TRUE, attnTimerHandler);
                }
                /* Enable Red light blinking to attract attention */
                IOTLightControlDeviceBlink(127, 0, 0, 32, 32);

                /* Change the Rx scan duty cycle on enabling attention */
                EnableHighDutyScanMode(TRUE);
            }
            else
            {
                /* Restore Light State */
                IOTLightControlDeviceSetColor(0,0,0);

                /* Restore the light Power State */
                IOTLightControlDevicePower(FALSE);

                /* Set back the scan to low duty cycle only if the device has
                 * already been grouped.
                 */
                EnableHighDutyScanMode(FALSE);
            }

            /* Send response data to model */
            if (state_data != NULL)
            {
                *state_data = (void *)&attn_data;
            }

            /* Debug logs */
            //DEBUG_STR("\r\n ATTENTION_SET_STATE : Enable :");
            //DEBUG_U8(attn_data.attract_attn);
            //DEBUG_STR("Duration : ");
            //DEBUG_U16(attn_data.attn_duration);
            //DEBUG_STR("\r\n");

        }
        break;

#ifdef ENABLE_DATA_MODEL
        /* Data stream model messages */
        case CSR_MESH_DATA_STREAM_SEND_CFM:
        {
            handleCSRmeshDataStreamSendCfm((CSR_MESH_STREAM_EVENT_T *)data);
        }
        break;

        case CSR_MESH_DATA_STREAM_DATA_IND:
        {
            handleCSRmeshDataStreamDataInd((CSR_MESH_STREAM_EVENT_T *)data);
        }
        break;

        /* Stream flush indication */
        case CSR_MESH_DATA_STREAM_FLUSH_IND:
        {
            handleCSRmeshDataStreamFlushInd((CSR_MESH_STREAM_EVENT_T *)data);
        }
        break;

        /* Received a single block of data */
        case CSR_MESH_DATA_BLOCK_IND:
        {
            handleCSRmeshDataBlockInd((CSR_MESH_STREAM_EVENT_T *)data);
        }
        break;
#endif /* ENABLE_DATA_MODEL */

        /* Actuator Messages handling */
        case CSR_MESH_ACTUATOR_GET_TYPES:
        {
            if (state_data != NULL)
            {
                *state_data = (void *)&g_tsapp_data.actuator_data;
            }
        }
        break;

        case CSR_MESH_ACTUATOR_SET_VALUE_NO_ACK:
        case CSR_MESH_ACTUATOR_SET_VALUE:
        {
            uint8 *p_data = data;
            sensor_type_t type = sensor_type_invalid;
            type = BufReadUint16(&p_data);
            *state_data = NULL;

            if(type == sensor_type_desired_air_temperature)
            {
                /* Length of the value of this type is 16 bits */
                current_desired_air_temp = BufReadUint16(&p_data);

                //DEBUG_STR(" RECEIVED DESIRED TEMP : ");
                //printInDecimal(current_desired_air_temp/32);
                //DEBUG_STR(" kelvin\r\n");

                /* Duplicate messages could be filtered here */
                if(current_desired_air_temp != last_bcast_desired_air_temp)
                {
                    /* Set last Broadcast Temperature to current temperature. */
                    last_bcast_desired_air_temp = current_desired_air_temp;
                    //startTempTransmission();
                }

                /* assign the state data to acknowledge the write */
                *state_data = (void *)&g_tsapp_data.actuator_data;
            }
        }
        break;

        /* Sensor Messages handling */
        case CSR_MESH_SENSOR_READ_VALUE:
        case CSR_MESH_SENSOR_MISSING:
        {
            SENSOR_MODEL_EVENT_T *sensor_event = (SENSOR_MODEL_EVENT_T*)data;
            uint8 *p_data = sensor_event->msg;
            uint8 *p_data_end = sensor_event->msg + sensor_event->msg_len;
            sensor_type_t type = sensor_type_invalid;

            while( p_data < p_data_end)
            {
                type = BufReadUint16(&p_data);
                if(type == sensor_type_desired_air_temperature || 
                   type == sensor_type_internal_air_temperature )
                {
                    //DEBUG_STR(" READ/MISSING : ");
                    //startTempTransmission();
                    break;
                }
                else
                {
                   /* Not interested in other sensor types. Move the pointer 
                    * ahead with the expected length to decode the next sensor
                    * type value.
                    */
                    if(type <= SENSOR_TYPE_SUPPORTED_MAX)
                    {
                        p_data += sensor_type_length[type];
                    }
                    /* As the values have to be written in ascending order if 
                     * sensor type recvd is more than supported type, we need
                     * not decode the other values.
                     */
                    else
                    {
                        break;
                    }
                }
            }
        }
        /* Code Fall-Through */
        case CSR_MESH_SENSOR_GET_TYPES:
        case CSR_MESH_SENSOR_GET_STATE:
        {
            if (state_data != NULL)
            {
                *state_data = (void *)&g_tsapp_data.sensor_data;
            }
        }
        break;

        case CSR_MESH_SENSOR_SET_STATE:
        {
            SENSOR_MODEL_EVENT_T *sensor_event = (SENSOR_MODEL_EVENT_T*)data;
            uint8 *p_data = sensor_event->msg;
            sensor_type_t type = sensor_type_invalid;
            type = BufReadUint16(&p_data);

            /* repeat interval of desired or current temp has changed */
            if(type == sensor_type_desired_air_temperature)
            {
                sensor_state[DESIRED_AIR_TEMP_IDX].repeat_interval = 
                                                   BufReadUint8(&p_data) & 0xFF;
                writeSensorDataToNVM(DESIRED_AIR_TEMP_IDX);
            }
            else if(sensor_type_internal_air_temperature)
            {
                sensor_state[CURRENT_AIR_TEMP_IDX].repeat_interval = 
                                                   BufReadUint8(&p_data) & 0xFF;
                writeSensorDataToNVM(CURRENT_AIR_TEMP_IDX);
            }

            if(sensor_state[CURRENT_AIR_TEMP_IDX].repeat_interval != 0 ||
               sensor_state[DESIRED_AIR_TEMP_IDX].repeat_interval != 0)
            {
                //startRepeatIntervalTimer();
            }
            else
            {
                /* Delete the repeat interval timer */
                TimerDelete(repeat_interval_tid);
                repeat_interval_tid = TIMER_INVALID;
            }

            if (state_data != NULL)
            {
                *state_data = (void *)&g_tsapp_data.sensor_data;
            }
        }
        break;

        case CSR_MESH_SENSOR_VALUE:
        {
//#ifdef ENABLE_ACK_MODE
            SENSOR_MODEL_EVENT_T *sensor_event = (SENSOR_MODEL_EVENT_T*)data;
            uint8 *p_data = sensor_event->msg;
            uint8 *p_data_end = sensor_event->msg + sensor_event->msg_len;
            sensor_type_t type = sensor_type_invalid;
            //SENSOR_FORMAT_TEMPERATURE_T current_air_humi_recvd = 0;
            //SENSOR_FORMAT_TEMPERATURE_T current_air_temp_recvd = 0;

            while( p_data < p_data_end)
            {
                type = BufReadUint16(&p_data);
                /*switch(type)
                {
                    case sensor_type_internal_air_temperature:
                    {
                        current_air_temp = BufReadUint16(&p_data);
                        //sensorDatIntegrityFlag|=0x1;
                        break;
                    }
                    case sensor_type_internal_humidity:
                    {
                        current_air_humi = BufReadUint16(&p_data);
                        //sensorDatIntegrityFlag|=0x2;
                        break;
                    }
                    default:break;
                }*/
                //if(sensorDatIntegrityFlag==0x03)
                //{
                //    bleTestServData.temp=current_air_temp;
                //    bleTestServData.humi=current_air_humi;//profile两位精度                    
                //}
                //else
                //{
                   /* Not interested in other sensor types. Move the pointer 
                    * ahead with the expected length to decode the next sensor
                    * type value.
                    */
                    //if(type <= SENSOR_TYPE_SUPPORTED_MAX)
                    //{
                    //    p_data += sensor_type_length[type];
                    //}
                    /* As the values have to be written in ascending order if 
                     * sensor type recvd is more than supported type, we need
                     * not decode the other values.
                     */
                    //else
                    //{
                        break;
                    //}
                //}
            }
#if 0
            /* We have received acknowledgement for the write_value sent.
             * update the acknowlege heater device list.
             */
            if(current_air_temp_recvd == last_bcast_air_temp &&
               desired_air_temp_recvd == last_bcast_desired_air_temp)
            {
                uint16 index;
                bool dev_found = FALSE;
                uint16 src_id = sensor_event->sensor_model_info.source_id;

                /* Check whether the heater is present in the list if its 
                 * present then update the ack recvd from the heater,
                 * otherwise add the new heater onto the list and increase 
                 * the count.
                 */
                 for(index = 0; index < MAX_HEATERS_IN_GROUP; index++)
                 {
                    if(heater_list[index].dev_id == src_id)
                    {
                        heater_list[index].ack_recvd = TRUE;
                        dev_found = TRUE;
                        heater_list[index].no_response_count = 0;
                        break;
                    }
                 }
                 if(dev_found == FALSE)
                 {
                    for(index = 0; index < MAX_HEATERS_IN_GROUP; index++)
                    {
                        if(heater_list[index].dev_id == 
                                                CSR_MESH_BROADCAST_ID)
                        {
                            heater_list[index].dev_id = src_id;
                            heater_list[index].ack_recvd = TRUE;
                            heater_list[index].no_response_count = 0;
                            break;
                        }
                    }
                 }
            }
#endif /* ENABLE_ACK_MODE */
        }
        break;

        case CSR_MESH_SENSOR_WRITE_VALUE:
        case CSR_MESH_SENSOR_WRITE_VALUE_NO_ACK:
        {
            SENSOR_MODEL_EVENT_T *sensor_event = (SENSOR_MODEL_EVENT_T*)data;
            uint8 *p_data = sensor_event->msg;
            uint8 *p_data_end = sensor_event->msg + sensor_event->msg_len;
            uint8 index = 0;
            sensor_type_t type[3] = {sensor_type_invalid};
            bool desired_temp_recvd = FALSE;

            while( p_data < p_data_end)
            {
                type[index] = BufReadUint16(&p_data);
                if(type[index] == sensor_type_desired_air_temperature)
                {
                    /* Length of the value of this type is 16 bits */
                    current_desired_air_temp = BufReadUint16(&p_data);
                    desired_temp_recvd = TRUE;

                    //DEBUG_STR(" RECEIVED DESIRED TEMP : ");
                    //printInDecimal(current_desired_air_temp/32);
                    //DEBUG_STR(" kelvin\r\n");
                }
                else
                {
                   /* Not interested in other sensor types. Move the pointer 
                    * ahead with the expected length to decode the next sensor
                    * type value.
                    */
                    if(type[index] <= SENSOR_TYPE_SUPPORTED_MAX)
                    {
                        p_data += sensor_type_length[type[index]];
                    }
                    /* As the values have to be written in ascending order if 
                     * sensor type recvd is more than supported type, we need
                     * not decode the other values.
                     */
                    else
                    {
                        break;
                    }
                }
                index++;
            }

            /* Duplicate messages could be filtered here */
            if(current_desired_air_temp != last_bcast_desired_air_temp)
            {
                /* Set last Broadcast Temperature to current temperature. */
                last_bcast_desired_air_temp = current_desired_air_temp;
                //startTempTransmission();
            }

            if(event_code == CSR_MESH_SENSOR_WRITE_VALUE)
            {
                if(desired_temp_recvd == TRUE)
                {
                    /* assign the state data to acknowledge the write */
                    *state_data = (void *)&g_tsapp_data.sensor_data;
                }
                else
                {
                    /* we are not interested in the type values of the write msg
                     * hence just ignore the msg and dont send any response.
                     */
                    *state_data = NULL;
                }
            }
        }
        break;

        /* Received a raw message from lower-layers.
         * Notify to the control device if connected.
         */
        case CSR_MESH_RAW_MESSAGE:
        {
            if (g_tsapp_data.state == app_state_connected)
            {
                MeshControlNotifyResponse(g_tsapp_data.gatt_data.st_ucid,
                                          data, length);
            }
        }
        break;

        default:
        break;
    }

    /* Commit LastETag Change. */
    if (update_lastetag)
    {
        CsrMeshUpdateLastETag(&g_node_data.device_ETag);
        Nvm_Write(g_node_data.device_ETag.ETag, sizeof(CSR_MESH_ETAG_T),
                                                        NVM_OFFSET_DEVICE_ETAG);
    }
}
