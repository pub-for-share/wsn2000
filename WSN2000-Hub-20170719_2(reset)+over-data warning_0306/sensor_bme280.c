#include "sensor_bme280.h"
#include <spi.h>
#include <debug.h>
#include <types.h>
#include <mem.h>

unsigned short dig_T1,dig_P1;
static short dig_T2,dig_T3;//short
short dig_P2,dig_P3,dig_P4,dig_P5,dig_P6,dig_P7,dig_P8,dig_P9;//EEPROM的18个校正系数
uint8 dig_H1,dig_H3;
int8 dig_H6;
static short dig_H2,dig_H4,dig_H5;//dig_H4,dig_H5实则为uint12
long ut;
long up,uh;//unsigned 
//uint32 t_fine;

extern void printInDecimal(long val)
{   if(val<0)
    {   DebugWriteChar('-');
        val=-val;
    }
    if(val >= 10)
    {   printInDecimal(val/10);
    }
    DebugWriteChar(('0' + (val%10)));/*自身反复循环嵌套：内层为最高位，外层为最低位*/
}

extern void printUnDecimal(long val)
{   
    if(val >= 10)
    {   printInDecimal(val/10);
    }
    DebugWriteChar(('0' + (val%10)));/*自身反复循环嵌套：内层为最高位，外层为最低位*/
}

void BME280_Trigger(void)
{
    bool ret_status;
    ret_status=SpiInit(10,11,4,9);
    //MOSI MISO SLK CS#

    /*Weather monitoring*/
    SpiWriteRegister(REG_CONFIG &0x7f,0x00);//t_sb:0.5ms,IIR:off,SPI3W_en=0 75 00
    SpiWriteRegister(REG_CTRL_MEAS &0x7f,0x24);//osr_t:x1,osr_p:x1,         74 24
    SpiWriteRegister(REG_CTRL_HUM &0x7f,0x01); //osr_h:x1                   72 01
    //X8:92 04;X4:6E 03;X2:4A 02;X1:26 01
    /*20161107*/
    static uint8 regCtrlMeas=0;
    regCtrlMeas=SpiReadRegister(REG_CTRL_MEAS|0x80);//                      F4 
    SpiWriteRegister(REG_CTRL_MEAS &0x7f,regCtrlMeas|0x02);//mode:forced    74 26
}

void BME280_Init(void)
{   
    uint8 chip_id;
    uint8 coeff[33];
    bool ret_status;
    ret_status=SpiInit(10,11,4,9);//MOSI MISO SLK CS#    
    
    SpiWriteRegister(REG_RESET &0x7f,RESET);//write mode, set msb=0         60 B0
    /*Weather monitoring*/
    SpiWriteRegister(REG_CONFIG &0x7f,0x00);//t_sb:0.5ms,IIR:off,SPI3W_en=0 75 00
    SpiWriteRegister(REG_CTRL_MEAS &0x7f,0x24);//osr_t:x1,osr_p:x1          74 24
    SpiWriteRegister(REG_CTRL_HUM &0x7f,0x01); //osr_h:x1                   72 01
 
    chip_id=SpiReadRegister(REG_CHIPID);                                  //D0

    if(chip_id==CHIPID) 
    {           
        SpiReadRegisterBurst(REG_DIG_T1,coeff,24,FALSE);
        dig_T1=coeff[0]|(((uint16)coeff[1])<<8);    //Read sensor temperature coeff, LSBFIRST
        dig_T2=coeff[2]|(((uint16)coeff[3])<<8);
        dig_T3=coeff[4]|(((uint16)coeff[5])<<8);
        dig_P1=coeff[6]|(((uint16)coeff[7])<<8);    //Read sensor pressure coeff
        dig_P2=coeff[8]|(((uint16)coeff[9])<<8);
        dig_P3=coeff[10]|(((uint16)coeff[11])<<8);
        dig_P4=coeff[12]|(((uint16)coeff[13])<<8);
        dig_P5=coeff[14]|(((uint16)coeff[15])<<8);
        dig_P6=coeff[16]|(((uint16)coeff[17])<<8);
        dig_P7=coeff[18]|(((uint16)coeff[19])<<8);
        dig_P8=coeff[20]|(((uint16)coeff[21])<<8);
        dig_P9=coeff[22]|(((uint16)coeff[23])<<8);
        dig_H1=SpiReadRegister(REG_DIG_H1);         //Read sensor humi coeff
        dig_H3=SpiReadRegister(REG_DIG_H3);
        dig_H6=SpiReadRegister(REG_DIG_H6);
        
        SpiReadRegisterBurst(REG_DIG_H2,coeff+24,2,FALSE);//uint16, Read sensor humi coeff
        dig_H2=coeff[24]|(((uint16)coeff[25])<<8);

        coeff[26]=SpiReadRegister(REG_DIG_H4);            //uint12, Read sensor humi coeff
        SpiReadRegisterBurst(REG_DIG_H5,coeff+27,2,FALSE);
        dig_H4=(((uint16)coeff[26])<<4)|(coeff[27]&0x0f);//MSBFIRST
        dig_H5=(((uint16)coeff[28])<<4)|((coeff[27]>>4)&0x0f);//LSBFIRST
    }    
}

void BME280_Getdata(uint8 dat0[],uint16 len,bool getBySpiLibrary)
{   
    uint8 dat[8]={0,0,0,0,0,0,0,0};
    MemCopy(dat,dat0,len);   
    if(getBySpiLibrary==TRUE)
    {
        SpiReadRegisterBurst(REG_PRESS_MSB,dat,8,FALSE);//MSBFIRST
    }
    up=((((uint32)dat[0])<<12)|(((uint32)dat[1])<<4)|(dat[2]>>4));
    //up=((((uint32)dat[0])<<16)|(((uint32)dat[1])<<8)|dat[2])>>4;
    ut=((((uint32)dat[3])<<12)|(((uint32)dat[4])<<4)|(dat[5]>>4));
    //ut=(int32)(((((uint32)dat[3])<<16)|(((uint32)dat[4])<<8)|dat[5])>>4);
    uh=((((uint16)dat[6])<<8)|dat[7]);
}

// Returns temperature in DegC, resolution is 0.01 DegC. Output value of “5123” equals 51.23 DegC.
// t_fine carries fine temperature as global value
uint8 BME280_compensation_int32(uint32 *T,uint32 *p,uint32 *h)
{
	int32 var1, var2, v_x1_u32r, t_fine; // T,
    var1=0;var2=0;//uint32 h;
    
    /*long ut=0;
    long up=0,uh=0;*/
    //uint8 dat[8];
    //BME280_Getdata(dat,8,TRUE);
    
    var1 = ( ((ut>>3)-((int32)dig_T1<<1)) * ((int32)dig_T2) ) >>11;
    var2 = ( ((((ut>>4)-((int32)dig_T1)) * ((ut>>4)-((int32)dig_T1)))>>12) * \
           ((int32)dig_T3) )>>14;
	t_fine = var1 + var2;
	*T = (t_fine * 5 + 128) >> 8;
    
    // Returns pressure in Pa as unsigned 32 bit integer. Output value of “96386” equals 96386 Pa = 963.86 hPa
    var1 = (((int32)t_fine)>>1) - (int32)64000;
    var2 = (((var1>>2)*(var1>>2))>>11) * ((int32)dig_P6);
	var2 = var2 + ((var1*((int32)dig_P5))<<1);
	var2 = (var2>>2)+(((int32)dig_P4)<<16);
	var1 = (((dig_P3 * (((var1>>2) * (var1>>2)) >> 13 )) >> 3) + ((((int32)dig_P2) * var1)>>1))>>18;
	var1 =((((32768+var1))*((int32)dig_P1))>>15);
	if (var1 == 0)
	{
		return 0; // avoid exception caused by division by zero
	}
	*p = (((uint32)(((int32)1048576)-up)-(var2>>12)))*3125;
	if (*p < 0x80000000)
	{
		*p = (*p << 1) / ((uint32)var1);
	}
	else
	{
		*p = (*p / (uint32)var1) * 2;
	}
	var1 = (((int32)dig_P9) * ((int32)(((*p>>3) * (*p>>3))>>13)))>>12;
	var2 = (((int32)(*p>>2)) * ((int32)dig_P8))>>13;
	*p = (uint32)((int32)*p + ((var1 + var2 + dig_P7) >> 4));
    *p= (unsigned long)*p;
    
    // Returns humidity in %RH as unsigned 32 bit integer in Q22.10 format (22 integer and 10 fractional bits).
    // Output value of “47445” represents 47445/1024 = 46.333 %RH
    v_x1_u32r =t_fine-((int32)76800);
    v_x1_u32r = (( ((uh<<14)-(((int32)dig_H4)<<20)-(((int32)dig_H5)*v_x1_u32r))+\
                ((int32)16384) )>>15) * (((((((v_x1_u32r*((int32)dig_H6))>>10)*(((v_x1_u32r*((int32)dig_H3))>>11)+((int32)32768)))>>10)+\
                (int32)2097152)*((int32)dig_H2)+8192)>>14);
                
    v_x1_u32r = (v_x1_u32r-(((((v_x1_u32r>>15)*(v_x1_u32r>>15))>>7)*((int32)dig_H1))>>4));
	v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
	v_x1_u32r = (v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r);
    
    *h=(uint32)(v_x1_u32r>>12);

    return 1;
}


#if 0
uint32 BME280_compensation_T_int32()
{
    static int32 var1, var2;
    static int32 T;
    
    var1 = ( ((ut>>3)-((int32)dig_T1<<1)) * ((int32)dig_T2) ) >>11;
    var2 = ( ((((ut>>4)-((int32)dig_T1)) * ((ut>>4)-((int32)dig_T1)))>>12) * \
           ((int32)dig_T3) )>>14;
	t_fine = var1 + var2;
	T = (t_fine * 5 + 128) >> 8; 
    return T;
}

uint32 BME280_compensation_p_int32()
{
    int32 var1, var2;
    uint32 p;
    // Returns pressure in Pa as unsigned 32 bit integer. Output value of “96386” equals 96386 Pa = 963.86 hPa
    var1 = (((int32)t_fine)>>1) - (int32)64000;
    var2 = (((var1>>2)*(var1>>2))>>11) * ((int32)dig_P6);
	var2 = var2 + ((var1*((int32)dig_P5))<<1);
	var2 = (var2>>2)+(((int32)dig_P4)<<16);
	var1 = (((dig_P3 * (((var1>>2) * (var1>>2)) >> 13 )) >> 3) + ((((int32)dig_P2) * var1)>>1))>>18;
	var1 =((((32768+var1))*((int32)dig_P1))>>15);
	if (var1 == 0)
	{
		return 0; // avoid exception caused by division by zero
	}
	p = (((uint32)(((int32)1048576)-up)-(var2>>12)))*3125;
	if (p < 0x80000000)
	{
		p = (p << 1) / ((uint32)var1);
	}
	else
	{
		p = (p / (uint32)var1) * 2;
	}
	var1 = (((int32)dig_P9) * ((int32)(((p>>3) * (p>>3))>>13)))>>12;
	var2 = (((int32)(p>>2)) * ((int32)dig_P8))>>13;
	p = (uint32)((int32)p + ((var1 + var2 + dig_P7) >> 4));
    p= (unsigned long)p;
    return p;
}

uint32 BME280_compensation_h_int32()
{
    int32 v_x1_u32r;
    uint32 h;
    // Returns humidity in %RH as unsigned 32 bit integer in Q22.10 format (22 integer and 10 fractional bits).
    // Output value of “47445” represents 47445/1024 = 46.333 %RH
    v_x1_u32r =t_fine-((int32)76800);
    v_x1_u32r = (( ((uh<<14)-(((int32)dig_H4)<<20)-(((int32)dig_H5)*v_x1_u32r))+\
                ((int32)16384) )>>15) * (((((((v_x1_u32r*((int32)dig_H6))>>10)*(((v_x1_u32r*((int32)dig_H3))>>11)+((int32)32768)))>>10)+\
                (int32)2097152)*((int32)dig_H2)+8192)>>14);
                
    v_x1_u32r = (v_x1_u32r-(((((v_x1_u32r>>15)*(v_x1_u32r>>15))>>7)*((int32)dig_H1))>>4));
	v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
	v_x1_u32r = (v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r);
    
    h=(uint32)(v_x1_u32r>>12); 
    return h;
}
#endif