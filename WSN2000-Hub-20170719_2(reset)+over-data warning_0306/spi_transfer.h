#ifndef _SPI_TRANSFER_H_
#define _SPI_TRANSFER_H_
#include <pio.h>
#include <timer.h>

#include "spi_master.h"

uint16 rx_queue[50];
uint8 unpackedData[20];


extern uint16 checkCmdPending(void);
extern uint16 unpackPopData(uint16 *data,bool oddEven,uint16 len);

extern void startGetRTCTimeHandler(timer_id tid);
extern void packRTCTimeHandler(timer_id tid);

#endif