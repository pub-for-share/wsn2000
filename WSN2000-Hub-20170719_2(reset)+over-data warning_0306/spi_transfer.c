#include "spi_transfer.h"
#include "ble_test_service.h"
#include <mem.h>
#include <timer.h>
#include <sleep.h>

/* ========================= */
/* Application Configuration */

/* SPI mode setting. Must match the SPI mode used by the Slave */
#define SPI_MODE                        (1U)

/* Maximum amount of data transferred in a single rx/tx transaction
 * in words */
#define MAX_TRANSACTION_SIZE            (100U)

/* Fill octet to be used to fill the empty areas of the shared RAM tx buffer.
 * Useful for debugging purposes */
#define SHARED_RAM_BUFFER_FILL          (0xFF)

/* Configure the inter byte delay. For a value of 1 it introduces 2 cycles of
 * delay between bytes (which is 0.125uSec at 16MHz clock). */
#define INTER_BYTE_DELAY                (72U)  /* 9 uSec additional delay */

/* Configure the delay between transactions, i.e. the time from the SSEL
 * de-assertion marking the end of a transfer till the SSEL assertion marking
 * the start of another. Value is in milli seconds.
 *
 * Set to 0 to use the default minimum delay.
 */
#define INTER_TRANSACTION_DELAY         (0U)


/* Maximum count to which the matches can grow before rolling the counters
 * back to 0. The value should be chosen such that when multiplied by 100 the
 * resulting value should fit in 32-bits.
 */
#define MAX_MATCHES                     ( 0xFFFFFFFF / 100U )

/*=========================================================================*
 * Private Data Types
 *========================================================================*/

/* Type defining the state of the SPI transaction
 *
 * Structure of the transactions:
 * < SSEL assertion > |len|  <delay 60us> |data tx/rx| < SSEL de-assertion >
 */
typedef enum
{
    state_initial = 0x0001,                /* Initial state. No transactions
                                            * have taken place yet, and hence
                                            * the slave has not been sent any
                                            * data */
    state_no_transfer = 0x0002,            /* No tranfer is currently in
                                            * progress */
    state_waiting_first_transfer = 0x0100, /* Waiting for the first transfer to
                                            * complete */
    state_waiting = 0x0200                 /* Waiting for transfer to
                                            * complete */
} State;

/* SPI Master data structure */
typedef struct _SPI_DATA_T
{
    /* SPI Data transmit buffer */
    uint16 data_tx[MAX_TRANSACTION_SIZE];
    
    /* Queue backing the transmit buffer */
    OQ_HANDLE_T tx_q;
    
    /* SPI Data receive buffer */
    uint16 data_rx[MAX_TRANSACTION_SIZE];
    
    /* Queue backing the receive buffer */
    OQ_HANDLE_T rx_q;
    
    /* Current state of the transaction */
    State state;
} SPI_DATA_T;

/*============================================================================*
 *  Private Data
 *============================================================================*/

/* SPI Master data */
static SPI_DATA_T g_spi_data;

//#define timers_num  25 //TimerInit() parameter:timer used number
//static uint16 app_timers[SIZEOF_APP_TIMER *timers_num];

static uint16 spiCmd[50];
static uint16 spiCmdLen[3]={0U,0U,0U};

/*20161208*/
static uint16 eofSpiCmdPtr=0; 

/* Buffer for received data */
static uint16 g_pop_data[sizeof(spiCmd) / sizeof(uint16)];

/* Various lengths of data to be transferred */
//static uint16 g_data_lens[] = {4U, 10U, 8U, 20U, 28U, 36U, 40U, 100U, 35U, 60U,
//                               80U, 120U, 148U, 198U, 198U};

/* Index pointing to the current transfer's length */
//static uint8 g_data_len_idx = 0U;

/*20161213*/
static bool spiInProcess=FALSE;

/*20161214*/
timer_id resetSpiInProcessTid=TIMER_INVALID;


/*============================================================================*
 *  Private Function Prototypes
 *============================================================================*/


/* Get index into the next element in the array of lengths */
//static uint8 getNextDataLenIdx(void);

/* Get index into the previous element in the array of lengths */
//static uint8 getPrevDataLenIdx(void);

#if (INTER_TRANSACTION_DELAY != 0)
    /* Timer call-back, that kick starts a transfer over SPI */
    static void kickOffNextTransfer(timer_id timer);
#else
    /* Delay loop, provides approx 3.8uSec per a count of 10 */
    //static void delay(uint16 nopCount);
#endif
    

/* Data status callback issued by the SPI master library when buffered
 * octets have been sent */
static void spiMasterDataCallback(OQ_HANDLE p_rx_q, OQ_HANDLE p_tx_q);

/*20161213*/
void timerHandlerResetspiInProcessFlag(timer_id tid);

/*============================================================================*
 *  Private Function Implementations
 *============================================================================*/
void cmdArrayCat(uint16 *arr,uint16 len)   //updata spiCmdLen & spiCmd
{
    //uint16 *temp;
    //temp=spiCmd+eofSpiCmdPtr;
    uint16 i;
    for(i=0;i<len;i++)
        spiCmd[eofSpiCmdPtr+i]=arr[i];
    //MemCopy(spiCmd+eofSpiCmdPtr,arr,len);
    eofSpiCmdPtr= eofSpiCmdPtr+len;
 
    if(spiCmdLen[0]==0)
        spiCmdLen[0]=len;
    else if(spiCmdLen[1]==0)
        spiCmdLen[1]=len; 
    else if(spiCmdLen[2]==0)
        spiCmdLen[2]=len;
    /**/    
}

extern uint16 checkCmdPending()
{   
    uint16 curSpiCmdStartPtr=spiCmdLen[0];//point to eof cmd set 0(start of cmd set 1）
    uint16 remainTotalLength=0;      
    uint16 i;
    
    MemSet(spiCmd,0x00,spiCmdLen[0]);
    spiCmdLen[0]=0;

    if(spiCmdLen[1]!=0)
    {
        remainTotalLength=spiCmdLen[1]+spiCmdLen[2];
        for(i=0;i<remainTotalLength;i++)
        {
            spiCmd[i]=spiCmd[curSpiCmdStartPtr+i];
        }
            
        spiCmdLen[0]=spiCmdLen[1];
        spiCmdLen[1]=spiCmdLen[2];
        spiCmdLen[2]=0;        
        return spiCmdLen[0];
    }
    else
        return 0;
}

uint16 checkCmdLength()
{
    uint16 totalLength=0;
    totalLength=spiCmdLen[0];
    return totalLength;
}
#if 0
/*----------------------------------------------------------------------------*
 *  NAME
 *      getNextDataLenIdx
 *
 *  DESCRIPTION
 *      Get index into the next element in the array of lengths
 *
 *  PARAMETERS
 *      None
 *
 *  RETURNS
 *      Index of the next element in the array of lengths
 *----------------------------------------------------------------------------*/
static uint8 getNextDataLenIdx(void)
{
    /* Next index into length array */
    uint8 next_len_idx = g_data_len_idx + 1U;
    /* Number of values in the length array */
    const uint8 lens_size = sizeof(g_data_lens) / sizeof(uint16);
    
    if (next_len_idx >= lens_size)
        /* If going beyond the array boundary */
    {
        /* reset back to start of the array */
        next_len_idx = 0U;
    }
    
    return next_len_idx;
} /* getNextDataLenIdx */

/*----------------------------------------------------------------------------*
 *  NAME
 *      getPrevDataLenIdx
 *
 *  DESCRIPTION
 *      Get index into the previous element in the array of lengths
 *
 *  PARAMETERS
 *      None
 *
 *  RETURNS
 *      Index of the previous element in the array of lengths
 *----------------------------------------------------------------------------*/
static uint8 getPrevDataLenIdx( void )
{
    /* Number of values in the length array */
    const uint8 lens_size = sizeof(g_data_lens) / sizeof(uint16);

    /* Previous element index is current index -1 unless current index is 0,
     * in which case the previous element is the last element */
    return g_data_len_idx ? (g_data_len_idx - 1U) : (lens_size - 1U);
} /* getPrevDataLenIdx */
#endif

#if (INTER_TRANSACTION_DELAY != 0)
/*----------------------------------------------------------------------------*
 *  NAME
 *      kickOffNextTransfer
 *
 *  DESCRIPTION
 *      Timer call-back, that kick starts a transfer over SPI
 *
 *  PARAMETERS
 *      timer [in]              Timer ID
 *
 *  RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/
static void kickOffNextTransfer(timer_id timer)
{
    /* Start the transaction and transfer just the length */
    if (!SpiMasterStartTransaction())
    {
        /* If failed to start the transaction, try again later */
        TimerCreate(INTER_TRANSACTION_DELAY * MILLISECOND, TRUE,
                    kickOffNextTransfer );
    }
    else
    {
        /* Update the transfer state. If this is the first transfer, indicate
         * that we're waiting for the first transfer to complete. */
        if (g_spi_data.state == state_initial)
        {
            g_spi_data.state = state_waiting_first_transfer;
        }
        else
        {
            g_spi_data.state = state_waiting;
        }
    }
} /* kickOffNextTransfer */
#else
#if 0
/*----------------------------------------------------------------------------*
 *  NAME
 *      delay
 *
 *  DESCRIPTION
 *      Delay loop, provides approx 3.8us per a count of 10
 *
 *  PARAMETERS
 *      nopCount [in]           Number of NOP instructions to inject
 *
 *  RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/
static void delay(uint16 nopCount)
{
    while (nopCount--)
    {
        __asm("nop");
    }
} /* delay */
#endif
#endif

/*----------------------------------------------------------------------------*
 *  NAME
 *      spiMasterDataCallback
 *
 *  DESCRIPTION
 *      Data status callback issued by the SPI master implementation when
 *      previously buffer octets were sent
 *
 *      It will also be issued to notify the application that certain amount
 *      of data has been transferred over to the PIO controller tx area of
 *      the application shared memory for onward transmission to the SPI
 *      master. This way application can keep track of the size of the
 *      tx queue and keep filling more data as necessary.
 *
 *  PARAMETERS
 *      p_rx_q [in]             Rx queue handle
 *      p_tx_q [in]             Tx queue handle
 *
 *  RETURNS
 *      Nothing
 *----------------------------------------------------------------------------*/
static void spiMasterDataCallback(OQ_HANDLE p_rx_q, OQ_HANDLE p_tx_q)
{
    static uint16 cmdPendingLen=0;
    SpiMasterEndTransaction();  
    SpiMasterDeinit();//20161206
    
    if(0)OQPopData(p_rx_q, g_pop_data,(sizeof(g_pop_data)/sizeof(uint16))*2U);
    OQPopData(p_rx_q, rx_queue,(sizeof(g_pop_data)/sizeof(uint16))*2U);
    OQClear(p_rx_q);
    cmdPendingLen=checkCmdPending();
    if(cmdPendingLen!=0)
    {
        spiTransfer(deviceType,PIO_CTRLR_CODE_ADDR0,cmdPendingLen*2);//only next to be processed
        spiInProcess=TRUE;
    }
    else
        spiInProcess=FALSE;//finish transfer task
    return;
} /* spiMasterDataCallback */
#if 0
void delay50ms(uint16 n)
{
    while(n--) TimeDelayUSec(50000);
}
#endif

void clearSpiCmdAndLen()
{        
    MemSet(spiCmd,0x00,sizeof(spiCmd)/sizeof(uint16));
    MemSet(spiCmdLen,0x00,sizeof(spiCmdLen)/sizeof(uint16));
    eofSpiCmdPtr=0;
}
extern uint16 unpackPopData(uint16 *data,bool oddEven,uint16 len)
{
    uint16 i=0;
    
    unpackedData[0]=((*data)>>8)&0xff;//discard 0xff in the beginning
    data++;

    for(i=1;i<2*(len-1);)
    {       
        unpackedData[i]=(*data)&0xff;//little end        
        unpackedData[i+1]=((*data)>>8)&0xff;
        i+=2;
        data++;
    } 
    if(oddEven==TRUE) unpackedData[2*(len-1)]=0;//丢掉最后多读的一位octet
    return 1;
}

void spiTransfer(uint16 dev_type,uint16 *p_pio_controller_code_addr,uint16 length)
{
    deviceType=dev_type;
    spiInProcess=TRUE;//20161214
     /* Set up the application buffer where the data to be transmitted will be
     * queued. This data will be picked up by the SPI Master library and
     * transferred over to the Tx area of the shared RAM when it receives an
     * interrupt from the PIO controller. The application can queue data at any
     * time.
     */
    OQSetFill(&(g_spi_data.tx_q), FALSE, 0U);
    OQCreate(g_spi_data.data_tx, MAX_TRANSACTION_SIZE, OQDataMode_packed,
              &(g_spi_data.tx_q));//OQDataMode_packed,

    /* Set up the application buffer where the received data will be queued
     * by the SPI slave implementation. The data will be filled by the SPI
     * slave when it receives an interrupt from the PIO controller. Application
     * can read the data any time, but in order to eliminate the possibility
     * of the buffer overrun and possibly corrupting the incoming data, this
     * buffer needs to be emptied as soon as the data status callback is
     * received
     */
    OQSetFill(&(g_spi_data.rx_q), FALSE, 0U);
    OQCreate(g_spi_data.data_rx, MAX_TRANSACTION_SIZE, OQDataMode_packed,
              &(g_spi_data.rx_q));//OQDataMode_packed,

    /* Only enter shallow sleep mode, because the PIO controller needs to run at
     * 16MHz in order to operate the SPI bus at ~1MHz */
    SleepModeChange(sleep_mode_shallow);

    /* Initialise the SPI master */
    SpiMasterInit(p_pio_controller_code_addr, spiMasterDataCallback,
                  &(g_spi_data.tx_q), &(g_spi_data.rx_q),
                  SHARED_RAM_BUFFER_FILL, INTER_BYTE_DELAY);//X

    /* Setup the pull-ups for MISO pin */
    PioSetPullModes(PIO_BIT_MASK(SPI_MASTER_PIO_MISO), pio_mode_weak_pull_up);

    /* Start the SPI master */
    SpiMasterStart();

    /* Followed by the data itself */
    OQQueueData(&(g_spi_data.tx_q), spiCmd, length);
    /*if(MemCmp(transferArray,readCoeffs1,length)==0) getCoeffs1=TRUE;
    if(MemCmp(transferArray,readCoeffs2,length)==0) getCoeffs2=TRUE;
    if(MemCmp(transferArray,readCoeffs3,length)==0) getCoeffs3=TRUE;
    if(MemCmp(transferArray,readSensorData,length)==0) getSensorData=TRUE;*/

#if (INTER_TRANSACTION_DELAY == 0)
    /* Start the SPI transaction without delay */
    SpiMasterStartTransaction();

    /* Update state to show first transfer is underway */
    g_spi_data.state = state_waiting_first_transfer;
#else
    /* Initial state. No transfers have been performed yet. */
    g_spi_data.state = state_initial;

    /* Initialise the timer to be used to kick off transfers */
    TimerInit(1U, app_timers);
    //TimerInit(timers_num, (void *)app_timers);
    
        //TimerCreate(INTER_TRANSACTION_DELAY * MILLISECOND, TRUE,
          //      kickOffNextTransfer);
#endif
    //TimerInit(timers_num, (void *)app_timers);
    //while(spiInProcess==TRUE)
      //TimeDelayUSec(800);
    
}
extern void startGetRTCTimeHandler(timer_id tid)   //read
{
    TimerDelete(tid);tid=TIMER_INVALID;
    /*20170210*/
    uint16 cmdLength=0;
    uint16 readRTCSetting[9]=
    {
        PACK(0X90,0XFF),PACK(0XFF,0XFF),PACK(0XFF,0XFF),PACK(0XFF,0XFF),PACK(0XFF,0XFF),
        PACK(0XFF,0XFF),PACK(0XFF,0XFF),PACK(0XFF,0XFF),0XFF,
    };
    clearSpiCmdAndLen();
    cmdArrayCat(readRTCSetting,sizeof(readRTCSetting)/sizeof(uint16));//9    
    cmdLength=checkCmdLength();
    deviceType=0;//0:PCF2123/1:BME280
    spiTransfer(deviceType,PIO_CTRLR_CODE_ADDR0,2*cmdLength);/**/
    TimerCreate(200*MILLISECOND,TRUE,packRTCTimeHandler);
}
extern void packRTCTimeHandler(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
    bleTestServData.currentTime=(((uint16)rx_queue[4])<<8) | rx_queue[3];/**/
            streamFinishTransfer|=0x10;
    /*bool oddOrEven=TRUE;
    unpackPopData(rx_queue,oddOrEven,16);
    uint16 timePacked[8],alarmReadTime;//,verifyNvm[2];
    timePacked[0]=(((uint16)unpackedData[1])<<8) | unpackedData[0];
    timePacked[1]=(((uint16)unpackedData[3])<<8) | unpackedData[2];
    timePacked[2]=(((uint16)unpackedData[5])<<8) | unpackedData[4];
    timePacked[3]=(((uint16)unpackedData[7])<<8) | unpackedData[6];
    timePacked[4]=(((uint16)unpackedData[9])<<8) | unpackedData[8];
    timePacked[5]=(((uint16)unpackedData[11])<<8) | unpackedData[10];
    timePacked[6]=(((uint16)unpackedData[13])<<8) | unpackedData[12];
    timePacked[7]=(((uint16)unpackedData[15])<<8) | unpackedData[14];
    alarmReadTime=(((uint16)unpackedData[3])<<8) | unpackedData[2];*/
    //return alarmReadTime;
}