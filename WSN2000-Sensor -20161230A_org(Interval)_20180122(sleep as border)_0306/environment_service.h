#ifndef _ENVIRONMENT_SERVICE_H_
#define _ENVIRONMENT_SERVICE_H_

#include <bt_event_types.h>
#include "app_gatt.h"   //some enum
//#include "app_data_stream.h"
#include "spi_transfer.h"
//#include "csr_mesh_tempsensor.h"

#define CONNECTION_CID  g_tsapp_data.gatt_data.st_ucid
#define REQUEST_DATA                0X12
#define RECEIVE_DATA1               0X13
#define RECEIVE_DATA2               0X14
#define RECEIVE_DATA3               0X15
#define RECEIVE_DATA4               0X16

#define CUSTOM_RTCTIME_CORRECTION   0X20

#define SENDCMD_INTERVAL    200*MILLISECOND

typedef struct{
    uint16 deviceID;//[0,255]
    /*uint16 temp;
    uint16 humi;
    uint32 pressure;
    uint16 currentVal;*/
    uint16 currentTime;
    //uint8 batLevel;
    //gatt_client_config batLevelCfg;
    gatt_client_config dataReadyCfg;
}bleTestServData_t;
bleTestServData_t bleTestServData;

//uint16 sendCmdCount;
uint8 streamFinishTransfer;
timer_id notifyTestDat_tid;
timer_id waitSensorDatarRead_tid;

//uint8 whichProcess;
//uint8 sensorDataReady;
uint8 sensorDataComplete;
uint16 targetDevID;
bool isRemote;
//uint16 timeTrace;

uint16 getBridgeFlag(void);
//void recvdCmdCbTimerHandler(timer_id tid);
void waitSensorDataReadTimerHandler(timer_id tid);

extern void bleTestServiceInit(void);
extern void bleTestHandleAccessRead(GATT_ACCESS_IND_T *p_ind);
extern void bleTestHandleAccessWrite(GATT_ACCESS_IND_T *p_ind);
extern bool bleTestCheckHandleRange(uint16 handle);

#endif