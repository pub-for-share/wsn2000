/******************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2015
 *  CSR Bluetooth Low Energy CSRmesh 1.3 Release
 *  Application version 1.3
 *
 *  FILE
 *    app_data_stream.c
 *
 * DESCRIPTION
 *    This file implements a simple protocol over the data model to exchange
 *    device information.
 *    The protocol:
 *       | CODE | LEN (1 or 2 Octets| Data (LEN Octets)|
 *       CODE Defined by APP_DATA_STREAM_CODE_T
 *       LEN - if MS Bit of First octet is 1, then len is 2 octets
 *       if(data[0] & 0x80) LEN = data[0]
 *
 ******************************************************************************/

/*=============================================================================*
 *  SDK Header Files
 *============================================================================*/
#include <gatt.h>
#include <timer.h>
#include <mem.h>
#include <debug.h>
#include <random.h>     //20160815
#include <sleep.h>      //20160816
#include <reset.h>      //20170307
/*============================================================================*
 *  CSR Mesh Header Files
 *============================================================================*/
#include <csr_mesh.h>
#include <stream_model.h>

/*=============================================================================*
 *  Local Header Files
*============================================================================*/
#include "app_data_stream.h"
#include "csr_mesh_tempsensor.h"
#ifdef  ENABLE_DATA_MODEL
/*=============================================================================*
 *  Private Definitions
 *============================================================================*/

#define DEVICE_INFO_STRING      "CSRmesh Temp Sensor application\r\n" \
                                "Supported Models:\r\n" \
                                "  Sensor Model\r\n" \
                                "  Bearer Model\r\n" \
                                "  Attention Model\r\n" \
                                "  Data Model"

/* Data stream send retry wait time */
//#define STREAM_SEND_TIME_INTERVAL         (500 * MILLISECOND)
#define STREAM_SEND_TIME_INTERVAL         0                               

/* Data stream received timeout value */
#define RX_STREAM_TIMEOUT                 (5 * SECOND)

/* Max number of retries */
#define MAX_SEND_RETRIES                  (30)

/*20170221*/
bool onlySetRTCOneTimeFlag=FALSE;     
bool onlyCorrectTimeOneTimeFlag=FALSE;                               
/*=============================================================================*
 *  Private Data
 *============================================================================*/
/* String to give a brief description of the application */
static uint8 device_info[256];

/* Device info length */
static uint8 device_info_length;

/* Stream bytes sent tracker */
static uint16 tx_stream_offset = 0;
static uint16 stream_send_retry_count = 0;

/* Stream send retry timer */
static timer_id stream_send_retry_tid = TIMER_INVALID;

/* Stream send retry counter */
static uint16 streamSendRetryCount = 0;
static uint16 streamSendRetryCountCheck = 0;

/* Current Rx Stream offset */
static uint16 rx_stream_offset = 0;

/* Rx Stream status flag */
static bool rx_stream_in_progress = FALSE;

/* Rx stream timeout tid */
static timer_id rx_stream_timeout_tid;

static APP_DATA_STREAM_CODE_T current_stream_code;

uint8 dat[8];/*new code20160603*/
uint16 targetDevid;
uint16 stream_len=0;

uint16 deviceid_local;
uint16 devid_target;
uint16 timecounter=0;
/*=============================================================================*
 *  Private Function Prototypes
 *============================================================================*/
static void streamSendRetryTimer(timer_id tid);
void streamSendRetryTimer1(timer_id tid);
void streamSendRetryTimerHandler(timer_id tid); //20160603
void forceIntoSleepTimerHandler(timer_id tid);//20170111
/*20160819*/
void debounceTimerHandlerCUSTOMREQDATA(timer_id tid);
/*20170210*/
void startGetRTCTimerHandler(timer_id tid);
void handleReturnToMeshOnState(timer_id tid);
void correctRTCTimeDebounceHandler(timer_id tid);
void fromBridgeHandler(timer_id tid);
void sendHumiDataHandler(timer_id id);
void sendPressureDataHandler(timer_id id);
/*=============================================================================*
 *  Private Function Implementations
 *============================================================================*/
uint8 bcd2int(uint8 bcddata)
{
    return (bcddata>>4)*10+(bcddata&0x0f);
}

void startGetRTCTimerHandler(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
    /*20170210*/
    uint16 cmdLength=0;
    uint16 readRTCSetting[]=
    {
        PACK(0X90,0XFF),PACK(0XFF,0XFF),PACK(0XFF,0XFF),PACK(0XFF,0XFF),PACK(0XFF,0XFF),
        PACK(0XFF,0XFF),PACK(0XFF,0XFF),PACK(0XFF,0XFF),0XFF,
    };
    clearSpiCmdAndLen();
    cmdArrayCat(readRTCSetting,sizeof(readRTCSetting)/sizeof(uint16));//9    
    cmdLength=checkCmdLength();
    deviceType=0;//0:PCF2123/1:BME280
    spiTransfer(deviceType,PIO_CTRLR_CODE_ADDR0,2*cmdLength);/**/
    TimerCreate(200*MILLISECOND,TRUE,storeRTCTimerHandler);   
}
void storeRTCTimerHandler(timer_id tid)
{
    /*20170210*/
    bool oddOrEven=TRUE;
    unpackPopData(rx_queue,oddOrEven,16);    
    uint16 timePacked[8];//,verifyNvm[2];
    timePacked[0]=(((uint16)unpackedData[1])<<8) | unpackedData[0];
    timePacked[1]=(((uint16)unpackedData[3])<<8) | unpackedData[2];
    timePacked[2]=(((uint16)unpackedData[5])<<8) | unpackedData[4];
    timePacked[3]=(((uint16)unpackedData[7])<<8) | unpackedData[6];
    timePacked[4]=(((uint16)unpackedData[9])<<8) | unpackedData[8];
    timePacked[5]=(((uint16)unpackedData[11])<<8) | unpackedData[10];
    timePacked[6]=(((uint16)unpackedData[13])<<8) | unpackedData[12];
    timePacked[7]=(((uint16)unpackedData[15])<<8) | unpackedData[14];
    
    /*20170307*/
    /*uint16 countdownPeriod=0;
    switch(multiTimesOneHour)
    {
        case 1:countdownPeriod=60;break;
        case 2:countdownPeriod=30;break;
        case 3:countdownPeriod=20;break;
        case 4:countdownPeriod=15;break;
    }*/
    if( !( (timePacked[0]==PACK(0X00,0X12)) && (timePacked[5]==PACK(0X80,0X80))&&\
        (timePacked[7]==PACK(0x03,0x00)) ) )
    {
        current_sensor_status=sensor_status_init;
        meshStatus=OFF;//ON:TRUE/OFF:FALSE
        saveSensorStatusToNVM((uint16)current_sensor_status);
        storeMeshStatusToNvm(&meshStatus,1);
        WarmReset();
    }
        
    storeRTCTimeToNVM(timePacked,8);//f87a   
}

void correctRTCTimeDebounceHandler(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
    onlyCorrectTimeOneTimeFlag=FALSE;
}

void forceIntoSleepTimerHandler(timer_id tid)   //current=5.1uA~7.7uA
{
    /*TimerDelete(tid);
    tid=TIMER_INVALID;
    PioSetModes(0x0e10,pio_mode_user);//0x1110 0001 0000
    PioSetDirs(0x0e10,0x0e10);
    PioSetPullModes(0x0e10,pio_mode_strong_pull_up);    
    CsrMeshEnableListening(FALSE);//20160816    
    SleepModeChange(sleep_mode_deep);*/
    TimerDelete(tid);tid=TIMER_INVALID;  
    PioSet(LED_PIO_IND,LED_OFF);

    CsrMeshEnableListening(FALSE);    
    CSR_MESH_ADVSCAN_PARAM_T param;//20160831
    CsrMeshGetAdvScanParam(&param);
    param.scan_duty_cycle = OFF_RX_DUTY_CYCLE;
    CsrMeshSetAdvScanParam(&param);
    
    PioSetModes( (1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|
               (1L<<SPI_MASTER_PIO_MISO),pio_mode_user );
    PioSetDirs( (1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|(1L<<SPI_MASTER_PIO_MISO),
               (1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|(1L<<SPI_MASTER_PIO_MISO) );
    PioSetPullModes((1L<<SPI_MASTER_PIO_SCLK)|(1L<<SPI_MASTER_PIO_MOSI)|(1L<<SPI_MASTER_PIO_MISO),
                    pio_mode_strong_pull_up);      
    SleepModeChange(sleep_mode_deep);    
   
    /*02170216*/
    meshStatus=FALSE;//ON:TRUE/OFF:FALSE
    storeMeshStatusToNvm(&meshStatus,1);
  
    PioSetEventMask(1L<<RTC_INT,pio_event_mode_both);//20170302
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      streamSendRetryTimer
 *
 *  DESCRIPTION
 *      Timer handler to retry sending next packet
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void streamSendRetryTimer(timer_id tid)
{
    if( tid == stream_send_retry_tid )
    {
        stream_send_retry_tid = TIMER_INVALID;
        streamSendRetryCount++;
        if( streamSendRetryCount < MAX_SEND_RETRIES )
        {
            StreamResendLastData();
            stream_send_retry_tid = TimerCreate(STREAM_SEND_TIME_INTERVAL, 
                                                TRUE, streamSendRetryTimer);
        }
        else
        {
            streamSendRetryCount = 0;
            StreamFlush();
            /* Set the mesh scan back to low duty cycle if the device is already 
             * configured.
             */
            EnableHighDutyScanMode(FALSE);
        }
    }
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      rxStreamTimeoutHandler
 *
 *  DESCRIPTION
 *      Timer handler to handle rx stream timeout
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void rxStreamTimeoutHandler(timer_id tid)
{
    if( tid == rx_stream_timeout_tid )
    {
        /* Reset the stream */
        rx_stream_timeout_tid = TIMER_INVALID;
        rx_stream_in_progress = FALSE;
        StreamReset();
        /* Set the mesh scan back to low duty cycle if the device is already 
         * configured.
         */
        EnableHighDutyScanMode(FALSE);
    }
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      sendNextPacket
 *
 *  DESCRIPTION
 *      Forms a stream data packet with the current counter and sends it to
 *      the stream receiver
 *
 *  RETURNS/MODIFIES
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
static void sendNextPacket(void)
{
    uint16 data_pending, len;

    /* Stop retry timer */
    streamSendRetryCount = 0;
    TimerDelete(stream_send_retry_tid);
    stream_send_retry_tid = TIMER_INVALID;

    data_pending = device_info_length+2 - tx_stream_offset;

    if( data_pending )
    {
        len = (data_pending > STREAM_DATA_BLOCK_SIZE_MAX)? 
                                    STREAM_DATA_BLOCK_SIZE_MAX:data_pending;

        /* Send the next packet */
        StreamSendData(&device_info[tx_stream_offset], len);
        tx_stream_offset += len;

        stream_send_retry_tid = TimerCreate(100 * MILLISECOND, TRUE,
                                                       streamSendRetryTimer);
    }
    else
    {

        /* Send flush to indicate end of stream */
        StreamFlush();
        /* Set the mesh scan back to low duty cycle if the device is already 
         * configured.
         */
        EnableHighDutyScanMode(FALSE);
    }
}

void sendDataStreamWithCFM(uint16 dat_len,uint16 devid_dest, uint16 counter)
{   
    targetDevid = devid_dest;
    stream_len  = dat_len;
    streamSendRetryCount = counter;
    streamSendRetryCountCheck=0;
    streamCFM=FALSE;
    
    /* Stop retry timer */
    TimerDelete(stream_send_retry_tid); //reset timer
    stream_send_retry_tid = TIMER_INVALID; //reset timer
    
    /*20060803*/
    TimerDelete(ledFlashtid);//idle state indication
    ledFlashtid=TIMER_INVALID;
    PioSet(LED_PIO_IND,0);//user mode:OFF
    PioSetMode(LED_PIO_IND,pio_mode_pwm1);//fast bilnk ind 
    PioConfigPWM(1,pio_pwm_mode_push_pull, 1, 200, 1,
                                           200, 1, 1, 5);
    PioEnablePWM(1,TRUE); 
    
    StreamStartSender(targetDevid); //set target device id
    StreamFlush();
    StreamSendData(dat,stream_len); //function to send the data
        
    stream_send_retry_tid = TimerCreate(100 * MILLISECOND, TRUE,
                                        streamSendRetryTimerHandler);//retry after 100ms call streamSendRetryTimerHandler
}

void streamSendRetryTimerHandler(timer_id tid)//20160520 //send retry timer handler
{
    stream_send_retry_tid = TIMER_INVALID; //reset timer
    static uint32 random32;//20160815
    streamSendRetryCountCheck++; //inc retry counter
    
    if(streamSendRetryCountCheck>streamSendRetryCount)//directly send data without cfm
    {
        if(current_sensor_status==sensor_status_register)
        {               
            streamSendRetryCount = 0; //reset send retry counter
            streamSendRetryCountCheck = 0;
            TimerDelete(stream_send_retry_tid);
            stream_send_retry_tid=TIMER_INVALID;
            streamCFM=FALSE;//20160816
   
            current_sensor_status=sensor_status_normal;//change status and start normal status timer
            saveSensorStatusToNVM((uint16)current_sensor_status);
            PioEnablePWM(1,FALSE);//LED OFF ind
            //PioSetMode(LED_PIO_IND,pio_mode_user);//20170109
            //PioSet(LED_PIO_IND,LED_OFF);
            TimerCreate(0*MILLISECOND,TRUE,forceIntoSleepTimerHandler);//20170216                 
        }        
    }
    else  
        if(streamCFM==FALSE)
    {
        StreamStartSender(targetDevid);
        StreamSendData(dat,stream_len);
        //StreamFlush();    //20170209    
        random32=(Random32()%20)*400;//(0:400:8000]
        stream_send_retry_tid =  
                TimerCreate(( STREAM_SEND_TIME_INTERVAL+random32 )*MILLISECOND, 
                            TRUE,streamSendRetryTimerHandler);
    }
}

void sendCustomCmd(uint16 retryCounter,uint8 customData[], \
                   uint16 dataLength,uint16 customCode, uint16 targDevid)   //uint16 intervalms,
{   
    EnableHighDutyScanMode(TRUE);//20160819
    streamSendRetryCount=retryCounter;//20170306:原先屏蔽的两句打开
    //sendCustomCmdIntervalms=intervalms;
    devid_target=targDevid;
    stream_len=dataLength;//20170303
    
    dat[0]=customCode; //send cmd
    uint16 i;
    for(i=0;i<dataLength;i++)        
        dat[i+1]=customData[i];
   
    /* Stop retry timer */
    stream_send_retry_count = 0;
    stream_send_retry_tid = TIMER_INVALID;
    //StreamStartSender(devid_target);StreamSendData(dat,2);
    StreamSendDataBlock(devid_target,dat,stream_len+1);//20160824

    streamCFM=FALSE;
    
    stream_send_retry_tid = 
            TimerCreate(200 * MILLISECOND,
                        TRUE, streamSendRetryTimer1);
}

/*20160520*/
void streamSendRetryTimer1(timer_id tid)
{     
    TimerDelete(stream_send_retry_tid);
    stream_send_retry_tid = TIMER_INVALID;
    
    stream_send_retry_count++;

    if( stream_send_retry_count<streamSendRetryCount )    //(streamCFM==TRUE)
    {            
        //StreamResendLastData();
        //StreamStartSender(0x0000);//broadcast
        //StreamSendData(dat,2);
        StreamSendDataBlock(devid_target,dat,stream_len+1);//20170210
        timecounter++;

        stream_send_retry_tid = TimerCreate(200*MILLISECOND, 
                                            TRUE,streamSendRetryTimer1);
    }
    else
    {
        stream_send_retry_count = 0;
        timecounter=0;//20160808
        /*if(whichStep==3) whichStep=0;
        whichStep++;
        switch(whichStep)
        {
            case 1:
            {
                TimerCreate(200*MILLISECOND,TRUE,sendHumiDataHandler);
            }
            break;
            case 2:
            {
                TimerCreate(200*MILLISECOND,TRUE,sendPressureDataHandler);
            }
            break;
            default:break;
        }*/    
        //StreamFlush();
    }
}
void sendHumiDataHandler(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
        uint8 measureSensorData1[3];    
                measureSensorData1[0]=0xbb;
                measureSensorData1[1]= current_air_humi&0xff;//little-endian
                measureSensorData1[2]=(current_air_humi>>8)&0xff;    
                sendCustomCmd(3,measureSensorData1,3,RECEIVE_DATA1,devid_bridge);//5*200ms
}
void sendPressureDataHandler(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
        uint8 measureSensorData1[4];    
                measureSensorData1[0]=0xcc;
                measureSensorData1[1]= current_air_pressure&0xff;
                measureSensorData1[2]=(current_air_pressure>>8)&0xff;
                measureSensorData1[3]=(current_air_pressure>>16)&0xff;//uint32->uint24
                sendCustomCmd(3,measureSensorData1,4,RECEIVE_DATA1,devid_bridge);//5*200ms
}
/*=============================================================================*
 *  Public Function Implementations
 *============================================================================*/

/*-----------------------------------------------------------------------------*
 *  NAME
 *      AppDataStreamInit
 *
 *  DESCRIPTION
 *      This function initializes the stream Model.
 *
 *  RETURNS
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
extern void AppDataStreamInit(uint16 *group_id_list, uint16 num_groups)
{
    /* Model initialisation */
    StreamModelInit(group_id_list, num_groups);

    /* Reset timers */
    stream_send_retry_tid = TIMER_INVALID;
    rx_stream_timeout_tid = TIMER_INVALID;

    /* Reset the device info */
    device_info_length = sizeof(DEVICE_INFO_STRING);
    device_info[0] = CSR_DEVICE_INFO_RSP;
    device_info[1] = device_info_length;

    MemCopy(&device_info[2], DEVICE_INFO_STRING, sizeof(DEVICE_INFO_STRING));
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      handleCSRmeshDataStreamFlushInd
 *
 *  DESCRIPTION
 *      This function handles the CSR_MESH_DATA_STREAM_FLUSH message.
 *
 *  RETURNS
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
extern void handleCSRmeshDataStreamFlushInd(CSR_MESH_STREAM_EVENT_T *p_event)
{   
    rx_stream_offset = 0;

    if( rx_stream_in_progress == FALSE )
    {
        /* Change the Rx scan duty cycle to active at the start of data stream */
        EnableHighDutyScanMode(TRUE);
        /* Start the stream timeout timer */
        TimerDelete(rx_stream_timeout_tid);
        rx_stream_timeout_tid = TimerCreate(RX_STREAM_TIMEOUT, TRUE,
                                                        rxStreamTimeoutHandler);
    }
    else
    {
        /* End of stream */
        rx_stream_in_progress = FALSE;
        TimerDelete(rx_stream_timeout_tid);
        rx_stream_timeout_tid = TIMER_INVALID;
        /* Set the mesh scan back to low duty cycle if the device is already 
         * configured.
         */
        EnableHighDutyScanMode(FALSE);
    }
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      handleCSRmeshDataBlockInd
 *
 *  DESCRIPTION
 *      This function handles the CSR_MESH_DATA_BLOCK_IND message
 *
 *  RETURNS
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
extern void handleCSRmeshDataBlockInd(CSR_MESH_STREAM_EVENT_T *p_event)
{
    deviceid_src =p_event->common_data.source_id;//heater ID                      
    deviceid_local=g_node_data.device_uuid.uuid[0];//sensor ID;
    uint8 recvdRTCData[6];//接收指令
    
    switch(p_event->data[0])
    {
        case CSR_DEVICE_INFO_REQ:
        {
            /* Change the Rx scan duty cycle to active at the start of stream */
            EnableHighDutyScanMode(TRUE);
            /* Set the source device ID as the stream target device */
            StreamStartSender(p_event->common_data.source_id );
            tx_stream_offset = 0;
            /* Set the opcode to CSR_DEVICE_INFO_RSP */
            device_info[0] = CSR_DEVICE_INFO_RSP;

            /* start sending the data */
            sendNextPacket();
        }
        break;

        case CSR_DEVICE_INFO_RESET:
        {
            /* Reset the device info */
            device_info_length = sizeof(DEVICE_INFO_STRING);
            device_info[0] = CSR_DEVICE_INFO_RSP;
            device_info[1] = device_info_length;
            MemCopy(&device_info[2], DEVICE_INFO_STRING,
                                                   sizeof(DEVICE_INFO_STRING));
        }
        break;

        /*case CUSTOM_REQ_DATA:                   //Gxxxx
        {
            if(flagCustomReqDataRec==FALSE)
            {
                BME280_Trigger();
                EnableHighDutyScanMode(TRUE);
                SleepModeChange(sleep_mode_never);    
                TimerCreate(3*SECOND,TRUE,debounceTimerHandlerCUSTOMREQDATA);
                TimerCreate(TIMER_SENSOR_READ_INIT_INTERVAL,TRUE,timerHandleReadBME280Data);
                flagCustomReqDataRec=TRUE;
            }            
        }
        break;
        
        case CUSTOM_REQ_DEVID:                           //ThhmmssAxDx->ThhmmssDx
        {                  
            MemSet(recvdRTCData,0x00,sizeof(recvdRTCData)/sizeof(uint8));
            MemCopy(recvdRTCData,&(p_event->data[1]),4);

            //new code20160512
            if((current_sensor_status==SENSOR_STATUS_INIT))//&&(dat1==REQ_ID_CMD)
            {           
                if(onlySetRTCOneTimeFlag==FALSE)    //ensure RTC only written once
                { 
                    PioSetEventMask(1L<<RTC_INT,pio_event_mode_disable);//20170302
                    onlySetRTCOneTimeFlag=TRUE;//20170221:lock
                    dat[0]=CUSTOM_REC_DEVID;
                    dat[1]=(uint8)(deviceid_local&0xff);
                    dat[2]=(uint8)((deviceid_local>>8)&0xff);             
                    current_sensor_status=SENSOR_STATUS_REGISTER; 
                    //TimerCreate(20*SECOND,TRUE,turnONMeshTimerHandler);//20160830
                              
                    hour=recvdRTCData[0];
                    minute=recvdRTCData[1];
                    second=recvdRTCData[2];
                    //multiTimesOneHour=recvdRTCData[3];
                    onDuration=recvdRTCData[3];
                    recvdSearchCmdPacked=recvdRTCData[3];//|((uint16)recvdRTCData[4]);
                    //recvdSearchCmdPacked=storeRecvdRTCDataPacked;
                    storeSearchCmdToNvm(&recvdSearchCmdPacked,1);//20170217
                    
                    sendDataStreamWithCFM(3,deviceid_src,100);//temporarily remove cfm
                    
                    uint16 cmdLength=0;
                    uint16 setPCF2123AlarmTimer[9]=
                    {
                        PACK(0X10,0X00),PACK(0X12,second),PACK(minute,hour),PACK(0X23,0X04),PACK(0X03,0X17),
                        PACK(0X80,0X80),PACK(0X80,0X80),PACK(0X00,0X03),0x00,//规则点开始(起点)  
                    };
                                       
                    setPCF2123AlarmTimer[5]=PACK(0X00,0X80);
                    clearSpiCmdAndLen();
                    cmdArrayCat(setPCF2123AlarmTimer,sizeof(setPCF2123AlarmTimer)/sizeof(uint16));//9
                    cmdLength=checkCmdLength();
                    deviceType=0;
                    spiTransfer(deviceType,PIO_CTRLR_CODE_ADDR0,2*cmdLength );//20170122

                    //TimerCreate(2*SECOND,TRUE,startGetRTCTimerHandler);//read RTC
                }
            }
        }
        break;*/        
        
        case CUSTOM_RTCTIME_CORRECTION:                 //TIME
        {
            MemSet(recvdRTCData,0x00,sizeof(recvdRTCData)/sizeof(uint8));
            MemCopy(recvdRTCData,&(p_event->data[1]),2);

                /*static uint16 restartRTCAlarm=0;
                uint8 minuteInt8=((recvdRTCData[1]>>4)*10) | (recvdRTCData[1]&0x0f);//bcd2int
                switch(minuteInt8/15)
                {
                    case 0:
                        restartRTCAlarm=PACK(0X19,0X15);//[0,15)
                        timeTrace=0;break;
                    case 1:
                        restartRTCAlarm=PACK(0X19,0X30);//[15,30)
                        timeTrace=1;break;                    
                    case 2:
                        restartRTCAlarm=PACK(0X19,0X45);//[30,45)
                        timeTrace=2;break;
                    case 3:
                        restartRTCAlarm=PACK(0X19,0X00);//[45,60)
                        timeTrace=3;break;
                }*/            
            if( (current_sensor_status==sensor_status_normal) &&
                (onlyCorrectTimeOneTimeFlag==FALSE) )
            {
                //TimerDelete(tid);tid=TIMER_INVALID;
                bool oddOrEven=TRUE;
                unpackPopData(rx_queue,oddOrEven,16); 
                
                uint16 cmdLength=0;
                uint16 correctTime[2];
                correctTime[0]=PACK(0X12,recvdRTCData[0]);
                correctTime[1]=PACK(recvdRTCData[1],0X01);//avoid false interrupt(alarm)    
                //01:02:00准点校时
                //if( ((unpackedData[4]==0x00)&&(unpackedData[3]<=0x59))||
                //    ((unpackedData[4]==0x01)&&(unpackedData[3]==0x00)) )    //slower/faster
                {
                    clearSpiCmdAndLen();
                    cmdArrayCat(correctTime,sizeof(correctTime)/sizeof(uint16));//2
                    //cmdArrayCat(&restartRTCAlarm,sizeof(restartRTCAlarm)/sizeof(uint16));//2   
                    cmdLength=checkCmdLength();
                    deviceType=0;
                    spiTransfer(deviceType,PIO_CTRLR_CODE_ADDR0,2*cmdLength );//20170122
                    TimerCreate(1*SECOND,TRUE,correctRTCTimeDebounceHandler);//time calibration
                }
                bleTestServData.currentTime=((uint16)recvdRTCData[1])<<8 | recvdRTCData[0];              
            }            
        }
        break;
        
            case REQUEST_DATA://target as sender
            {
                devid_bridge=p_event->common_data.source_id;
                if(flagCustomReqDataRec==FALSE)
                {
                    //isRemote=TRUE;
                    BME280_Trigger();
                    TimerCreate(10*MILLISECOND,TRUE,timerHandleReadBME280Data);
                    //PioSet(LED_PIO_IND,LED_OFF);//OFF
                    //TimerCreate(2*SECOND,TRUE,fromBridgeHandler);
                    TimerCreate(3*SECOND,TRUE,debounceTimerHandlerCUSTOMREQDATA);
                    flagCustomReqDataRec=TRUE;//locked
                }
            }
            break;        
                
        default:
        break;
    }
}
void fromBridgeHandler(timer_id tid)
{
    TimerDelete(tid);tid=TIMER_INVALID;
    PioSet(LED_PIO_IND,LED_ON);//ON
}
void debounceTimerHandlerCUSTOMREQDATA(timer_id tid)
{
    TimerDelete(tid);
    tid=TIMER_INVALID;        
    flagCustomReqDataRec=FALSE;
}
/*-----------------------------------------------------------------------------*
 *  NAME
 *      handleCSRmeshDataStreamDataInd
 *
 *  DESCRIPTION
 *      This function handles the CSR_MESH_DATA_STREAM_DATA_IND message
 *
 *  RETURNS
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
extern void handleCSRmeshDataStreamDataInd(CSR_MESH_STREAM_EVENT_T *p_event)
{  
    TimerDelete(rx_stream_timeout_tid);
    rx_stream_timeout_tid = TimerCreate(RX_STREAM_TIMEOUT, TRUE,
                                               rxStreamTimeoutHandler);
    /*20160603*/
    //targetDevid==p_event->common_data.source_id;
    deviceid_src =p_event->common_data.source_id;//heater ID                      
    deviceid_local=g_node_data.device_uuid.uuid[0];//sensor ID;
    
    /* Set stream_in_progress flag to TRUE */
    rx_stream_in_progress = TRUE;

    if( rx_stream_offset == 0 )
    {
        /* If the stream offset is 0. The data[0] will be the CODE */
        switch(p_event->data[0])
        {
            case CSR_DEVICE_INFO_REQ:
            {
                /* Change the Rx scan duty cycle to active at start of stream */
                EnableHighDutyScanMode(TRUE);
                /* Set the source device ID as the stream target device */
                StreamStartSender(p_event->common_data.source_id);
                tx_stream_offset = 0;
                /* Set the stream code to CSR_DEVICE_INFO_RSP */
                device_info[0] = CSR_DEVICE_INFO_RSP;
                /* Start sending */
                sendNextPacket();
            }
            break;

            case CSR_DEVICE_INFO_RESET:
            {
                /* Reset the device info */
                device_info_length = sizeof(DEVICE_INFO_STRING);
                device_info[0] = CSR_DEVICE_INFO_RSP;
                device_info[1] = device_info_length;
                MemCopy(&device_info[2], DEVICE_INFO_STRING,
                                                    sizeof(DEVICE_INFO_STRING));
            }
            break;

            case CSR_DEVICE_INFO_SET:
            {
                /* CSR_DEVICE_INFO_SET is received. Store the code, length and
                 * the data into the device_info array in the format received
                 */
                current_stream_code = CSR_DEVICE_INFO_SET;
                device_info_length = p_event->data[1];
                MemCopy(device_info, p_event->data, p_event->data_len);
                rx_stream_offset = p_event->data_len;
                /* Change the Rx scan duty cycle to active at start of stream */
                EnableHighDutyScanMode(TRUE);
            }
            break;            
            
            default:
            break;            
        }
    }
    else
    {
        if( current_stream_code == CSR_DEVICE_INFO_SET
            && rx_stream_offset + p_event->data_len < sizeof(device_info) )
        {
            MemCopy(&device_info[rx_stream_offset], p_event->data,
                                                            p_event->data_len);
            rx_stream_offset += p_event->data_len;
        }

        /* No other CODE is handled currently */
    }
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      handleCSRmeshDataStreamSendCfm
 *
 *  DESCRIPTION
 *      This function handles the CSR_MESH_DATA_STREAM_SEND_CFM message
 *
 *  RETURNS
 *      Nothing
 *
 *----------------------------------------------------------------------------*/
extern void handleCSRmeshDataStreamSendCfm(CSR_MESH_STREAM_EVENT_T *p_event) //handler for receving CFM signal from traget dev(receiver)
{   
    streamCFM=TRUE; 
    
    if(current_sensor_status==sensor_status_register)
    {   
        
        streamSendRetryCount = 0; //reset send retry counter
        streamSendRetryCountCheck = 0;
        TimerDelete(stream_send_retry_tid);
        stream_send_retry_tid=TIMER_INVALID;
        
        current_sensor_status=sensor_status_normal;//change status and start normal status timer
        saveSensorStatusToNVM((uint16)current_sensor_status);
    
        PioEnablePWM(1,FALSE);//LED OFF ind
        PioSetMode(LED_PIO_IND,pio_mode_user);
        if(ledFlashFlag==0)                   
            PioSet(LED_PIO_IND,0);//OFF
        else 
            PioSet(LED_PIO_IND,1);//ON         
        TimerCreate(0*MILLISECOND,TRUE,forceIntoSleepTimerHandler);//20170216
    }
}
#endif /* ENABLE_DATA_MODEL */

